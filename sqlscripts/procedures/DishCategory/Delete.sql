-- DishCategory
use wwwet;

-- Delete
DELIMITER ;;
CREATE PROCEDURE `DishCategoryDelete`(
	 pId INT 
)
BEGIN
DELETE FROM `DishCategory`
	WHERE `DishCategory`.`Id` = pId;
END ;;
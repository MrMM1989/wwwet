<?php $partialView('Shared', 'AdminHeader'); ?>
    <!--Mealtime/ReadingOne-->
    <main>
        <section id="content">
            <h2>Tijd van Maaltijd wijzigen: <?php echo $model->getName(); ?></h2>
            <form action="?uc=Mealtime-Update" method="post">
                <ul>
                    <li>
                        <input id="Mealtime-Id" type="hidden" name="Mealtime-Id"
                               value="<?php echo $model->getId();?>" />
                    </li>
                    <li>
                        <label for="Mealtime-Name">Naam</label>
                        <input id="Mealtime-Name" name="Mealtime-Name" type="text"
                               value="<?php echo $model->getName();?>" />
                    </li>
                </ul>

                <ul>
                    <li class="buttonbarbottom">
                        <button class="buttongreen" name="submit" type="submit">
                            <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Opslaan
                        </button>
                        <a class="button buttonspaceleft buttonred" href="?uc=Mealtime-index">
                            <i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Annuleren
                        </a>
                    </li>
                </ul>
            </form>
        </section>
    </main>

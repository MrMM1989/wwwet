<?php
/**
 * Created by PhpStorm.
 * User: Kurt
 * Date: 1/05/2016
 * Time: 13:50
 */

namespace ModernWays\WWWET\Model;


class ResidentPreferences extends \ModernWays\Mvc\Model
{
    private $id;
    private $idResident;
    private $idPreference;
    private $comment;

    protected $residentList;
    protected $preferenceList;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdResident()
    {
        return $this->idResident;
    }

    /**
     * @param mixed $idResident
     */
    public function setIdResident($idResident)
    {
        $this->idResident = $idResident;
    }

    /**
     * @return mixed
     */
    public function getIdPreference()
    {
        return $this->idPreference;
    }

    /**
     * @param mixed $idPreference
     */
    public function setIdPreference($idPreference)
    {
        $this->idPreference = $idPreference;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getResidentList()
    {
        return $this->residentList;
    }

    /**
     * @param mixed $residentList
     */
    public function setResidentList($residentList)
    {
        $this->residentList = $residentList;
    }

    /**
     * @return mixed
     */
    public function getPreferenceList()
    {
        return $this->preferenceList;
    }

    /**
     * @param mixed $preferenceList
     */
    public function setPreferenceList($preferenceList)
    {
        $this->preferenceList = $preferenceList;
    }
}
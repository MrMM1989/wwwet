-- Preference --
use wwwet;
-- Stored Procedures --

-- Select All --
DELIMITER ;;
CREATE PROCEDURE `PreferencesSelectAll`(
)
BEGIN
SELECT `Preference`.*, `Dish`.`Name` as DishName, 
        `DietaryInfo`.`Description` as DietaryInfoDescription, 
        `DishCategory`.`Name` as DishCategoryName, 
        `PreferenceType`.`Name` as PreferenceTypeName
FROM `Preference`
LEFT OUTER JOIN `Dish` on `Dish`.`Id` = `Preference`.IdDish
LEFT OUTER JOIN `DietaryInfo` on `DietaryInfo`.`Id` = `Preference`.IdDietaryInfo
LEFT OUTER JOIN `DishCategory` on `DishCategory`.`Id` = `Preference`.IdDishCategory
LEFT OUTER JOIN `PreferenceType` on `PreferenceType`.`Id` = `Preference`.IdPreferenceType;
END ;;
<?php $partialView('Shared', 'AdminHeader'); ?>
<!--standardMenu\Inserting-->
<main>
    <section id="content">
        <h2>Een nieuw Standaardmenu toevoegen</h2>
        <form action="index.php?uc=Menu-standardMenuInsert" method="post">
            <ul>
                <li>
                    <label for="standardMenu-name">Naam</label>
                    <input id="standardMenu-name" name="standardMenu-name" type="text" required />
                </li>
                <li>
                    <label for="standardMenu-idMealtime">Tijdstip</label>
                    <?php if($model->getMealtimeList()): ?>
                        <select name="standardMenu-idMealtime" id="standardMenu-idMealtime">
                            <?php foreach($model->getMealtimeList() as $item): ?>
                                <option value="<?php echo $item['Id']?>"<?php echo ($item['Id'] == 2 ? ' selected' : '');?>>
                                    <?php echo $item['Name']; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    <?php else: ?>
                        Geen tijdstippen gevonden.
                    <?php endif;?>
                </li>
                <li>
                    <label for="standardMenu-date">Datum</label>
                    <!--DatePick-->
                    <select id="standardMenu-date" name="standardMenu-date">
                        <option disabled>--Selecteer een datum--</option>
                        <?php foreach($model->getDatelist() as $item): ?>
                            <option value="<?php echo $item['database-format']; ?>"
                                <?php echo ($item['database-format'] == date('Y-m-d') ? ' selected' : '');?>
                            >
                                <?php echo $item['ui-format']; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </li>
            </ul>
            <h3>Keukenmenus</h3>
            <ul>
                <li>
                    <label for="kitchenMenu-list">Kies een keukenmenu</label>
                    <select id="kitchenMenu-list" class="smaller" name="kitchenMenu-list">
                        <option disabled="disabled">--Selecteer een keukenmenu--</option>
                        <?php if(!empty($model->getKitchenMenuList())): ?>
                            <?php foreach($model->getKitchenMenuList() as $item): ?>
                                <option value="<?php echo $item['Id']; ?>">
                                    <?php echo $item['Date']; ?>
                                </option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </li>
            </ul>
            <h3>Gerechten</h3>
            <ul>
                <li>
                    <ul id="ajax" class="edit-list">
                        <li>Geen gerechten toegevoegd</li>
                    </ul>
                </li>
                <?php if(!empty($model->getKitchenMenuList())): ?>
                    <?php foreach($model->getKitchenMenuList() as $keukenMenus): ?>
                        <li id="menu-dishes-li<?php echo $keukenMenus['Id'];?>">
                            <label for="menu-dishes<?php echo $keukenMenus['Id'];?>">Selecteer een gerecht</label>
                            <select id="menu-dishes<?php echo $keukenMenus['Id'];?>" class="smaller" name="menu-dishes">
                                <option disabled="disabled">--Selecteer een gerecht--</option>
                                <?php foreach($keukenMenus['Dishes'] as $gerecht): ?>
                                    <option value="<?php echo $gerecht['IdDish']; ?>">
                                        <?php echo $gerecht['DishName']; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                            <button id="menu-dish-add<?php echo $keukenMenus['Id'];?>" class="horizontal buttongreen buttonspaceleft"
                                    name="menu-dish-add" type="button">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                        </li>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>

            <ul>
                <!--Buttons-->
                <li class="buttonbarbottom">
                    <button class="buttongreen" name="submit" type="submit">
                        <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Toevoegen
                    </button>
                    <a class="button buttonspaceleft buttonred" href="index.php?uc=Menu-standardMenuIndex">
                        <i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Annuleren
                    </a>
                </li>
            </ul>
        </form>
    </section>
    <script src="/js/showhide-kitchenmenu.js"></script>
</main>
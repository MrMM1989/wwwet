<?php $partialView('Shared', 'AdminHeader'); ?>
<!--Dish/Inserting-->
    <main>
        <section id="content">
            <h2>Een niew gerecht toevoegen</h2>
            <form action="index.php?uc=Dish-Insert" method="post">
                <ul>
                    <li>
                        <label for="Dish-Name">Naam</label>
                        <input type="text" id="Dish-Name" name="Dish-Name" required/>
                    </li>
                    <li>
                        <label for="Dish-IdDishCategory">Categorie</label>
                        <?php
                        if($model->getDishCategoryList()) {
                            ?>
                            <select name="Dish-IdDishCategory" id="Dish-IdDishCategory">
                                <?php
                                foreach ($model->getDishCategoryList() as $item) {
                                    ?>
                                    <option value="<?php echo $item['Id']?>"><?php echo $item['Name']; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <?php
                        } else {
                            ?>
                            Geen categorieën gevonden
                            <?php
                        }
                        ?>
                    </li>
                    <li>
                        <label for="Dish-ByDefault">Standaard alternatief</label>
                        <input id="Dish-ByDefault" name="Dish-ByDefault" type="checkbox" />
                    </li>
                    <li>
                        <label>Terugkerend op:</label>
                    </li>
                    <li>
                        <div>
                            <input id="Dish-Monday" name="Dish-Monday" type="checkbox"/>
                            <label for="Dish-Monday">Maandag</label>
                        </div>
                        <div>
                            <input id="Dish-Tuesday" name="Dish-Tuesday" type="checkbox"/>
                            <label for="Dish-Tuesday">Dinsdag</label>
                        </div>
                        <div>
                            <input id="Dish-Wednesday" name="Dish-Wednesday" type="checkbox"/>
                            <label for="Dish-Wednesday">Woensdag</label>
                        </div>
                        <div>
                            <input id="Dish-Thursday" name="Dish-Thursday" type="checkbox"/>
                            <label for="Dish-Thursday">Donderdag</label>
                        </div>
                        <div>
                            <input id="Dish-Friday" name="Dish-Friday" type="checkbox"/>
                            <label for="Dish-Friday">Vrijdag</label>
                        </div>
                        <div>
                            <input id="Dish-Saturday" name="Dish-Saturday" type="checkbox"/>
                            <label for="Dish-Saturday">Zaterdag</label>
                        </div>
                        <div>
                            <input id="Dish-Sunday" name="Dish-Sunday" type="checkbox"/>
                            <label for="Dish-Sunday">Zondag</label>
                        </div>
                    </li>
                </ul>
                <h3>Dieet Informatie</h3>
                <ul>
                    <li>
                        <ul id="ajax" class="edit-list">
                            <li>Geen dieet informatie toegevoegd</li>
                        </ul>
                    </li>
                    <li>
                        <label for="dietary-info">Voeg dieet informatie toe: </label>
                        <select id="dietary-info" class="smaller" name="dietary-info">
                            <option disabled="disabled">--Selecteer dieet informatie--</option>
                            <?php if(!empty($model->getDietaryInfoList())): ?>
                                <?php foreach($model->getDietaryInfoList() as $item): ?>
                                    <option value="<?php echo $item['Id']; ?>"><?php echo $item['Description']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                        <button id="dietary-info-add" class="horizontal buttongreen buttonspaceleft" name="dietary-info-add" type="button">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </button>
                    </li>

                    <!--Buttons-->
                    <li class="buttonbarbottom">
                        <button class="buttongreen" name="submit" type="submit">
                            <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Toevoegen
                        </button>
                        <a class="button buttonspaceleft buttonred" href="index.php?uc=Dish-Index">
                            <i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Annuleren
                        </a>
                    </li>
                </ul>
            </form>
        </section>
    </main>
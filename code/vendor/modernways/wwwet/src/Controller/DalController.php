<?php
namespace ModernWays\WWWET\Controller;

class DalController extends \ModernWays\Mvc\Controller {
	
	//Provider configuration
	protected $providerName = 'wwwet-server';
	protected $databaseName = 'wwwet';
	protected $password = 'W6db/io!8';
	protected $username = 'wwwetweb';
	protected $hostname = '164.132.193.60:3306';
	
	protected $dal;
	protected $model;
	protected $modelName;
	protected $provider;
	
	public function __construct ($route, $noticeBoard)
	{		
		//Call the parent constructor
		parent::__construct($route, $noticeBoard);
		
		// Create provider
		$this->provider = $this->createProvider($noticeBoard);
	}
	
	public function initModelName($model)
	{
		$this->modelName = $this->getModelName($model);
		
		return $this->modelName;
	}
	
	public function createProvider($noticeBoard)
	{
		$provider = new \ModernWays\AnOrmApart\Provider($this->providerName, $noticeBoard);
		$provider->setDatabaseName($this->databaseName);
		$provider->setHostName($this->hostname);
		$provider->setUserName($this->username);
		$provider->setPassword($this->password);
		
		return $provider;
	}
	
	public function loadModel($noticeBoard)
	{
		$modelClass = $this->loadModelClass($this->modelName);
		$this->model = new $modelClass($noticeBoard);
		
		return $this->model;
	}
	
	public function loadForeignModel($modelName, $noticeBoard){
		
		$modelClass = $this->loadModelClass($modelName);		
		$model = new $modelClass($noticeBoard);
		
		return $model;		
	}
		
	public function loadDal($noticeBoard)
	{		
		// Load DAL
		$dalClass = $this->loadDalClass($this->modelName);
		$this->dal = new $dalClass($this->model, $this->provider);	
		
		return $this->dal;	
	}
	
	public function loadForeignDal($model){
		
		// Get name of the model - Needed for loading the dal
		$modelName = $this->getModelName($model);
		
		// Load DAL
		$dalClass = $this->loadDalClass($modelName);
		$dal = new $dalClass($model, $this->provider);
		
		return $dal;		
	}
	
	private function getModelName($model){
		
		$reflectionClass = new \ReflectionClass($model);		
		return $reflectionClass->getShortName();		
	}
	
	private function loadModelClass($modelName){
		
		$modelClass = '\ModernWays\WWWET\Model\\'.$modelName;
		return $modelClass;		
	}
	
	private function loadDalClass($dalName){
		
		$dalClass = '\ModernWays\WWWET\Dal\\'.$dalName;
		return $dalClass;
	}	
	
}
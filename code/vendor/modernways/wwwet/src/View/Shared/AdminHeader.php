<?php
	$noticeboard = new \ModernWays\AnOrmApart\NoticeBoard();

	$session = new \ModernWays\Identity\Session($noticeboard);
 ?>
<header>
	<h1 id="pagetitle"><a href="index.php?uc=Admin-index">Wat eten we vandaag?</a></h1>
	<nav class="usernav">
		<ul>
			<?php if($session->isTicketSet()): ?>
			<li>
				Welkom <a href=""><?php echo $session->get('Name'); ?></a>&nbsp;|  
			</li>
			<?php endif; ?>
			<li>
				&nbsp;<a href="index.php?uc=Home-logout">Uitloggen</a> 
			</li>
		</ul>
	</nav>
</header>
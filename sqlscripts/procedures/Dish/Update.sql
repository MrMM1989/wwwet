-- Dish --
use wwwet;
-- Stored Procedures --

-- Update --
DELIMITER ;;
CREATE PROCEDURE `DishUpdate`(
	pId INT ,
	pIdDishCategory INT ,
	pName NVARCHAR (255) ,
	pByDefault BIT ,
	pMargin FLOAT ,
	pMonday BIT ,
	pTuesday BIT ,
	pWednesday BIT ,
	pThursday BIT ,
	pFriday BIT ,
	pSaturday BIT ,
	pSunday BIT 
)
BEGIN
UPDATE `Dish`
	SET
		`IdDishCategory` = pIdDishCategory,
		`Name` = pName,
		`ByDefault` = pByDefault,
		`Margin` = pMargin,
		`Monday` = pMonday,
		`Tuesday` = pTuesday,
		`Wednesday` = pWednesday,
		`Thursday` = pThursday,
		`Friday` = pFriday,
		`Saturday` = pSaturday,
		`Sunday` = pSunday,
		`UpdatedOn` = NOW()
	WHERE `Dish`.`Id` = pId;
END ;;
-- Menu --
use wwwet;
-- Stored Procedures --

-- GetKitchenMenuDishes --
DELIMITER ;;
CREATE PROCEDURE `MenuGetKitchenMenuDishesByIdMenu`(
	IN pIdMenu INT
)
BEGIN
	select MenuDish.Id, MenuDish.IdMenu, MenuDish.IdDish, Menu.Date, Menu.Name as MenuName, Dish.Name as DishName, Menu.IdMenuType
	from MenuDish
		inner join Menu on MenuDish.IdMenu = Menu.Id
		inner join Dish on MenuDish.IdDish = Dish.Id
	where Menu.Id = pIdMenu
	order by Menu.Date;
END ;;
USE wwwet;
-- -----------------------------------------------------
-- Create View vwSelectResidentGroup
-- -----------------------------------------------------
DROP VIEW IF EXISTS vwSelectResidentGroup;
CREATE VIEW vwSelectResidentGroup AS
SELECT r.Id, r.Name, r.RoomNumber, r.Diabetes, r.Diet, r.Extra, r.IdGroup, g.Name AS NameGroup
FROM Resident AS r 
INNER JOIN `Group` AS g
ON r.IdGroup = g.Id;
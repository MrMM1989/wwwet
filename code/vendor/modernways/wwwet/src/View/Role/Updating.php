<?php $partialView('Shared', 'AdminHeader'); ?>
<main>
	<section id="content">
		<h2>Een rol wijzigen: <?php echo $model->getName(); ?></h2>
		<form action="index.php?uc=Role-update" method="post">
			<ul>
				<li>
					<input id="role-id" name="role-id" type="hidden" value="<?php echo $model->getId(); ?>" />
					<label for="role-name">Naam: </label>
					<input id="role-name" name="role-name" type="text" required  value="<?php echo $model->getName(); ?>"/>
				</li>
				<li class="buttonbarbottom">
					<button class="buttongreen" name="submit" type="submit">
						<i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Wijzigen
					</button>
					<a class="button buttonspaceleft buttonred" href="index.php?uc=Role-readingOne_<?php echo $model->getId(); ?>">
						<i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Annuleren
					</a>
				</li>
			</ul>
		</form>		
	</section>
</main>
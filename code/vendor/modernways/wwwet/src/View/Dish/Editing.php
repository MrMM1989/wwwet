<!--Dish\Editing-->
<div class="list">
    <?php
    if (count($model->getList()) > 0)
    {
        ?>
        <table>
            <?php
            foreach ($model->getList() as $item)
            {
                ?>
                <tr>
                    <td>
                        <a href="index.php?uc=Dish-readingOne_<?php echo $item['Id'];?>">Select</a>
                    </td>
                    <td>
                        <?php echo $item['Name'];?>
                    </td>
                    <td>
                        <?php echo $item['IdDishCategoryName'];?>
                    </td>
                    <td>
                        <?php echo ($item['ByDefault'] == '1' ? 'Alt' : '');
                        echo ($item['Monday'] == '1' ? 'Ma' : '');
                        echo ($item['Tuesday'] == '1' ? 'Di' : '');
                        echo ($item['Wednesday'] == '1' ? 'Wo' : '');
                        echo ($item['Thursday'] == '1' ? 'Do' : '');
                        echo ($item['Friday'] == '1' ? 'Vr' : '');
                        echo ($item['Saturday'] == '1' ? 'Za' : '');
                        echo ($item['Sunday'] == '1' ? 'Zo' : '');?>
                    </td>

                </tr>
                <?php
            }
            ?>
        </table>
        <?php
    }
    else
    {
        ?>
        <p>Geen gerechten</p>
        <?php
    }
    ?>
</div>

<?php $appStateView(); ?>
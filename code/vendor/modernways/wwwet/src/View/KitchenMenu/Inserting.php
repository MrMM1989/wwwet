<?php $partialView('Shared', 'AdminHeader'); ?>
<!--KitchenMenu\Inserting-->
<main>
    <section id="content">
        <h2>Een nieuw Keukenmenu toevoegen</h2>
        <form action="index.php?uc=Menu-KitchenMenuInsert" method="post">
            <ul>
                <li>
                    <label for="kitchenMenu-idMealtime">Tijdstip</label>
                    <?php if($model->getMealtimeList()): ?>
                        <select name="kitchenMenu-idMealtime" id="kitchenMenu-idMealtime">
                            <?php foreach($model->getMealtimeList() as $item): ?>
                                <option value="<?php echo $item['Id']?>"<?php echo ($item['Id'] == 2 ? ' selected' : '');?>>
                                    <?php echo $item['Name']; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    <?php else: ?>
                        Geen tijdstippen gevonden.
                    <?php endif;?>
                </li>
                <li>
                    <label for="kitchenMenu-date">Datum</label>
                    <!--DatePick-->
                    <select id="kitchenMenu-date" name="kitchenMenu-date">
                        <option disabled>--Selecteer een datum--</option>
                        <?php foreach($model->getDatelist() as $item): ?>
                            <option value="<?php echo $item['database-format']; ?>"
                                <?php echo ($item['database-format'] == date('Y-m-d') ? ' selected' : '');?>
                            >
                                <?php echo $item['ui-format']; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </li>
            </ul>
            <h3>Gerechten</h3>
            <ul>
                <li>
                    <ul id="ajax" class="edit-list">
                        <li>Geen gerechten toegevoegd</li>
                    </ul>
                </li>
                <li>
                    <label for="menu-dishes">Voeg gerechten toe: </label>
                    <select id="menu-dishes" class="smaller" name="menu-dishes">
                        <option disabled="disabled">--Selecteer een gerecht--</option>
                        <?php if(!empty($model->getDishList())): ?>
                            <?php foreach($model->getDishList() as $item): ?>
                                <option value="<?php echo $item['Id']; ?>"><?php echo $item['Name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <button id="menu-dish-add" class="horizontal buttongreen buttonspaceleft" name="menu-dish-add" type="button">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                </li>

                <!--Buttons-->
                <li class="buttonbarbottom">
                    <button class="buttongreen" name="submit" type="submit">
                        <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Toevoegen
                    </button>
                    <a class="button buttonspaceleft buttonred" href="index.php?uc=Menu-KitchenMenuIndex">
                        <i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Annuleren
                    </a>
                </li>
            </ul>
        </form>
    </section>
</main>
<?php
/**
 * Created by PhpStorm.
 * User: Kurt
 * Date: 1/05/2016
 * Time: 13:27
 */

namespace ModernWays\WWWET\Dal;


class Menu extends \ModernWays\AnOrmApart\Dal
{
    public function __construct(\ModernWays\WWWET\Model\Menu $model, \ModernWays\AnOrmApart\Provider $provider)
    {
        $this->model = $model;
        parent::__construct($provider);
    }

    public function readingOne()
    {
        return parent::readingOne();
    }

    public function readingAllKitchen($storedProcedureName = 'SelectAllKitchen')
    {
        return parent::readingAll($storedProcedureName);
    }

    public function readingAllStandard($storedProcedureName = 'SelectAllStandard')
    {
        return parent::readingAll($storedProcedureName);
    }

    public function readMealtime() {
        if (!$this->provider->isConnected()) {
            $this->provider->open();
        }
        if ($this->provider->isConnected()) {
            try {
                $preparedStatement = $this->provider->getPdo()->prepare("CALL MealtimeSelectAll()");
                $preparedStatement->execute();
                $this->rowCount = $preparedStatement->rowCount();
                $this->model->setMealtimeList($preparedStatement->fetchAll());

                if ($this->model->getList()) {
                    $this->model->getModelState()->crudNotice('Menu: readMealtime', true, $this, null, $preparedStatement->errorInfo());
                } else {
                    $this->model->getModelState()->crudNotice('Menu: readMealtime', false, $this, null, $preparedStatement->errorInfo());
                }

            } catch (\PDOException $e) {
                $this->model->getModelState()->crudNotice("Menu: readMealtime, Error: {$e}", true, $this, $e);
            }
        }
    }

    public function linkDishes() {
        $list = $_SESSION['DishList'];

        if (!$this -> provider -> isConnected()) {
            $this -> provider -> open();
        }
        if ($this -> provider -> isConnected()) {
            try {
                foreach($list as $item) {
                    //Prepare the statement
                    $insertSql = 'INSERT INTO MenuDish (IdMenu, IdDish) VALUES (:idMenu, :idDish)';
                    $preparedStatement = $this -> provider -> getPdo() -> prepare($insertSql);
                    $preparedStatement -> bindValue(':idMenu', $this->model->getId(), \PDO::PARAM_INT);
                    $preparedStatement -> bindValue(':idDish', $item['Id'], \PDO::PARAM_INT);

                    //Execute statement
                    $result = $preparedStatement->execute();

                    if ($result) {
                        $this->model->getModelState()->crudNotice('LinkDishes', true, $this, null, $preparedStatement->errorInfo());
                    } else {
                        $this->model->getModelState()->crudNotice('LinkDishes', false, $this, null, $preparedStatement->errorInfo());
                    }
                }

            } catch (\PDOException $e) {
                $this -> model -> getModelState() -> crudNotice('Insert', false, $this, $e, null);
            }
        } else {
            $this -> model -> getModelState() -> disconnected($this -> provider);
        }
    }

    public function readLinkedDishes() {
        if (!$this -> provider -> isConnected()) {
            $this -> provider -> open();
        }
        if ($this -> provider -> isConnected()) {
            try {
                //Prepare the statement
                $findSql = 'call MenuGetDishes(:idMenu)';
                $preparedStatement = $this -> provider -> getPdo() -> prepare($findSql);
                $preparedStatement -> bindValue(':idMenu', $this->model->getId(), \PDO::PARAM_INT);

                //Execute statement
                $preparedStatement->execute();

                //Set Model
                $this->model->setMenuDishList($preparedStatement->fetchAll());

            } catch (\PDOException $e) {
                $this -> model -> getModelState() -> crudNotice('MenuGetDishes', false, $this, $e, null);
            }
        } else {
            $this -> model -> getModelState() -> disconnected($this -> provider);
        }
    }

    public function readKitchenMenus() {
        if (!$this -> provider -> isConnected()) {
            $this -> provider -> open();
        }
        if ($this -> provider -> isConnected()) {
            try {
                $menusSql = 'call MenuGetKitchenMenus()';
                $preparedStatement = $this->provider->getPdo()->prepare($menusSql);
                $preparedStatement->execute();

                $tmpKitchenMenus = $preparedStatement->fetchAll();
                $kitchenMenuList = array();

                foreach($tmpKitchenMenus as $item) {
                    $dishesSql = 'call MenuGetKitchenMenuDishesByIdMenu(' . $item['Id'] . ')';
                    $preparedStatement = $this->provider->getPdo()->prepare($dishesSql);
                    $preparedStatement->execute();

                    $item['Dishes'] = $preparedStatement->fetchAll();

                    array_push($kitchenMenuList, $item);
                }

                $this->model->setKitchenMenuList($kitchenMenuList);

            } catch (\PDOException $e) {
                $this -> model -> getModelState() -> crudNotice('GetKitchenMenuDishList', false, $this, $e, null);
            }
        } else {
            $this -> model -> getModelState() -> disconnected($this -> provider);
        }
    }
}
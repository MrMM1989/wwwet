<?php
namespace ModernWays\WWWET\Controller;

class Preferences extends \ModernWays\WWWET\Controller\DalController
{

    public function __construct($route, $noticeBoard)
    {
        // Call parent constructor
        parent::__construct($route, $noticeBoard);

        // Set modelName & load model and DAL
        $this->modelName = $this->initModelName($this);
        $this->loadModel($this->noticeBoard);
        $this->loadDal($this->noticeBoard);
    }

    public function delete()
    {
        $this->model->setId($this->route->getId());

        $this->dal->deleteOne();
        $this->dal->readingAll();

        return $this->view($this->modelName, 'Index', $this->model);
    }

    public function deleting()
    {
        $this->model->setId($this->route->getId());
        $this->model->setDishList($this->dal->reading('`Dish`', 'Name, Id', null, false));
        $this->model->setDietaryInfoList($this->dal->reading('`DietaryInfo`', 'Description, Id', null, false));
        $this->model->setDishCategoryList($this->dal->reading('`DishCategory`', 'Name, Id', null, false));
        $this->model->setPreferenceTypeList($this->dal->reading('`PreferenceType`', 'PreferenceTypeSelectAll', null, true));

        $this->dal->readingOne();

        return $this->view($this->modelName, 'Deleting', $this->model);
    }

    public function index()
    {
        $this->dal->readingAll();

        return $this->view($this->modelName, 'Index', $this->model);
    }

    public function insert()
    {
        $this->model->setName(filter_input(INPUT_POST, 'preferences-name', FILTER_SANITIZE_STRING));

        $preferenceType = filter_input(INPUT_POST, 'preferences-idPreferenceType', FILTER_SANITIZE_NUMBER_INT);
        $this->model->setIdPreferenceType($preferenceType);

        if($_POST['preferences-yesNo'] == 0) {
            $this->model->setYesNo(0);
        } elseif($_POST['preferences-yesNo'] == 1) {
            $this->model->setYesNo(1);
        }

        switch ($preferenceType) {
            case 1:
                $this->model->setIdDietaryInfo(filter_input(INPUT_POST, 'preferences-idDietaryInfo', FILTER_SANITIZE_NUMBER_INT));
                break;
            case 2:
                $this->model->setIdDish(filter_input(INPUT_POST, 'preferences-idDish', FILTER_SANITIZE_NUMBER_INT));
                break;
            case 3:
                $this->model->setIdDishCategory(filter_input(INPUT_POST, 'preferences-idDishCategory', FILTER_SANITIZE_NUMBER_INT));
                break;
            case 4:
                $gerechtOfType = filter_input(INPUT_POST, 'preferences-gerechtOfType', FILTER_SANITIZE_STRING);

                if($gerechtOfType == 'Gerecht') {
                    $this->model->setIdDish(filter_input(INPUT_POST, 'preferences-idDish', FILTER_SANITIZE_NUMBER_INT));
                } elseif ($gerechtOfType == 'Type Gerecht') {
                    $this->model->setIdDishCategory(filter_input(INPUT_POST, 'preferences-idDishCategory', FILTER_SANITIZE_NUMBER_INT));
                }

                $this->model->setPreperation(filter_input(INPUT_POST, 'preferences-preperation', FILTER_SANITIZE_STRING));
                break;
            case 5:
                $gerechtOfType = filter_input(INPUT_POST, 'preferences-gerechtOfType', FILTER_SANITIZE_STRING);

                if($gerechtOfType == 'Gerecht') {
                    $this->model->setIdDish(filter_input(INPUT_POST, 'preferences-idDish', FILTER_SANITIZE_NUMBER_INT));
                } elseif ($gerechtOfType == 'Type Gerecht') {
                    $this->model->setIdDishCategory(filter_input(INPUT_POST, 'preferences-idDishCategory', FILTER_SANITIZE_NUMBER_INT));
                }

                $this->model->setPortionSize(filter_input(INPUT_POST, 'preferences-portionSize', FILTER_SANITIZE_STRING));
                break;
        }

        $this->dal->insert();
        $this->dal->readingAll();

        return $this->view($this->modelName, 'Index', $this->model);
    }

    public function inserting()
    {
        $this->model->setDishList($this->dal->reading('`Dish`', 'Name, Id', null, false));
        $this->model->setDietaryInfoList($this->dal->reading('`DietaryInfo`', 'Description, Id', null, false));
        $this->model->setDishCategoryList($this->dal->reading('`DishCategory`', 'Name, Id', null, false));
        $this->model->setPreferenceTypeList($this->dal->reading('`PreferenceType`', 'PreferenceTypeSelectAll', null, true));

        return $this->view($this->modelName, 'Inserting', $this->model);
    }

    public function readingOne()
    {
        if(isset($_POST['preferences-id'])) {
            $this->model->setId(filter_input(INPUT_POST, 'preferences-id', FILTER_SANITIZE_NUMBER_INT));
        } else {
            $this->model->setId($this->route->getId());
        }
        $this->model->setDishList($this->dal->reading('`Dish`', 'Name, Id', null, false));
        $this->model->setDietaryInfoList($this->dal->reading('`DietaryInfo`', 'Description, Id', null, false));
        $this->model->setDishCategoryList($this->dal->reading('`DishCategory`', 'Name, Id', null, false));
        $this->model->setPreferenceTypeList($this->dal->reading('`PreferenceType`', 'PreferenceTypeSelectAll', null, true));

        $this->dal->readingOne();

        return $this->view($this->modelName, 'ReadingOne', $this->model);
    }

    public function update()
    {
        if(isset($_POST['preferences-id'])) {
            $this->model->setId(filter_input(INPUT_POST, 'preferences-id', FILTER_SANITIZE_NUMBER_INT));
        } else {
            $this->model->setId($this->route->getId());
        }
        $this->model->setName(filter_input(INPUT_POST, 'preferences-name', FILTER_SANITIZE_STRING));
        $this->model->setIdDish(filter_input(INPUT_POST, 'preferences-idDish', FILTER_SANITIZE_NUMBER_INT));
        $this->model->setIdDietaryInfo(filter_input(INPUT_POST, 'preferences-idDietaryInfo', FILTER_SANITIZE_NUMBER_INT));
        $this->model->setIdDishCategory(filter_input(INPUT_POST, 'preferences-idDishCategory', FILTER_SANITIZE_NUMBER_INT));
        $this->model->setIdPreferenceType(filter_input(INPUT_POST, 'preferences-idPreferenceType', FILTER_SANITIZE_NUMBER_INT));
        $this->model->setPreperation(filter_input(INPUT_POST, 'preferences-preperation', FILTER_SANITIZE_STRING));
        $this->model->setPortionSize(filter_input(INPUT_POST, 'preferences-portionSize', FILTER_SANITIZE_STRING));

        $this->model->setDishList($this->dal->reading('`Dish`', 'Name, Id', null, false));
        $this->model->setDietaryInfoList($this->dal->reading('`DietaryInfo`', 'Description, Id', null, false));
        $this->model->setDishCategoryList($this->dal->reading('`DishCategory`', 'Name, Id', null, false));
        $this->model->setPreferenceTypeList($this->dal->reading('`PreferenceType`', 'PreferenceTypeSelectAll', null, true));

        $this->dal->updateOne();
        $this->dal->readingOne();

        return $this->view($this->modelName, 'ReadingOne', $this->model);
    }

    public function updating()
    {
        if(isset($_POST['preferences-id'])) {
            $this->model->setId(filter_input(INPUT_POST, 'preferences-id', FILTER_SANITIZE_NUMBER_INT));
        } else {
            $this->model->setId($this->route->getId());
        }

        $this->dal->readingOne();

        return $this->view($this->modelName, 'Updating', $this->model);
    }
}
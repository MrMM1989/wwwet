<?php
namespace ModernWays\WWWET\Controller;

class Attendant extends \ModernWays\WWWET\Controller\AppController {

	public function __construct($route, $noticeBoard) {
		// Call parent constructor
		parent::__construct($route, $noticeBoard);

		// Set modelName & load model and DAL
		$this -> modelName = $this -> initModelName($this);
		$this -> loadModel($this -> noticeBoard);
		$this -> loadDal($this -> noticeBoard);
	}

	public function attendantGroupAdd() {
			
		if ($this -> isAuthenticated()) {
			
			$id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
			$name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

			$_SESSION['AttendantGroupList'][$id]['Id'] = $id;	
			$_SESSION['AttendantGroupList'][$id]['Name'] = $name;				
		} else {
			
			return $this -> view('Home', 'Index', null);
		}
	}
	
	public function delete(){
			
		if ($this -> isAuthenticated()) {
			
			$this -> model -> setId($this -> route -> getId());
		
			$this -> dal -> delete();
			$this -> dal -> readingAll();

			return $this -> view($this -> modelName, 'Index', $this -> model);				
		} else {
			
			return $this -> view('Home', 'Index', null);
		}
	}
	
	public function deleting(){
			
		if ($this -> isAuthenticated()) {
			
			$this -> model -> setId($this -> route -> getId());

			$this -> dal -> readingOne();

			return $this -> view($this -> modelName, 'Deleting', $this -> model);
		} else {
			
			return $this -> view('Home', 'Index', null);
		}
	}

	public function index() {
		
		if ($this -> isAuthenticated()) {
			
			$this -> dal -> readingAll();

			return $this -> view($this -> modelName, 'Index', $this -> model);	
			
		} else {
			
			return $this -> view('Home', 'Index', null);
		}
	}

	public function insert() {
			
		if ($this -> isAuthenticated()) {
			
			//Fetch the fields
			$this -> model -> setName(filter_input(INPUT_POST, 'attendant-name', FILTER_SANITIZE_STRING));
			$this -> model -> setUserName(filter_input(INPUT_POST, 'attendant-username', FILTER_SANITIZE_STRING));
			$this -> model -> setIdRole(filter_input(INPUT_POST, 'attendant-role', FILTER_SANITIZE_NUMBER_INT));

			//Hash the password
			$password = filter_input(INPUT_POST, 'attendant-password', FILTER_SANITIZE_STRING);
			$hashedPassword = password_hash($password, PASSWORD_DEFAULT);

			$this -> model -> setPassword($hashedPassword);

			//Set the list of the assigned groups
			$this -> model -> setAttendantGroupList($_SESSION['AttendantGroupList']);

			//Insert & return to index page
			$this -> dal -> insert();
	
			$this -> dal -> readingAll();
		
			//Unset session key
			unset($_SESSION['AttendantGroupList']);

			return $this -> view($this -> modelName, 'Index', $this -> model);			
		} else {
			
			return $this -> view('Home', 'Index', null);
		}
	}

	public function inserting() {
			
		if ($this -> isAuthenticated()) {
				
			$_SESSION['AttendantGroupList'] = array();
			$this -> model -> setGroupList($this -> dal -> reading('`Group`', 'Name, Id', null, false));
			$this -> model -> setRoleList($this -> dal -> reading('Role', 'Name, Id', null, false));
			return $this -> view($this -> modelName, 'Inserting', $this -> model);	
		} else {
			
			return $this -> view('Home', 'Index', null);
		}		
	}
	
	public function readList() {
			
		if ($this -> isAuthenticated()) {
				
			echo json_encode($_SESSION['AttendantGroupList']);

			exit();
		} else {
			
			return $this -> view('Home', 'Index', null);
		}
	}

	public function readingOne() {
			
		if ($this -> isAuthenticated()) {
			
			$this -> model -> setId($this -> route -> getId());

			$this -> dal -> readingOne();

			return $this -> view($this -> modelName, 'ReadingOne', $this -> model);				
		} else {
			
			return $this -> view('Home', 'Index', null);
		}
	}

	public function removeItem() {
			
		if ($this -> isAuthenticated()) {
			
			$id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);

			unset($_SESSION['AttendantGroupList'][$id]);			
		} else {
			
			return $this -> view('Home', 'Index', null);
		}
	}
	
	public function update(){
			
		if ($this -> isAuthenticated()) {
			
			//Fetch the fields
			$this -> model -> setId(filter_input(INPUT_POST, 'attendant-id', FILTER_SANITIZE_NUMBER_INT));
			$this -> model -> setName(filter_input(INPUT_POST, 'attendant-name', FILTER_SANITIZE_STRING));
			$this -> model -> setUserName(filter_input(INPUT_POST, 'attendant-username', FILTER_SANITIZE_STRING));
			$this -> model -> setIdRole(filter_input(INPUT_POST, 'attendant-role', FILTER_SANITIZE_NUMBER_INT));

			//Set the list of the assigned groups
			$this -> model -> setAttendantGroupList($_SESSION['AttendantGroupList']);

			//Update
			$this -> dal -> update();

			$this -> dal -> readingOne();
		
			//Unset session key
			unset($_SESSION['AttendantGroupList']);
			unset($_SESSION['OriginalAttendantGroupList']);

			return $this -> view($this -> modelName, 'ReadingOne', $this -> model);	
			
		} else {
			
			return $this -> view('Home', 'Index', null);
		}
	}

	public function updating() {
		
		if ($this -> isAuthenticated()) {
			
			if(isset($_SESSION['AttendantGroupList'])){
			
				unset($_SESSION['AttendantGroupList']);
			}
			if(isset($_SESSION['OriginalAttendantGroupList'])){
			
				unset($_SESSION['OriginalAttendantGroupList']);
			}			
			
			$this -> model -> setId($this -> route -> getId());
			$this -> model -> setGroupList($this -> dal -> reading('`Group`', 'Name, Id', null, false));
			$this -> model -> setRoleList($this -> dal -> reading('Role', 'Name, Id', null, false));
			
			$this -> dal -> readingOne();
		
			if(!empty($this->model->getAttendantGroupList())){
						
				foreach($this->model->getAttendantGroupList() as $item){
				
					$_SESSION['AttendantGroupList'][$item['Id']]['Id'] = $item['Id'];
					$_SESSION['AttendantGroupList'][$item['Id']]['Name'] = $item['Name'];
				}
			
				$_SESSION['OriginalAttendantGroupList'] = $_SESSION['AttendantGroupList'];
			}
				
			return $this -> view($this -> modelName, 'Updating', $this -> model);	
			
		} else {
			
			return $this -> view('Home', 'Index', null);
		}
	}
}

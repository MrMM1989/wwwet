<?php
namespace ModernWays\WWWET\Controller;

class Home extends \ModernWays\WWWET\Controller\AppController {
	public function __construct($route, $noticeBoard) {

		// Call parent constructor
		parent::__construct($route, $noticeBoard);

		// Set modelName & load model - Need to do it manually because of name
		// doesn't correspond to Attendant model
		$this -> modelName = $this -> initModelName($this);
		$this -> model = new \ModernWays\WWWET\Model\Attendant($this -> noticeBoard);
	}

	public function index() {
		
		if ($this -> isAuthenticated()) {

			return $this -> view('Admin', 'Index', $this -> model);

		} else {
			return $this -> view($this -> modelName, 'Index', $this -> model);
		}
	}

	public function login() {

		if ($this -> isAuthenticated()) {

			return $this -> view('Admin', 'Index', $this -> model);

		} else {

			$userName = filter_input(INPUT_POST, 'uname', FILTER_SANITIZE_STRING);
			$userPassword = filter_input(INPUT_POST, 'upassword', FILTER_SANITIZE_STRING);

			if ($this -> authentication -> login($userName, $userPassword)) {

				return $this -> view('Admin', 'Index', $this -> model);

			} else {
				$this -> model -> showLoginMessage('FAILED');
				return $this -> view('Home', 'Index', $this -> model);
			}
		}
	}

	public function logout() {
		$this -> session -> end();
		return $this -> view($this -> modelName, 'Index', $this -> model);
	}
	
	public function testDateHelper(){
		
		$dateHelper = new \ModernWays\WWWET\Helpers\DateHelper();
		
		$dateList = $dateHelper->getDateList();
		
		var_dump($dateList);
		exit();
	}
}

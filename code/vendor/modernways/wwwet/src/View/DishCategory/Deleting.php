<?php $partialView('Shared', 'AdminHeader'); ?>
<main>
    <section id="content">
        <h2>Een categorie verwijderen: <?php echo $model->getName(); ?></h2>
        <p class="warning">Weet je zeker dat je deze categorie wilt verwijderen?</p>
        <p class="buttonbartop">
            <a class="button buttongreen" href="index.php?uc=DishCategory-delete_<?php echo $model->getId(); ?>">
                <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Ja
            </a>
            <a class="button buttonspaceleft buttonred" href="index.php?uc=DishCategory-readingOne_<?php echo $model->getId(); ?>">
                <i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Nee
            </a>
        </p>

        <p><strong>Naam: </strong><?php echo $model->getName(); ?></p>
        <p><strong>Volgorde: </strong><?php echo $model->getOrder(); ?></p>
    </section>
</main>
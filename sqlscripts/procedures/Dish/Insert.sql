-- Dish --
use wwwet;
-- Stored Procedures --
-- Insert --
DELIMITER ;;
CREATE PROCEDURE `DishInsert`(
	OUT pId INT ,
	IN pIdDishCategory INT ,
	IN pName NVARCHAR (255) ,
	IN pByDefault BIT ,
	IN pMargin FLOAT ,
	IN pMonday BIT ,
	IN pTuesday BIT ,
	IN pWednesday BIT ,
	IN pThursday BIT ,
	IN pFriday BIT ,
	IN pSaturday BIT ,
	IN pSunday BIT 
    -- ,IN pInsertedBy NVARCHAR (255) 
)
BEGIN
INSERT INTO `Dish`
	(
		`Dish`.`IdDishCategory`,
		`Dish`.`Name`,
		`Dish`.`ByDefault`,
		`Dish`.`Margin`,
		`Dish`.`Monday`,
		`Dish`.`Tuesday`,
		`Dish`.`Wednesday`,
		`Dish`.`Thursday`,
		`Dish`.`Friday`,
		`Dish`.`Saturday`,
		`Dish`.`Sunday`,
		-- `Dish`.`InsertedBy`,
		`Dish`.`InsertedOn`
	)
	VALUES
	(
		pIdDishCategory,
		pName,
		pByDefault,
		pMargin,
		pMonday,
		pTuesday,
		pWednesday,
		pThursday,
		pFriday,
		pSaturday,
		pSunday,
		-- pInsertedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END ;;
DELIMITER ;;
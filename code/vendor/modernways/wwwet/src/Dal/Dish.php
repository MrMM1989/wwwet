<?php
/**
 * Created by PhpStorm.
 * User: Kurt
 * Date: 30/04/2016
 * Time: 14:48
 */
namespace ModernWays\WWWET\Dal;
class Dish extends \ModernWays\AnOrmApart\Dal
{
    public function __construct(\ModernWays\WWWET\Model\Dish $model, \ModernWays\AnOrmApart\Provider $provider)
    {
        $this->model = $model;
        parent::__construct($provider);
    }

    public function createOne()
    {
        $result = parent::createOne();
        $this->linkDietaryInfo();

        if ($result) {
            $this->model->showMessage('Dish', 'InsertSuccessMessage', true);
        } else {
            $this->model->showMessage('Dish', 'InsertFailedMessage', false);
        }
    }

    public function updateOne() {
        $result = parent::updateOne();

        if ($result) {
            $this->model->showMessage('Dish', 'UpdateSuccessMessage', true);
        } else {
            $this->model->showMessage('Dish', 'UpdateFailedMessage', false);
        }
    }

    public function DeleteOne() {
        $result = parent::DeleteOne();

        if($result) {
            $this->model->showMessage('Dish', 'DeleteSuccessMessage', true);
        } else {
            $this->model->showMessage('Dish', 'DeleteFailedMessage', false);
        }
    }

    public function readDishCategories() {
        if (!$this->provider->isConnected()) {
            $this->provider->open();
        }
        if ($this->provider->isConnected()) {
            try {
                $preparedStatement = $this->provider->getPdo()->prepare("CALL DishCategorySelectAll()");
                $preparedStatement->execute();
                $this->rowCount = $preparedStatement->rowCount();
                $this->model->setDishCategoryList($preparedStatement->fetchAll());

                if ($this->model->getList()) {
                    $this->model->getModelState()->crudNotice('readDishCategories', true, $this, null, $preparedStatement->errorInfo());
                } else {
                    $this->model->getModelState()->crudNotice('readDishCategories', false, $this, null, $preparedStatement->errorInfo());
                }

            } catch (\PDOException $e) {
                $this->model->getModelState()->crudNotice("readDishCategories, Error: {$e}", true, $this, $e);
            }
        }
    }

    public function linkDietaryInfo() {
        $list = $_SESSION['DietaryInfoList'];

            if (!$this -> provider -> isConnected()) {
                $this -> provider -> open();
            }
            if ($this -> provider -> isConnected()) {
                try {
                    foreach($list as $item) {
                        //Prepare the statement
                        $insertSql = 'INSERT INTO DishDiet (IdDish, IdDietaryINfo) VALUES (:idDish, :idDietaryInfo)';
                        $preparedStatement = $this -> provider -> getPdo() -> prepare($insertSql);
                        $preparedStatement -> bindValue(':idDish', $this->model->getId(), \PDO::PARAM_INT);
                        $preparedStatement -> bindValue(':idDietaryInfo', $item['Id'], \PDO::PARAM_INT);

                        //Execute statement
                        $preparedStatement -> execute();
                    }

                } catch (\PDOException $e) {
                    $this -> model -> getModelState() -> crudNotice('Insert', false, $this, $e, null);
                }
            } else {
                $this -> model -> getModelState() -> disconnected($this -> provider);
            }
    }

    public function updateDietaryInfo() {
        $this->readLinkedDietaryInfo();
        $currentDietaryInfo = $this->model->getLinkedDietaryInfo();
        $newDietaryInfo = $_SESSION['DietaryInfoList'];

        $toDelete = array();
        $toInsert = array();

        // Alles in current dat niet in new staat moet worden gedelete
        foreach ($currentDietaryInfo as $current) {
            $contains = false;
            foreach ($newDietaryInfo as $new) {
                if($current['Id'] == $new['Id']) {
                    $contains = true;
                }
            }
            if(!$contains) {
                array_push($toDelete, $current);
            }
        }
        // Alles in new dat niet in current zit moet worden geinsert
        foreach($newDietaryInfo as $new) {
            $contains = false;
            foreach($currentDietaryInfo as $current) {
                if($new['Id'] == $current['Id']){
                    $contains = true;
                }
            }
            if(!$contains) {
                array_push($toInsert, $new);
            }
        }

        // In database
        if (!$this -> provider -> isConnected()) {
            $this -> provider -> open();
        }
        if ($this -> provider -> isConnected()) {
            try {
                // Delete removed
                foreach($toDelete as $item) {
                    //Prepare the statement
                    $deleteSsl = 'DELETE FROM DishDiet WHERE IdDish = :idDish AND IdDietaryInfo = :idDietaryInfo';
                    $preparedStatement = $this -> provider -> getPdo() -> prepare($deleteSsl);
                    $preparedStatement -> bindValue(':idDish', $this->model->getId(), \PDO::PARAM_INT);
                    $preparedStatement -> bindValue(':idDietaryInfo', $item['Id'], \PDO::PARAM_INT);

                    //Execute statement
                    $preparedStatement -> execute();
                }

                // Insert new
                foreach($toInsert as $item) {
                    //Prepare the statement
                    $insertSql = 'INSERT INTO DishDiet (IdDish, IdDietaryInfo) VALUES (:idDish, :idDietaryInfo)';
                    $preparedStatement = $this -> provider -> getPdo() -> prepare($insertSql);
                    $preparedStatement -> bindValue(':idDish', $this->model->getId(), \PDO::PARAM_INT);
                    $preparedStatement -> bindValue(':idDietaryInfo', $item['Id'], \PDO::PARAM_INT);

                    //Execute statement
                    $preparedStatement -> execute();
                }
            } catch (\PDOException $e) {
                $this -> model -> getModelState() -> crudNotice('Update', false, $this, $e, null);
            }
        } else {
            $this -> model -> getModelState() -> disconnected($this -> provider);
        }
    }

    public function readLinkedDietaryInfo() {
        if (!$this -> provider -> isConnected()) {
            $this -> provider -> open();
        }
        if ($this -> provider -> isConnected()) {
            try {
                //Prepare the statement
                $findSql = 'call DishGetDietaryInfo(:idDish);';
                $preparedStatement = $this -> provider -> getPdo() -> prepare($findSql);
                $preparedStatement -> bindValue(':idDish', $this->model->getId(), \PDO::PARAM_INT);

                //Execute statement
                $preparedStatement->execute();

                //Set Model
                $this->model->setLinkedDietaryInfo($preparedStatement->fetchAll());

            } catch (\PDOException $e) {
                $this -> model -> getModelState() -> crudNotice('DishGetDietaryInfo', false, $this, $e, null);
            }
        } else {
            $this -> model -> getModelState() -> disconnected($this -> provider);
        }
    }
}

<?php
$succesMessage = false;
$title = '';
$message = '';
foreach($model->getModelState()->getBoard() as $boardItem){
    if($boardItem->getType() === 'SUCCESS'){
        $title = $boardItem->getName();
        $message = $boardItem->getText();
        $succesMessage = true;
    }
}
$partialView('Shared', 'AdminHeader');
?>
    <!--Dish\Index-->
    <main>
        <section id="content">
            <h2>Categorieën</h2>
            <?php if($succesMessage): ?>
                <p class="message messagegreen">
                    <strong><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<?php echo $title; ?></strong> <?php echo $message; ?>
                </p>
            <?php endif; ?>
            <p class="buttonbartop">
                <a class="button buttongreen" href="index.php?uc=DishCategory-Inserting">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Nieuwe Categorie
                </a>
            </p>

            <h3>Huidige Categorieën</h3>
            <?php if($model->getList()): ?>
                <table>
                    <thead>
                        <tr>
                            <th>Naam</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $index = 1;
                        foreach ($model->getList() as $item)
                        {
                            if($index%2 == 0) {
                        ?>
                                <tr class="alternate">
                        <?php
                            } else {
                        ?>
                                <tr>
                        <?php
                            }
                        ?>
                                <td>
                                    <?php echo $item['Name'];?>
                                </td>
                                <td>
                                    <a href="index.php?uc=DishCategory-readingOne_<?php echo $item['Id'];?>">Selecteer</a>
                                </td>
                            </tr>
                        <?php
                            $index++;
                        }
                        ?>
                    </tbody>
                </table>
            <?php else: ?>
                <p>
                    Geen categorieën gevonden in de database
                </p>
            <?php endif; ?>
            <p><a href="index.php?uc=Admin-index">Terug naar admin index</a></p>
        </section>
    </main>

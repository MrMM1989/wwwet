<?php $partialView('Shared', 'AdminHeader'); ?>
<!--KitchenMenu\Deleting-->
<main>
    <section id="content">
        <h2>Een menu verwijderen: <?php echo $model->getName();?></h2>
        <p class="buttonbartop">
            <a class="button buttongreen" href="index.php?uc=Menu-standardMenuDelete_<?php echo $model->getId(); ?>">
                <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Ja
            </a>
            <a class="button buttonspaceleft buttonred" href="index.php?uc=Menu-standardMenuReadingOne_<?php echo $model->getId(); ?>">
                <i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Nee
            </a>
        </p>

        <p><strong>Naam: </strong><?php echo $model->getName(); ?></p>
        <p><strong>Datum: </strong><?php echo $model->getDate(); ?></p>
        <p><strong>Tijdstip: </strong>
            <?php if ($model->getMealtimeList()): ?>
                <?php foreach($model->getMealtimeList() as $item): ?>
                    <?php if($item['Id'] == $model->getIdMealtime()): ?>
                        <?php echo $item['Name']; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </p>
        <h3>Gerechten: </h3>
        <?php if ($model->getMenuDishList()): ?>
            <ul class="read-list">
                <?php foreach ($model->getMenuDishList() as $item): ?>
                    <li><?php echo $item['Name'];?></li>
                <?php endforeach; ?>
            </ul>
        <?php else: ?>
            <p>Geen gerechten.</p>
        <?php endif; ?>
    </section>
</main>
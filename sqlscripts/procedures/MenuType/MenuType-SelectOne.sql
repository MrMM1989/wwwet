-- Menu Type Select One --
use wwwet;
DELIMITER ;;
CREATE PROCEDURE `MenuTypeSelectOne`(
	pId INT
)
BEGIN
SELECT * FROM `MenuType`
	WHERE `MenuType`.`Id` = pId;
END ;;
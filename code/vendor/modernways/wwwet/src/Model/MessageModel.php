<?php
namespace ModernWays\WWWET\Model;

class MessageModel extends \ModernWays\Mvc\Model
{
	protected $isValid;
	
	public function __construct($noticeBoard)
	{		
		// Call parent constructor
		parent::__construct($noticeBoard);
		
		$this->isValid = false;
	}
	
	public function getIsValid()
	{
		return $this->isValid;
	}
	
	public function importMessages(){
		
		$file = new \ModernWays\WWWET\Libraries\FileMessageHandler();
		
		return $file->readMessageFile();
	}
	
	public function showLoginMessage($type){
		
		$messages = $this->importMessages();
		
		switch($type){
			
			case 'FAILED':	
			default:
				$this->modelState->start('Inlogen gefaald!');
				$this->modelState->setType('LOGIN');
				$this->modelState->setText($messages['Login']['Failed']);
				$this->modelState->log();
				break;				 
		}		
	}
	
	public function showMessage($entity, $messageType, $isSuccess)
	{		
		$messages = $this->importMessages();
		
		if($isSuccess)
		{
			$this->modelState->start('Succes!');
			$this->modelState->setType('SUCCESS');
			$this->modelState->setText($messages[$entity][$messageType]);
			$this->modelState->log();
		}
		else 
		{
			$this->modelState->start('Helaas!');
			$this->modelState->setType('FAILED');
			$this->modelState->setText($messages[$entity][$messageType]);
			$this->modelState->log();
		}		
	}
}
-- -----------------------------------------------------
-- Table `Role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Role` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `InsertedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `InsertedOn` TIMESTAMP NULL DEFAULT NULL,
  `UpdatedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `UpdatedOn` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `uc_Role_Name` (`Name` ASC));


-- -----------------------------------------------------
-- Table `Attendant`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Attendant` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `InsertedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `InsertedOn` TIMESTAMP NULL DEFAULT NULL,
  `UpdatedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `UpdatedOn` TIMESTAMP NULL DEFAULT NULL,
  `UserName` VARCHAR(25) CHARACTER SET 'utf8' NOT NULL,
  `Password` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `IdRole` INT(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `uc_Attendant_Name` (`Name` ASC),
  INDEX `fk_AttendantRole_idx` (`IdRole` ASC),
  UNIQUE INDEX `UserName_UNIQUE` (`UserName` ASC),
  CONSTRAINT `fk_AttendantRole`
    FOREIGN KEY (`IdRole`)
    REFERENCES `Role` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE);


-- -----------------------------------------------------
-- Table `DishCategory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DishCategory` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `Order` INT(11) NOT NULL,
  `InsertedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `InsertedOn` TIMESTAMP NULL DEFAULT NULL,
  `UpdatedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `UpdatedOn` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `uc_DishCategory_Name` (`Name` ASC));


-- -----------------------------------------------------
-- Table `Dish`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Dish` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `IdDishCategory` INT(11) NOT NULL,
  `Name` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `ByDefault` BIT(1) NULL DEFAULT NULL,
  `Margin` FLOAT NULL DEFAULT NULL,
  `Monday` BIT(1) NULL DEFAULT NULL,
  `Tuesday` BIT(1) NULL DEFAULT NULL,
  `Wednesday` BIT(1) NULL DEFAULT NULL,
  `Thursday` BIT(1) NULL DEFAULT NULL,
  `Friday` BIT(1) NULL DEFAULT NULL,
  `Saturday` BIT(1) NULL DEFAULT NULL,
  `Sunday` BIT(1) NULL DEFAULT NULL,
  `InsertedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `InsertedOn` TIMESTAMP NULL DEFAULT NULL,
  `UpdatedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `UpdatedOn` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `uc_Dish_Name` (`Name` ASC, `IdDishCategory` ASC),
  INDEX `fk_DishIdDishCategory` (`IdDishCategory` ASC),
  CONSTRAINT `fk_DishIdDishCategory`
    FOREIGN KEY (`IdDishCategory`)
    REFERENCES `DishCategory` (`Id`));


-- -----------------------------------------------------
-- Table `Mealtime`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Mealtime` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `InsertedBy` VARCHAR(256) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `InsertedOn` TIMESTAMP NULL DEFAULT NULL,
  `UpdatedBy` VARCHAR(256) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `UpdatedOn` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `uc_Name` (`Name` ASC));


-- -----------------------------------------------------
-- Table `Group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Group` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NOT NULL,
  `InsertedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `InsertedOn` TIMESTAMP NULL DEFAULT NULL,
  `UpdatedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `UpdatedOn` TIMESTAMP NULL,
  PRIMARY KEY (`Id`));


-- -----------------------------------------------------
-- Table `Resident`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Resident` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(120) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `RoomNumber` VARCHAR(12) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `Diabetes` BIT(1) NULL DEFAULT NULL,
  `Diet` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `Extra` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `IdGroup` INT NOT NULL,
  `InsertedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `InsertedOn` TIMESTAMP NULL DEFAULT NULL,
  `UpdatedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `UpdatedOn` TIMESTAMP NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_ResidentGroup_idx` (`IdGroup` ASC),
  CONSTRAINT `fk_ResidentGroup`
    FOREIGN KEY (`IdGroup`)
    REFERENCES `Group` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `MenuType`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `MenuType` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `InsertedBy` VARCHAR(256) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `InsertedOn` TIMESTAMP NULL DEFAULT NULL,
  `UpdatedBy` VARCHAR(256) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `UpdatedOn` TIMESTAMP NULL DEFAULT NULL,
  `MenuTypecol` VARCHAR(45) NULL,
  PRIMARY KEY (`Id`));


-- -----------------------------------------------------
-- Table `Menu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Menu` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Date` DATE NOT NULL,
  `Name` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `IdMealtime` INT(11) NOT NULL,
  `IdMenuType` INT NOT NULL,
  `InsertedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `InsertedOn` TIMESTAMP NULL DEFAULT NULL,
  `UpdatedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `UpdatedOn` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_MenuIdMealtime` (`IdMealtime` ASC),
  INDEX `fk_MenuIdMenuType_idx` (`IdMenuType` ASC),
  CONSTRAINT `fk_MenuIdMealtime`
    FOREIGN KEY (`IdMealtime`)
    REFERENCES `Mealtime` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `fk_MenuIdMenuType`
    FOREIGN KEY (`IdMenuType`)
    REFERENCES `MenuType` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE);


-- -----------------------------------------------------
-- Table `Meal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Meal` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `IdMenu` INT(11) NOT NULL,
  `IdResident` INT(11) NOT NULL,
  `Comment` VARCHAR(1023) NULL,
  `InsertedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `InsertedOn` TIMESTAMP NULL DEFAULT NULL,
  `UpdatedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `UpdatedOn` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_MealMenuDishIdMenu` (`IdMenu` ASC),
  INDEX `fk_MealMenuDishIdResident` (`IdResident` ASC),
  CONSTRAINT `fk_MealMenuDishIdMenu`
    FOREIGN KEY (`IdMenu`)
    REFERENCES `Menu` (`Id`),
  CONSTRAINT `fk_MealMenuDishIdResident`
    FOREIGN KEY (`IdResident`)
    REFERENCES `Resident` (`Id`)
    ON DELETE CASCADE);


-- -----------------------------------------------------
-- Table `MenuDish`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `MenuDish` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `IdMenu` INT(11) NOT NULL,
  `IdDish` INT(11) NOT NULL,
  `Optional` BIT(1) NOT NULL,
  `Meal` CHAR(1) NULL DEFAULT NULL,
  `InsertedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `InsertedOn` TIMESTAMP NULL DEFAULT NULL,
  `UpdatedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `UpdatedOn` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_MenuDishIdMenu` (`IdMenu` ASC),
  INDEX `fk_MenuDishIdDish` (`IdDish` ASC),
  CONSTRAINT `fk_MenuDishIdDish`
    FOREIGN KEY (`IdDish`)
    REFERENCES `Dish` (`Id`),
  CONSTRAINT `fk_MenuDishIdMenu`
    FOREIGN KEY (`IdMenu`)
    REFERENCES `Menu` (`Id`));


-- -----------------------------------------------------
-- Table `DietaryInfo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DietaryInfo` (
  `Id` INT NOT NULL,
  `Description` VARCHAR(255) NULL,
  `InsertedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `InsertedOn` TIMESTAMP NULL DEFAULT NULL,
  `UpdatedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `UpdatedOn` TIMESTAMP NULL,
  PRIMARY KEY (`Id`));


-- -----------------------------------------------------
-- Table `PreferenceType`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `PreferenceType` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(255) NOT NULL,
  `InsertedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `InsertedOn` TIMESTAMP NULL DEFAULT NULL,
  `UpdatedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `UpdatedOn` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `Name_UNIQUE` (`Name` ASC));


-- -----------------------------------------------------
-- Table `Preference`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Preference` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `YesNo` TINYINT(1) NOT NULL DEFAULT '0',
  `IdDish` INT(11) NULL DEFAULT NULL,
  `IdDietaryInfo` INT(11) NULL DEFAULT NULL,
  `IdDishCategory` INT(11) NULL DEFAULT NULL,
  `IdPreferenceType` INT NOT NULL,
  `Preperation` VARCHAR(45) NULL DEFAULT NULL,
  `PortionSize` VARCHAR(45) NULL DEFAULT NULL,
  `InsertedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `InsertedOn` TIMESTAMP NULL DEFAULT NULL,
  `UpdatedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `UpdatedOn` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `uc_Preferences_Name` (`Name` ASC),
  INDEX `Dish_idx` (`IdDish` ASC),
  INDEX `FK_Preference_DietaryInfo_idx` (`IdDietaryInfo` ASC),
  INDEX `FK_Preference_DishCategory_idx` (`IdDishCategory` ASC),
  INDEX `FK_Preference_PreferenceType_idx` (`IdPreferenceType` ASC),
  CONSTRAINT `FK_Preference_Dish`
    FOREIGN KEY (`IdDish`)
    REFERENCES `Dish` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_Preference_DietaryInfo`
    FOREIGN KEY (`IdDietaryInfo`)
    REFERENCES `DietaryInfo` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_Preference_DishCategory`
    FOREIGN KEY (`IdDishCategory`)
    REFERENCES `DishCategory` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_Preference_PreferenceType`
    FOREIGN KEY (`IdPreferenceType`)
    REFERENCES `PreferenceType` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE);


-- -----------------------------------------------------
-- Table `ResidentPreferences`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ResidentPreferences` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `IdResident` INT(11) NOT NULL,
  `IdPreference` INT(11) NOT NULL,
  `DishAlternative` INT(11) NULL DEFAULT NULL,
  `Comment` VARCHAR(255) NULL DEFAULT NULL,
  `InsertedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `InsertedOn` TIMESTAMP NULL DEFAULT NULL,
  `UpdatedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `UpdatedOn` TIMESTAMP NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_ResidentPreferencesIdResident` (`IdResident` ASC),
  INDEX `fk_ResidentPreferencesIdPreference` (`IdPreference` ASC),
  INDEX `fk_ResidentPreferencesDish_idx` (`DishAlternative` ASC),
  CONSTRAINT `fk_ResidentPreferencesIdPreference`
    FOREIGN KEY (`IdPreference`)
    REFERENCES `Preference` (`Id`),
  CONSTRAINT `fk_ResidentPreferencesIdResident`
    FOREIGN KEY (`IdResident`)
    REFERENCES `Resident` (`Id`),
  CONSTRAINT `fk_ResidentPreferencesDish`
    FOREIGN KEY (`DishAlternative`)
    REFERENCES `Dish` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `DishDiet`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DishDiet` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `IdDish` INT(11) NULL,
  `IdDietaryInfo` INT(11) NULL,
  `InsertedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `InsertedOn` TIMESTAMP NULL DEFAULT NULL,
  `UpdatedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `UpdatedOn` TIMESTAMP NULL,
  PRIMARY KEY (`ID`),
  INDEX `FK_DishDiet_Dish_idx` (`IdDish` ASC),
  INDEX `FK_DishDiet_DietaryInfo_idx` (`IdDietaryInfo` ASC),
  CONSTRAINT `FK_DishDiet_Dish`
    FOREIGN KEY (`IdDish`)
    REFERENCES `Dish` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_DishDiet_DietaryInfo`
    FOREIGN KEY (`IdDietaryInfo`)
    REFERENCES `DietaryInfo` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `AttendantGroup`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `AttendantGroup` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `IdAttendant` INT NOT NULL,
  `IdGroup` INT NOT NULL,
  `InsertedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `InsertedOn` TIMESTAMP NULL DEFAULT NULL,
  `UpdatedBy` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `UpdatedOn` TIMESTAMP NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_AttendantGroupAttendant_idx` (`IdAttendant` ASC),
  INDEX `fk_AttendantGroupGroup_idx` (`IdGroup` ASC),
  CONSTRAINT `fk_AttendantGroupAttendant`
    FOREIGN KEY (`IdAttendant`)
    REFERENCES `Attendant` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_AttendantGroupGroup`
    FOREIGN KEY (`IdGroup`)
    REFERENCES `Group` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
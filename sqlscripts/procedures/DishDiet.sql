-- DishDiet
-- Create
DROP TABLE IF EXISTS `DishDiet`;
CREATE TABLE `DishDiet` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdDish` int(11) NOT NULL,
  `IdDietaryInfo` int(11) NOT NULL,
  `InsertedBy` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `InsertedOn` timestamp NULL DEFAULT NULL,
  `UpdatedBy` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `UpdatedOn` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_DishDiet_Dish` (`IdDish`),
  KEY `fk_DishDiet_DietaryInfo` (`IdDietaryInfo`),
  CONSTRAINT `fk_DishDiet_Dish` FOREIGN KEY (`IdDish`) REFERENCES `Dish` (`Id`),
  CONSTRAINT `fk_DishDiet_DietaryInfo` FOREIGN KEY (`IdDietaryInfo`) REFERENCES `DietaryInfo` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Stored Procedures
-- Insert
DELIMITER ;;
CREATE PROCEDURE `DishDietInsert`(
	OUT pId INT ,
    IN pIdDish INT,
    IN pIdDietaryInfo INT,
    IN pInsertedBy NVARCHAR (255)
)
BEGIN
INSERT INTO `DishDiet`
	(
		`DishDiet`.`IdDish`,
		`DishDiet`.`IdDietaryInfo`,
        `DishDiet`.`InsertedBy`,
		`DishDiet`.`InsertedOn`
	)
	VALUES
	(
		pIdDish,
		pIdDietaryInfo,
        pInsertedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END ;;

-- Update
DELIMITER ;;
CREATE PROCEDURE `DishDietUpdate`(
	pId INT ,
    IN pIdDish INT,
    IN pIdDietaryInfo INT
)
BEGIN
UPDATE `DishDiet`
	SET
		`IdDish` = pIdDish,
        `IdDietaryInfo` = pIdDietaryInfo,
		`UpdatedBy` = pUpdatedBy,
		`UpdatedOn` = NOW()
	WHERE `DishDiet`.`Id` = pId;
END ;;

-- Delete
DELIMITER ;;
CREATE PROCEDURE `DishDietDelete`(
	 pId INT 
)
BEGIN
DELETE FROM `DishDiet`
	WHERE `DishDiet`.`Id` = pId;
END ;;

-- Count
DELIMITER ;;
CREATE PROCEDURE `DishDietCount`(
)
BEGIN
	SELECT count(*) FROM `DishDiet`;
END ;;

-- SelectAll
DELIMITER ;;
CREATE PROCEDURE `DishDietSelectAll`(
)
BEGIN
SELECT *
	FROM `DishDiet`;
END ;;

-- SelectOne
DELIMITER ;;
CREATE PROCEDURE `DishDietSelectOne`(
	 pId INT 
)
BEGIN
SELECT * FROM `DishDiet`
	WHERE `DishDiet`.`Id` = pId;
END ;;
<?php
/**
 * Created by PhpStorm.
 * User: Kurt
 * Date: 1/05/2016
 * Time: 13:52
 */

namespace ModernWays\WWWET\Model;


class Preferences extends \ModernWays\WWWET\Model\MessageModel
{
    private $id;
    private $name;
    private $yesNo;
    private $idDish;
    private $idDietaryInfo;
    private $idDishCategory;
    private $idPreferenceType;
    private $preperation;
    private $portionSize;

    protected $dishList;
    protected $dietaryInfoList;
    protected $dishCategoryList;
    protected $preferenceTypeList;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getYesNo()
    {
        return $this->yesNo;
    }

    /**
     * @param mixed $yesNo
     */
    public function setYesNo($yesNo)
    {
        $this->yesNo = $yesNo;
    }

    /**
     * @return mixed
     */
    public function getIdDish()
    {
        return $this->idDish;
    }

    /**
     * @param mixed $idDish
     */
    public function setIdDish($idDish)
    {
        $this->idDish = $idDish;
    }

    /**
     * @return mixed
     */
    public function getIdDietaryInfo()
    {
        return $this->idDietaryInfo;
    }

    /**
     * @param mixed $idDietaryInfo
     */
    public function setIdDietaryInfo($idDietaryInfo)
    {
        $this->idDietaryInfo = $idDietaryInfo;
    }

    /**
     * @return mixed
     */
    public function getIdDishCategory()
    {
        return $this->idDishCategory;
    }

    /**
     * @param mixed $idDishCategory
     */
    public function setIdDishCategory($idDishCategory)
    {
        $this->idDishCategory = $idDishCategory;
    }

    /**
     * @return mixed
     */
    public function getIdPreferenceType()
    {
        return $this->idPreferenceType;
    }

    /**
     * @param mixed $idPreferenceType
     */
    public function setIdPreferenceType($idPreferenceType)
    {
        $this->idPreferenceType = $idPreferenceType;
    }

    /**
     * @return mixed
     */
    public function getPreperation()
    {
        return $this->preperation;
    }

    /**
     * @param mixed $preperation
     */
    public function setPreperation($preperation)
    {
        $this->preperation = $preperation;
    }

    /**
     * @return mixed
     */
    public function getPortionSize()
    {
        return $this->portionSize;
    }

    /**
     * @param mixed $portionSize
     */
    public function setPortionSize($portionSize)
    {
        $this->portionSize = $portionSize;
    }

    /**
     * @return mixed
     */
    public function getDishList()
    {
        return $this->dishList;
    }

    /**
     * @param mixed $dishList
     */
    public function setDishList($dishList)
    {
        $this->dishList = $dishList;
    }

    /**
     * @return mixed
     */
    public function getDietaryInfoList()
    {
        return $this->dietaryInfoList;
    }

    /**
     * @param mixed $dietaryInfoList
     */
    public function setDietaryInfoList($dietaryInfoList)
    {
        $this->dietaryInfoList = $dietaryInfoList;
    }

    /**
     * @return mixed
     */
    public function getDishCategoryList()
    {
        return $this->dishCategoryList;
    }

    /**
     * @param mixed $dishCategoryList
     */
    public function setDishCategoryList($dishCategoryList)
    {
        $this->dishCategoryList = $dishCategoryList;
    }

    /**
     * @return mixed
     */
    public function getPreferenceTypeList()
    {
        return $this->preferenceTypeList;
    }

    /**
     * @param mixed $preferenceTypeList
     */
    public function setPreferenceTypeList($preferenceTypeList)
    {
        $this->preferenceTypeList = $preferenceTypeList;
    }



}
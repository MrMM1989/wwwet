<?php 
	$failedMessage = false;
	$succesMessage = false;
	$title = '';
	$message = '';
	foreach($model->getModelState()->getBoard() as $boardItem){
		if($boardItem->getType() === 'SUCCESS'){
				$title = $boardItem->getName();
				$message = $boardItem->getText();
				$succesMessage = true;			
		}
		if($boardItem->getType() === 'FAILED'){
				$title = $boardItem->getName();
				$message = $boardItem->getText();
				$failedMessage = true;			
		}
	}
	$partialView('Shared', 'AdminHeader'); 
?>
<main>
	<section id="content">
		<h2>Details van een bewoner bekijken: <?php echo $model->getName(); ?></h2>
		<?php if($succesMessage): ?>
			<p class="message messagegreen">
				<strong><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<?php echo $title; ?></strong> <?php echo $message; ?>
			</p>
		<?php endif; ?>
		<?php if($failedMessage): ?>
			<p class="message messagered">
				<strong><i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<?php echo $title; ?></strong> <?php echo $message; ?>
			</p>
		<?php endif; ?>
		<p class="buttonbartop">
			<a class="button buttonyellow" href="index.php?uc=Resident-updating_<?php echo $model->getId(); ?>">
				<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Bewoner wijzigen
			</a>
			<a class="button buttonspaceleft buttonred" href="index.php?uc=Resident-deleting_<?php echo $model->getId(); ?>">
				<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Bewoner verwijderen
			</a>
		</p>
		<p class="buttonbartop">
			<a class="button buttonblue" href="index.php?uc=Resident-alterPreferenceList_<?php echo $model->getId(); ?>">
				<i class="fa fa-list" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Bewerk Voorkeuren
			</a>
		</p>
		<p>
			<strong>Naam: </strong><?php echo $model->getName(); ?>
		</p>
		<p>
			<strong>Kamernummer: </strong><?php echo $model->getRoomNumber(); ?>
		</p>
		<p>
			<strong>Diabetes: </strong> <?php echo ($model->getDiabetes())? 'Ja' : 'Nee' ?> 
		</p>
		<p>
			<strong>Groep: </strong><?php echo $model->getNameGroup(); ?>
		</p>
		<?php if(!empty($model->getDiet())): ?>
			<p>
				<strong>Dieet: </strong><?php echo $model->getDiet(); ?>
			</p>
		<?php endif; ?>
		<?php if(!empty($model->getExtra())): ?>
			<p>
				<strong>Extra: </strong><?php echo $model->getExtra(); ?>
			</p>
		<?php endif; ?>
		<p><a href="index.php?uc=Resident-index">Terug naar overzicht bewoners</a></p>
	</section>
</main>
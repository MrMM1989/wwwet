<?php
/**
 * Created by PhpStorm.
 * User: Kurt
 * Date: 1/05/2016
 * Time: 13:42
 */

namespace ModernWays\WWWET\Dal;


class DishDiet extends \ModernWays\AnOrmApart\Dal
{
    public function __construct(\ModernWays\WWWET\Model\DishDiet $model, \ModernWays\AnOrmApart\Provider $provider)
    {
        $this->model = $model;
        parent::__construct($provider);
    }

    public function readDishes() {
        if (!$this->provider->isConnected()) {
            $this->provider->open();
        }
        if ($this->provider->isConnected()) {
            try {
                $preparedStatement = $this->provider->getPdo()->prepare("CALL DishSelectAll()");
                $preparedStatement->execute();
                $this->rowCount = $preparedStatement->rowCount();
                $this->model->setDishList($preparedStatement->fetchAll());

                if ($this->model->getList()) {
                    $this->model->getModelState()->crudNotice('DishDiet: readDishes', true, $this, null, $preparedStatement->errorInfo());
                } else {
                    $this->model->getModelState()->crudNotice('DishDiet: readDishes', false, $this, null, $preparedStatement->errorInfo());
                }

            } catch (\PDOException $e) {
                $this->model->getModelState()->crudNotice("DishDiet: readDishes, Error: {$e}", true, $this, $e);
            }
        }
    }

    public function readDietaryInfo() {
        if (!$this->provider->isConnected()) {
            $this->provider->open();
        }
        if ($this->provider->isConnected()) {
            try {
                $preparedStatement = $this->provider->getPdo()->prepare("CALL DietaryInfoSelectAll()");
                $preparedStatement->execute();
                $this->rowCount = $preparedStatement->rowCount();
                $this->model->setDishList($preparedStatement->fetchAll());

                if ($this->model->getList()) {
                    $this->model->getModelState()->crudNotice('DishDiet: readDietaryInfo', true, $this, null, $preparedStatement->errorInfo());
                } else {
                    $this->model->getModelState()->crudNotice('DishDiet: readDietaryInfo', false, $this, null, $preparedStatement->errorInfo());
                }

            } catch (\PDOException $e) {
                $this->model->getModelState()->crudNotice("DishDiet: readDietaryInfo, Error: {$e}", true, $this, $e);
            }
        }
    }
}
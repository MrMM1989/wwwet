use wwwet;

-- Insert
DELIMITER ;;
CREATE PROCEDURE `MealtimeInsert`(
	OUT pId INT ,
	IN pName NVARCHAR (255)
    -- , IN pInsertedBy NVARCHAR (255) 
)
BEGIN
INSERT INTO `Mealtime`
	(
		`Mealtime`.`Name`,
		-- `Mealtime`.`InsertedBy`,
		`Mealtime`.`InsertedOn`
	)
	VALUES
	(
		pName,
		-- pInsertedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END ;;
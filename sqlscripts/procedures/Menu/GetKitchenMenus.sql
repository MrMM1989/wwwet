-- Menu --
use wwwet;
-- Stored Procedures --

-- GetKitchenMenuDishes --
DELIMITER ;;
CREATE PROCEDURE `MenuGetKitchenMenus`(
)
BEGIN
	select * from Menu
	where Menu.IdMenuType = 1
	order by Menu.Date;
END ;;s
<?php
namespace ModernWays\WWWET\Helpers;

class DateHelper {
	
	public function getDateList(){
		
		$dateList = array();
		
		//Add the dates from 7 days ago until yesterday to list
		$daycounter = -7;
		for($daycounter; $daycounter < 0; $daycounter++){
			
			$time = strtotime(+ $daycounter.'days');
			
			$dateList[$daycounter + 7]['timestamp'] = $time;
			$dateList[$daycounter + 7]['database-format'] = date('Y-m-d', $time);
			$dateList[$daycounter + 7]['ui-format'] = date('l d-m-Y', $time);
		}
		
		//Add dates from today to 30 days further to list		
		$daycounter = 0;
		for($daycounter; $daycounter < 31; $daycounter++){
			
			$time = strtotime(+ $daycounter.'days');
			
			$dateList[$daycounter + 7]['timestamp'] = $time;
			$dateList[$daycounter + 7]['database-format'] = date('Y-m-d', $time);
			$dateList[$daycounter + 7]['ui-format'] = date('l d-m-Y', $time);
		}
		
		return $dateList;
	}
}
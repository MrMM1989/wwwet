/**
 * Created by Kurt on 21/05/2016.
 */

function show() {
    var dropdown = document.getElementById('preferences-idPreferenceType');
    var index = dropdown.selectedIndex;
    var selected = dropdown.options[index].text;

    document.getElementById('Ingredient').style.display = 'none';
    document.getElementById('Gerecht').style.display = 'none';
    document.getElementById('Type Gerecht').style.display = 'none';
    document.getElementById('Bereiding').style.display = 'none';
    document.getElementById('Portie').style.display = 'none';
    document.getElementById('GerechtOfType').style.display = 'none';

    document.getElementById(selected).style.display = 'block';

    if (selected == 'Bereiding' || selected == 'Portie'){
        document.getElementById('GerechtOfType').style.display = 'block';
    }
}

function choose() {
    var radio = document.getElementsByName('preferences-gerechtOfType');
    var choice;

    for(var i =0; i < radio.length; i++) {
        if(radio[i].checked) {
            choice = radio[i].value;
        }
    }

    document.getElementById('Gerecht').style.display = 'none';
    document.getElementById('Type Gerecht').style.display = 'none';

    document.getElementById(choice).style.display = 'block';
}

document.addEventListener('DOMContentLoaded', function(event) {
    show();
    var dropdown = document.getElementById('preferences-idPreferenceType');
    dropdown.onchange = show;

    var radio = document.getElementsByName('preferences-gerechtOfType');
    for(var i =0; i <= radio.length; i++) {
        radio[i].onchange = choose;
    }
});
<?php
namespace ModernWays\WWWET\Controller;

class DishCategory extends \ModernWays\Mvc\Controller
{
    public function index()
    {
        $model = new \ModernWays\WWWET\Model\DishCategory($this->noticeBoard);
        $provider = new \ModernWays\WWWET\Dal\Provider('wwwet', $this->noticeBoard);
        $dal = new \ModernWays\WWWET\Dal\DishCategory($model, $provider);

        $dal->readingAll();
        return $this->view('DishCategory', 'Index', $model);
    }

    public function readingOne()
    {
        $model = new \ModernWays\WWWET\Model\DishCategory($this->noticeBoard);
        $provider = new \ModernWays\WWWET\Dal\Provider('wwwet', $this->noticeBoard);
        $dal = new \ModernWays\WWWET\Dal\DishCategory($model, $provider);

        $model->setId($this->route->getId());
        $dal->readingOne();
        return $this->view('DishCategory', 'ReadingOne', $model);
    }

    public function deleting() {
        $model = new \ModernWays\WWWET\Model\DishCategory($this->noticeBoard);
        $provider = new \ModernWays\WWWET\Dal\Provider('wwwet', $this->noticeBoard);
        $dal = new \ModernWays\WWWET\Dal\DishCategory($model, $provider);

        if(isset($_POST['Dish-Id'])) {
            $model->setId(filter_input(INPUT_POST, 'Dish-Id', FILTER_SANITIZE_NUMBER_INT));
        } else {
            $model->setId($this->route->getId());
        }

        $dal->readingOne();

        return $this->view('DishCategory', 'Deleting', $model);
    }

    public function delete() {
        $model = new \ModernWays\WWWET\Model\DishCategory($this->noticeBoard);
        $provider = new \ModernWays\WWWET\Dal\Provider('wwwet', $this->noticeBoard);
        $dal = new \ModernWays\WWWET\Dal\DishCategory($model, $provider);

        // Delete
        if(isset($_POST['DishCategory-Id'])) {
            $model->setId(filter_input(INPUT_POST, 'DishCategory-Id', FILTER_SANITIZE_NUMBER_INT));
        } else {
            $model->setId($this->route->getId());
        }

        $dal->deleteOne();
        $dal->readingAll();
        return $this->view('DishCategory', 'Index', $model);
    }

    public function Inserting() {
        $model = new \ModernWays\WWWET\Model\DishCategory($this->noticeBoard);

        return $this->view('DishCategory', 'Inserting', $model);
    }

    public function Insert() {
        $model = new \ModernWays\WWWET\Model\DishCategory($this->noticeBoard);
        $provider = new \ModernWays\WWWET\Dal\Provider('wwwet', $this->noticeBoard);
        $dal = new \ModernWays\WWWET\Dal\DishCategory($model, $provider);

        // Nieuwe Categorie Maken
        $model->setName(filter_input(INPUT_POST, 'DishCategory-Name', FILTER_SANITIZE_STRING));
        $model->setOrder(filter_input(INPUT_POST, 'DishCategory-Order', FILTER_SANITIZE_NUMBER_INT));
        // Inserted By ??

        // Categorie inserten in database
        $dal->createOne();
        $dal->readingAll();
        return $this->view('DishCategory', 'Index', $model);

    }

    public function updating() {
        $model = new \ModernWays\WWWET\Model\DishCategory($this->noticeBoard);
        $provider = new \ModernWays\WWWET\Dal\Provider('wwwet', $this->noticeBoard);
        $dal = new \ModernWays\WWWET\Dal\DishCategory($model, $provider);

        // Select te updaten
        if(isset($_POST['DishCategory-Id'])) {
            $model->setId(filter_input(INPUT_POST, 'DishCategory-Id', FILTER_SANITIZE_NUMBER_INT));
        } else {
            $model->setId($this->route->getId());
        }

        $dal->readingOne();;

        return $this->view('DishCategory', 'Updating', $model);
    }

    public function update() {
        $model = new \ModernWays\WWWET\Model\DishCategory($this->noticeBoard);
        $provider = new \ModernWays\WWWET\Dal\Provider('wwwet', $this->noticeBoard);
        $dal = new \ModernWays\WWWET\Dal\DishCategory($model, $provider);

        // Categorie updaten
        $model->setId(filter_input(INPUT_POST, 'DishCategory-Id', FILTER_SANITIZE_NUMBER_INT));
        $model->setName(filter_input(INPUT_POST, 'DishCategory-Name', FILTER_SANITIZE_STRING));
        $model->setOrder(filter_input(INPUT_POST, 'DishCategory-Order', FILTER_SANITIZE_NUMBER_INT));

        // Categorie updaten in database
        $dal->updateOne();

        // Refresh
        $dal->readingAll();
        return $this->view('DishCategory', 'ReadingOne', $model);
    }
}
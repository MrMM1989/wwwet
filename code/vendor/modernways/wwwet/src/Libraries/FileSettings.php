<?php
namespace ModernWays\WWWET\Libraries;

class FileSettings {
	
	protected $applicationDir;
	
	public function __construct()
	{
		$this->applicationDir = str_replace('\vendor\modernways\wwwet\src\Libraries', '', __DIR__);
	}
}

<?php $partialView('Shared', 'AdminHeader'); ?>
<main id="tile">
	<section class="row">
		<section class="field bluegreen">
			<!--<p><a href="index.php?uc=Meal-index">Maaltijden</a></p>-->
		</section>
		<section class="field bluegreen">
			<p><a href="index.php?uc=Menu-standardMenuIndex">Standaardmenu</a></p>
		</section>
		<section class="field bluegreen">
			<p><a href="index.php?uc=Menu-kitchenMenuIndex">Keukenmenu</a></p>
		</section>
		<section class="field bluegreen">
			<!--<p><a href="index.php?uc=Report-index">Rapporten</a></p>-->
		</section>
	</section>
	<section class="row">
		<section class="field green">
			<p><a href="index.php?uc=Resident-index">Bewoners</a></p>
		</section>
		<section class="field green">
			<p><a href="index.php?uc=Preferences-index">Voorkeuren</a></p>
		</section>
		<section class="field green">
			<p><a href="index.php?uc=Dish-index">Gerechten</a></p>
		</section>
		<section class="field green">
			<p><a href="index.php?uc=DishCategory-index">Categorieën</a></p>
		</section>
	</section>
	<section class="row">
		<section class="field darkorange">
			<p><a href="index.php?uc=Mealtime-index">Tijdstippen</a></p>
		</section>
		<section class="field darkorange">
			<p><a href="index.php?uc=Group-index">Groepen</a></p>
		</section>
		<section class="field darkorange">
			<p><a href="index.php?uc=Attendant-index">Beheerders</a></p>
		</section>
		<section class="field darkorange">
			<p><a href="index.php?uc=Role-index">Rollen</a></p>
		</section>
	</section>
</main>
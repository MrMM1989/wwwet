<?php
/**
 * Created by PhpStorm.
 * User: Kurt
 * Date: 1/05/2016
 * Time: 13:57
 */

namespace ModernWays\WWWET\Dal;


class ResidentPreferences extends \ModernWays\AnOrmApart\Dal
{
    public function __construct(\ModernWays\WWWET\Model\ResidentPreferences $model, \ModernWays\AnOrmApart\Provider $provider)
    {
        $this->model = $model;
        parent::__construct($provider);
    }

    public function readResidents() {
        if (!$this->provider->isConnected()) {
            $this->provider->open();
        }
        if ($this->provider->isConnected()) {
            try {
                $preparedStatement = $this->provider->getPdo()->prepare("CALL ResidentSelectAll()");
                $preparedStatement->execute();
                $this->rowCount = $preparedStatement->rowCount();
                $this->model->setResidentList($preparedStatement->fetchAll());

                if ($this->model->getList()) {
                    $this->model->getModelState()->crudNotice('Dal ResidentPreferences: readResidents', true, $this, null, $preparedStatement->errorInfo());
                } else {
                    $this->model->getModelState()->crudNotice('Dal ResidentPreferences: readResidents', false, $this, null, $preparedStatement->errorInfo());
                }

            } catch (\PDOException $e) {
                $this->model->getModelState()->crudNotice("Dal ResidentPreferences: readResidents, Error: {$e}", true, $this, $e);
            }
        }
    }

    public function readPreferences() {
        if (!$this->provider->isConnected()) {
            $this->provider->open();
        }
        if ($this->provider->isConnected()) {
            try {
                $preparedStatement = $this->provider->getPdo()->prepare("CALL PreferenceSelectAll()");
                $preparedStatement->execute();
                $this->rowCount = $preparedStatement->rowCount();
                $this->model->setResidentList($preparedStatement->fetchAll());

                if ($this->model->getList()) {
                    $this->model->getModelState()->crudNotice('Dal ResidentPreferences: readPreferences', true, $this, null, $preparedStatement->errorInfo());
                } else {
                    $this->model->getModelState()->crudNotice('Dal ResidentPreferences: readPreferences', false, $this, null, $preparedStatement->errorInfo());
                }

            } catch (\PDOException $e) {
                $this->model->getModelState()->crudNotice("Dal ResidentPreferences: readPreferences, Error: {$e}", true, $this, $e);
            }
        }
    }
}
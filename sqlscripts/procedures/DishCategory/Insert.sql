-- DishCategory
use wwwet;

-- Stored Procedures
-- Insert
DELIMITER ;;
CREATE PROCEDURE `DishCategoryInsert`(
	OUT pId INT ,
	IN pName NVARCHAR (255) ,
	IN pOrder INT
)
BEGIN
INSERT INTO `DishCategory`
	(
		`DishCategory`.`Name`,
		`DishCategory`.`Order`,
		`DishCategory`.`InsertedOn`
	)
	VALUES
	(
		pName,
		pOrder,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END ;;
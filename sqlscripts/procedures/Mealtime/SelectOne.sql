use wwwet;

-- Select One
DELIMITER ;;
CREATE PROCEDURE `MealtimeSelectOne`(
	 pId INT 
)
BEGIN
SELECT * FROM `Mealtime`
	WHERE `Mealtime`.`Id` = pId;
END ;;
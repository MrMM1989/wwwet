USE wwwet;
-- -----------------------------------------------------
-- Create Procedure GroupReadingOne
-- -----------------------------------------------------
DROP PROCEDURE IF EXISTS GroupSelectOne;
DELIMITER //
CREATE PROCEDURE GroupSelectOne(
	pId INT
)
BEGIN
SELECT Id, Name 
FROM `Group`
WHERE Id = pId;
END //
DELIMITER ;
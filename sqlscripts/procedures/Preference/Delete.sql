-- Preference --
use wwwet;
-- Stored Procedures --

-- Delete --
DELIMITER ;;
CREATE PROCEDURE `PreferencesDelete`(
	 pId INT 
)
BEGIN
DELETE FROM `Preference`
	WHERE `Preference`.`Id` = pId;
END ;;
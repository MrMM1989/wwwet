<?php
/**
 * Created by PhpStorm.
 * User: Kurt
 * Date: 1/05/2016
 * Time: 12:27
 */
namespace ModernWays\WWWET\Model;

class Resident extends \ModernWays\WWWET\Model\MessageModel {
	private $id;
	private $name;
	private $roomNumber;
	private $diabetes;
	private $diet;
	private $extra;
	private $idGroup;
	private $nameGroup;

	protected $dishList;
	protected $groupList;
	protected $preferenceList;
	protected $residentPreferenceList;

	public function getId() {
		return $this -> id;
	}

	public function setId($id) {
		$this -> id = $id;
	}

	public function getName() {
		return $this -> name;
	}

	public function setName($name) {
		$this -> name = $name;
	}

	public function getRoomNumber() {
		return $this -> roomNumber;
	}

	public function setRoomNumber($roomNumber) {
		$this -> roomNumber = $roomNumber;
	}

	public function getDiabetes() {
		return $this -> diabetes;
	}

	public function setDiabetes($diabetes) {
		$this -> diabetes = $diabetes;
	}

	public function getDiet() {
		return $this -> diet;
	}

	public function setDiet($diet) {
		$this -> diet = $diet;
	}

	public function getExtra() {
		return $this -> extra;
	}

	public function setExtra($extra) {
		$this -> extra = $extra;
	}

	public function getIdGroup() {
		return $this->idGroup;
	}
	
	public function setIdGroup($idGroup){
		$this->idGroup = $idGroup;
	}
	
	public function getNameGroup(){
		return $this->nameGroup;
	}
	
	public function setNameGroup($nameGroup){
		$this->nameGroup = $nameGroup;
	}
	
	public function getDishList(){
		return $this->dishList;
	}
	
	public function setDishList($dishList){
		$this->dishList = $dishList;
	}
	
	public function getGroupList(){
		return $this->groupList;
	}
	
	public function setGroupList($groupList){
		$this->groupList = $groupList;
	}
	
	public function getPreferenceList(){
		return $this->preferenceList;
	}
	
	public function setPreferenceList($preferenceList){
		$this->preferenceList = $preferenceList;
	}
	
	public function getResidentPreferenceList(){
		return $this->residentPreferenceList;
	}
	
	public function setResidentPreferenceList($residentPreferenceList){
		$this->residentPreferenceList = $residentPreferenceList;
	}
}

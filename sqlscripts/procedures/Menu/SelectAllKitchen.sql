-- Dish --
use wwwet;
-- Stored Procedures --

-- Select All --
DELIMITER ;;
CREATE PROCEDURE `MenuSelectAllKitchen`(
)
BEGIN
SELECT `Menu`.*, `Mealtime`.Name as `MealtimeName` 
FROM `Menu`
LEFT OUTER JOIN `Mealtime` ON `Menu`.`IdMealtime` = `Mealtime`.`Id`
LEFT OUTER JOIN `MenuType` ON `Menu`.`IdMenuTYpe` = `MenuType`.`Id`
WHERE `Menu`.`IdMenuType` = 1;
END ;;
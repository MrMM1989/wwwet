-- Dish --
use wwwet;
-- Stored Procedures --

-- Select All --
DELIMITER ;;
CREATE PROCEDURE `MenuGetDishes`(
	pIdMenu INT
)
BEGIN
    select Dish.Id, Dish.Name from Dish
    inner join MenuDish
		on MenuDish.IdDish = Dish.Id
	where idMenu = pIdMenu;
END ;;

-- Preference --
use wwwet;
-- Stored Procedures --

-- Select One --
DELIMITER ;;
CREATE PROCEDURE `PreferencesSelectOne`(
	 pId INT 
)
BEGIN
SELECT * FROM `Preference`
	WHERE `Preference`.`Id` = pId;
END ;;
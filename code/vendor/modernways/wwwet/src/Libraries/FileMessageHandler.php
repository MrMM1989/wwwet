<?php
namespace ModernWays\WWWET\Libraries;

class FileMessageHandler extends \ModernWays\WWWET\Libraries\FileSettings{
	
	private $messageFileLocation;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->messageFileLocation = $this->applicationDir.'\program-data\messages.json';
	}
	
	public function readMessageFile()
	{
		$messages = file_get_contents($this->messageFileLocation);
		
		return json_decode($messages, true);
	}
}
USE wwwet;
-- -----------------------------------------------------
-- Create View vwSelectResidentPreferenceList
-- -----------------------------------------------------
DROP VIEW IF EXISTS vwSelectResidentPreferenceList;
CREATE VIEW vwSelectResidentPreferenceList AS
SELECT rp.Id, rp.IdResident, rp.IdPreference, p.Name AS NamePreference, rp.DishAlternative, d.Name AS NameDish
FROM ResidentPreferences AS rp
INNER JOIN Preference AS p 
ON rp.IdPreference = p.id
LEFT OUTER JOIN Dish AS d
ON rp.DishAlternative = d.Id
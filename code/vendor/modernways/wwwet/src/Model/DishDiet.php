<?php
/**
 * Created by PhpStorm.
 * User: Kurt
 * Date: 1/05/2016
 * Time: 13:39
 */

namespace ModernWays\WWWET\Model;


class DishDiet extends \ModernWays\Mvc\Model
{
    private $id;
    private $idDish;
    private $idDietaryInfo;
    protected $dishList;
    protected $dietaryInfoList;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdDish()
    {
        return $this->idDish;
    }

    /**
     * @param mixed $idDish
     */
    public function setIdDish($idDish)
    {
        $this->idDish = $idDish;
    }

    /**
     * @return mixed
     */
    public function getIdDietaryInfo()
    {
        return $this->idDietaryInfo;
    }

    /**
     * @param mixed $idDietaryInfo
     */
    public function setIdDietaryInfo($idDietaryInfo)
    {
        $this->idDietaryInfo = $idDietaryInfo;
    }

    /**
     * @return mixed
     */
    public function getDishList()
    {
        return $this->dishList;
    }

    /**
     * @param mixed $dishList
     */
    public function setDishList($dishList)
    {
        $this->dishList = $dishList;
    }

    /**
     * @return mixed
     */
    public function getDietaryInfoList()
    {
        return $this->dietaryInfoList;
    }

    /**
     * @param mixed $dietaryInfoList
     */
    public function setDietaryInfoList($dietaryInfoList)
    {
        $this->dietaryInfoList = $dietaryInfoList;
    }

}
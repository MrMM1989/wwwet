<?php
$succesMessage = false;
$title = '';
$message = '';
foreach($model->getModelState()->getBoard() as $boardItem){
    if($boardItem->getType() === 'SUCCESS'){
        $title = $boardItem->getName();
        $message = $boardItem->getText();
        $succesMessage = true;
    }
}
$partialView('Shared', 'AdminHeader');
?>
<!--Dish/ReadingOne-->
<main>
    <section id="content">
        <h2>Details van Gerecht: <?php echo $model->getName(); ?></h2>
        <?php if($succesMessage): ?>
            <p class="message messagegreen">
                <strong><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<?php echo $title; ?></strong> <?php echo $message; ?>
            </p>
        <?php endif; ?>
        <!--Buttons-->
        <p class="buttonbartop">
            <a class="button buttonyellow" href="?uc=Dish-updating_<?php echo $model->getId();?>">
                <i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Wijzigen
            </a>
            <a class="button buttonspaceleft buttonred" href="?uc=Dish-deleting_<?php echo $model->getId();?>">
                <i class="fa fa-trash" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Verwijderen
            </a>
        </p>

        <p><strong>Naam: </strong><?php echo $model->getName(); ?></p>
        <p><strong>Marge: </strong><?php echo $model->getMargin(); ?></p>
        <p><strong>Categorie: </strong>
                <?php
                if ($model->getDishCategoryList())
                {
                    foreach ($model->getDishCategoryList() as $item)
                    {
                        if($item['Id'] == $model->getIdDishCategory()):
                            ?>
                            <?php echo $item['Name'];?>
                        <?php endif; ?>
                        <?php
                    }
                }
                ?>
        </p>
        <p><strong>Standaard Alternatief: </strong><?php echo ($model->getByDefault() ? 'Ja' : 'Nee'); ?></p>
        <p><strong>Terugkerend Op: </strong></p>
        <ul>
            <?php echo ($model->getMonday() ? '<li>Maandag</li>' : '');?>
            <?php echo ($model->getTuesday() ? '<li>Dinsdag</li>' : '');?>
            <?php echo ($model->getWednesday() ? '<li>Woensdag</li>' : '');?>
            <?php echo ($model->getThursday() ? '<li>Donderdag</li>' : '');?>
            <?php echo ($model->getFriday() ? '<li>Vrijdag</li>' : '');?>
            <?php echo ($model->getSaturday() ? '<li>Zaterdag</li>' : '');?>
            <?php echo ($model->getSunday() ? '<li>Zondag</li>' : '');?>
        </ul>

        <h3>Dieet informatie: </h3>
        <?php if ($model->getLinkedDietaryInfo()): ?>
            <ul class="read-list">
                <?php foreach ($model->getLinkedDietaryInfo() as $item): ?>
                    <li><?php echo $item['Description'];?></li>
                <?php endforeach; ?>
            </ul>
        <?php else: ?>
            <p>Geen dieet informatie.</p>
        <?php endif; ?>

        <p><a href="index.php?uc=Dish-index">Terug naar overzicht Gerechten</a></p>
    </section>
</main>

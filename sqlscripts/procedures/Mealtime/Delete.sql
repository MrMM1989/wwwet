use wwwet;

-- Delete
DELIMITER ;;
CREATE PROCEDURE `MealtimeDelete`(
	 pId INT 
)
BEGIN
DELETE FROM `Mealtime`
	WHERE `Mealtime`.`Id` = pId;
END ;;
-- Dish --
use wwwet;
-- Stored Procedures --

-- Select All --
DELIMITER ;;
CREATE PROCEDURE `DishSelectAll`(
)
BEGIN
SELECT `Dish`.`Id`, `Dish`.`IdDishCategory`, `IdDishCategory-DishCategory`.`Name` as IdDishCategoryName, `Dish`.`Name`, `Dish`.`ByDefault`, `Dish`.`Margin`, `Dish`.`Monday`, `Dish`.`Tuesday`, `Dish`.`Wednesday`, `Dish`.`Thursday`, `Dish`.`Friday`, `Dish`.`Saturday`, `Dish`.`Sunday`
	FROM `Dish`
	INNER JOIN `DishCategory` as `IdDishCategory-DishCategory`
		ON `Dish`.`IdDishCategory` = `IdDishCategory-DishCategory`.`Id`
	ORDER BY `Dish`.`Name`;
END ;;
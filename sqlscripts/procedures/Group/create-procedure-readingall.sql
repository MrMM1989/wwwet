USE wwwet;
-- -----------------------------------------------------
-- Create Procedure GroupReadingAll
-- -----------------------------------------------------
DROP PROCEDURE IF EXISTS GroupSelectAll;
DELIMITER //
CREATE PROCEDURE GroupSelectAll()
BEGIN
SELECT Id, Name 
FROM `Group`
ORDER BY Id;
END //
DELIMITER ;
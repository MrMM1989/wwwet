<?php
namespace ModernWays\WWWET\Controller;

class Role extends \ModernWays\WWWET\Controller\AppController {
		
	public function __construct($route, $noticeBoard)
	{
		// Call parent constructor
		parent::__construct($route, $noticeBoard);
		
		// Set modelName & load model and DAL
		$this->modelName = $this->initModelName($this);
		$this->loadModel($this->noticeBoard);
		$this->loadDal($this->noticeBoard);
	}
	
	public function delete()
	{
		if($this->isAuthenticated()){
			
			$this->model->setId($this->route->getId());
		
			$this->dal->delete();
			$this->dal->readingAll();
		
			return $this->view($this->modelName, 'Index', $this->model);			
		}else{
       	
		   return $this -> view('Home', 'Index', null);
       }	
	}
	
	public function deleting()
	{
		if($this->isAuthenticated()){
			
			$this->model->setId($this->route->getId());
		
			$this->dal->readingOne();
		
			return $this->view($this->modelName, 'Deleting', $this->model);	
		}else{
       	
		   return $this -> view('Home', 'Index', null);
       }	
	}
	
	public function index()
    {
       if($this->isAuthenticated()){
      	  
		   $this->dal->readingAll();
	   
      	   return $this->view($this->modelName, 'Index', $this->model); 	
       }else{
       	
		   return $this -> view('Home', 'Index', null);
       }	  
    }
	
	public function insert()
	{
		if($this->isAuthenticated()){
			
			$this->model->setName(filter_input(INPUT_POST, 'role-name', FILTER_SANITIZE_STRING));
		
			$this->dal->insert();
			$this->dal->readingAll();
		
			return $this->view($this->modelName, 'Index', $this->model);	
		}else{
       	
		   return $this -> view('Home', 'Index', null);
       }	
	}
	
	public function inserting()
	{
		if($this->isAuthenticated()){
			return $this->view($this->modelName, 'Inserting', $this->model);
		}else{
       	
		   return $this -> view('Home', 'Index', null);
       }	
	}
	
	public function readingOne()
	{
		if($this->isAuthenticated()){
			
			$this->model->setId($this->route->getId());
		
			$this->dal->readingOne();
		
			return $this->view($this->modelName, 'ReadingOne', $this->model);
		}else{
       	
		   return $this -> view('Home', 'Index', null);
       }			
	}
	
	public function update()
	{
		if($this->isAuthenticated()){
			
			$this->model->setId(filter_input(INPUT_POST, 'role-id', FILTER_SANITIZE_NUMBER_INT));
			$this->model->setName(filter_input(INPUT_POST, 'role-name', FILTER_SANITIZE_STRING));
		
			$this->dal->update();
			$this->dal->readingOne();
		
			return $this->view($this->modelName, 'ReadingOne', $this->model);
		}else{
       	
		   return $this -> view('Home', 'Index', null);
       }	
	}
	
	public function updating()
	{
		if($this->isAuthenticated()){
				
			$this->model->setId($this->route->getId());
		
			$this->dal->readingOne();
		
			return $this->view($this->modelName, 'Updating', $this->model);	
		}else{
       	
		   return $this -> view('Home', 'Index', null);
       }	
	}
}	
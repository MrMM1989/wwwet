<?php
/**
 * Created by PhpStorm.
 * User: Kurt
 * Date: 1/05/2016
 * Time: 13:14
 */
namespace ModernWays\WWWET\Model;

class Meal extends \ModernWays\Mvc\Model
{
    private $id;
    private $date;
    private $comment;
    private $idResident;
    private $idMenu;
    protected $residentList;
    protected $menuList;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getIdResident()
    {
        return $this->idResident;
    }

    /**
     * @param mixed $idResident
     */
    public function setIdResident($idResident)
    {
        $this->idResident = $idResident;
    }

    /**
     * @return mixed
     */
    public function getIdMenu()
    {
        return $this->idMenu;
    }

    /**
     * @param mixed $idMenu
     */
    public function setIdMenu($idMenu)
    {
        $this->idMenu = $idMenu;
    }

    /**
     * @return mixed
     */
    public function getResidentList()
    {
        return $this->residentList;
    }

    /**
     * @param mixed $residentList
     */
    public function setResidentList($residentList)
    {
        $this->residentList = $residentList;
    }

    /**
     * @return mixed
     */
    public function getMenuList()
    {
        return $this->menuList;
    }

    /**
     * @param mixed $menuList
     */
    public function setMenuList($menuList)
    {
        $this->menuList = $menuList;
    }
}
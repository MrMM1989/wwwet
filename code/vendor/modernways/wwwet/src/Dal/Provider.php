<?php
namespace ModernWays\WWWET\Dal;
class Provider extends \ModernWays\AnOrmApart\Provider
{
    public function __construct($name, $noticeBoard)
    {
        parent::__construct($name, $noticeBoard);

        $this->noticeBoard = $noticeBoard;

        if (!defined('HOST_PROVIDER'))
        {
            $this->databaseName = 'wwwet';
            $this->password = '@9WwE3t';
            $this->userName = 'wwwet';
            $this->hostName = '164.132.193.60:3306';
        }
        else
        {
            switch (HOST_PROVIDER)
            {
                case('wwwet-old'):
                    $this->databaseName = 'wwwet-old';
                    $this->password = '@9WwE3t';
                    $this->userName = 'wwwet';
                    $this->hostName = '164.132.193.60:3306';
                    break;
                case('wwwet'):
                    $this->databaseName = 'wwwet';
                    $this->password = '@9WwE3t';
                    $this->userName = 'wwwet';
                    $this->hostName = '164.132.193.60:3306';
                    break;
                default :
                    $this->databaseName = 'wwwet';
                    $this->password = '@9WwE3t';
                    $this->userName = 'wwwet';
                    $this->hostName = '164.132.193.60:3306';
                    break;
            }
        }
    }
}

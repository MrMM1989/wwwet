<?php
namespace ModernWays\WWWET\Controller;

class Group extends \ModernWays\WWWET\Controller\DalController {
		
	public function __construct($route, $noticeBoard)
	{
		// Call parent constructor
		parent::__construct($route, $noticeBoard);
		
		// Set modelName & load model and DAL
		$this->modelName = $this->initModelName($this);
		$this->loadModel($this->noticeBoard);
		$this->loadDal($this->noticeBoard);
	}
	
	public function delete()
	{
		$this->model->setId($this->route->getId());
		
		$this->dal->delete();
		$this->dal->readingAll();
		
		return $this->view($this->modelName, 'Index', $this->model);
	}
	
	public function deleting()
	{
		$this->model->setId($this->route->getId());
		
		$this->dal->readingOne();
		
		return $this->view($this->modelName, 'Deleting', $this->model);
	}
	
	public function index()
    {
	   $this->dal->readingAll();
	   
       return $this->view($this->modelName, 'Index', $this->model);
    }
	
	public function insert()
	{
		$this->model->setName(filter_input(INPUT_POST, 'group-name', FILTER_SANITIZE_STRING));
		
		$this->dal->insert();
		$this->dal->readingAll();
		
		return $this->view($this->modelName, 'Index', $this->model);
	}
	
	public function inserting()
	{
		return $this->view($this->modelName, 'Inserting', $this->model);
	}
	
	public function readingOne()
	{
		$this->model->setId($this->route->getId());
		
		$this->dal->readingOne();
		
		return $this->view($this->modelName, 'ReadingOne', $this->model);
	}
	
	public function update()
	{
		$this->model->setId(filter_input(INPUT_POST, 'group-id', FILTER_SANITIZE_NUMBER_INT));
		$this->model->setName(filter_input(INPUT_POST, 'group-name', FILTER_SANITIZE_STRING));
		
		$this->dal->update();
		$this->dal->readingOne();
		
		return $this->view($this->modelName, 'ReadingOne', $this->model);
	}
	
	public function updating()
	{
		$this->model->setId($this->route->getId());
		
		$this->dal->readingOne();
		
		return $this->view($this->modelName, 'Updating', $this->model);
	}
}	
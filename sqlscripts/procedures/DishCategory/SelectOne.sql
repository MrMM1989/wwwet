-- DishCategory
use wwwet;

-- SelectOne
DELIMITER ;;
CREATE PROCEDURE `DishCategorySelectOne`(
	 pId INT 
)
BEGIN
SELECT * FROM `DishCategory`
	WHERE `DishCategory`.`Id` = pId;
END ;;
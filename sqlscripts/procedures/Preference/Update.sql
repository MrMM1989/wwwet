-- Preference --
use wwwet;
-- Stored Procedures --

-- Update --
DELIMITER ;;
CREATE PROCEDURE `PreferencesUpdate`(
	OUT pId INT,
    IN pName NVARCHAR(255),
    IN pIdDish INT,
    IN pIdDietaryInfo INT,
    IN pIdDishCategory INT,
    IN pIdPreferenceType INT,
    IN pPreperation NVARCHAR(45),
    IN pPortionSize NVARCHAR(45)
)
BEGIN
UPDATE `Preference`
	SET
		`Name` = pName,
		`IdDish` = pIdDish,
		`IdDietaryInfo` = pIdDietaryInfo,
		`IdDishCategory` = pIdDishCategory,
		`IdPreferenceType` = pIdPrefrenceType,
		`Preperation` = pPreperation,
		`PortionSize` = pPortionSize,
		`UpdatedOn` = NOW()
	WHERE `Dish`.`Id` = pId;
END ;;
<?php $partialView('Shared', 'AdminHeader'); ?>
<main>
	<section id="content">
		<h2>Een nieuwe beheerder toevoegen</h2>
		<form action="index.php?uc=Attendant-insert" method="post">
			<h3>Algemeen</h3>
			<ul>
				<li>
					<label for="attendant-name">Naam: </label>
					<input id="attendant-name" name="attendant-name" type="text" required />
				</li>
				<li>
					<label for="attendant-username">Gebruikersnaam: </label>
					<input id="attendant-username" name="attendant-username" type="text" required />
				</li>
				<li>
					<label for="attendant-password">Wachtwoord: </label>
					<input id="attendant-password" name="attendant-password" type="password" required />
				</li>
				<li>
					<label for="attendant-role">Rol: </label>
					<select id="attendant-role" name="attendant-role">
						<option disabled="disabled">--Selecteer een rol--</option>
						<?php if(!empty($model->getRoleList())): ?>
							<?php foreach($model->getRoleList() as $item): ?>
								<option value="<?php echo $item['Id']; ?>"><?php echo $item['Name']; ?></option>
							<?php endforeach; ?>
						<?php endif; ?>
					</select>
				</li>
			</ul>
			<h3>Beheerder van groepen</h3>
			<ul>
				<li>
					<ul id="ajax" class="edit-list">
						<li>Geen groepen toegevoegd</li>
					</ul>
				</li>
				<li>
					<label for="attendant-group">Voeg een groep toe: </label>
					<select id="attendant-group" class="smaller" name="attendant-group">
						<option disabled="disabled">--Selecteer een groep--</option>
						<?php if(!empty($model->getGroupList())): ?>
							<?php foreach($model->getGroupList() as $item): ?>
								<option value="<?php echo $item['Id']; ?>"><?php echo $item['Name']; ?></option>
							<?php endforeach; ?>
						<?php endif; ?>
					</select>
					<button id="attendant-group-add" class="horizontal buttongreen buttonspaceleft" name="attendant-group-add" type="button">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				</li>
				<li class="buttonbarbottom">
					<button class="buttongreen" name="submit" type="submit">
						<i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Toevoegen
					</button>
					<a class="button buttonspaceleft buttonred" href="index.php?uc=Attendant-index">
						<i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Annuleren
					</a>
				</li>
			</ul>
		</form>		
	</section>
</main>
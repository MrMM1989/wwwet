-- Menu --
use wwwet;
-- Stored Procedures --
-- Insert --
DELIMITER ;;
CREATE PROCEDURE `MenuDelete`(
	pId INT
)
BEGIN
	DELETE FROM MenuDish
		WHERE MenuDish.IdMenu = pId;
	DELETE FROM Menu
		WHERE Menu.id = pId;
END ;;
DELIMITER ;;
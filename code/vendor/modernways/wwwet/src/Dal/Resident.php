<?php
/**
 * Created by PhpStorm.
 * User: Kurt
 * Date: 1/05/2016
 * Time: 13:02
 */
namespace ModernWays\WWWET\Dal;
class Resident extends \ModernWays\AnOrmApart\Dal
{
    public function __construct(\ModernWays\WWWET\Model\Resident $model, \ModernWays\AnOrmApart\Provider $provider)
    {
        $this->model = $model;
        parent::__construct($provider);
    }
	
	public function delete(){
		
		$result = false;
		if (!$this -> provider -> isConnected()) {
			$this -> provider -> open();
		}
		if ($this -> provider -> isConnected()) {
			try {  

				//Prepare the statement
				$deleteSql = 'DELETE FROM Resident WHERE Id = :id';
				$preparedStatement = $this -> provider -> getPdo() -> prepare($deleteSql);
				$preparedStatement -> bindValue(':id', $this -> model -> getId(), \PDO::PARAM_INT);

				//Execute statement
				$result = $preparedStatement -> execute();
				
				if($result){
					$this->model->showMessage('Resident', 'DeleteSuccessMessage', true);
				}else {
					$this->model->showMessage('Resident', 'DeleteFailedMessage', false);
				}
				

			} catch (\PDOException $e) {
				$this -> model -> getModelState() -> crudNotice('DeleteResidentPrerefence', false, $this, $e, null);
			}
		} else {
			$this -> model -> getModelState() -> disconnected($this -> provider);
		}

		return $result;
	}
	
	public function deleteResidentPreference($preferenceToRemove){
		
		$result = false;
		if (!$this -> provider -> isConnected()) {
			$this -> provider -> open();
		}
		if ($this -> provider -> isConnected()) {
			try {  

				//Prepare the statement
				$deleteSql = 'DELETE FROM ResidentPreferences WHERE IdResident = :idresident AND IdPreference = :idpreference';
				$preparedStatement = $this -> provider -> getPdo() -> prepare($deleteSql);
				$preparedStatement -> bindValue(':idresident', $this -> model -> getId(), \PDO::PARAM_INT);
				$preparedStatement -> bindParam(':idpreference', $preferenceToRemove, \PDO::PARAM_INT);

				//Execute statement
				$result = $preparedStatement -> execute();
				$this -> model -> getModelState() -> crudNotice('DeleteResidentPreference', false, $this, null, null);

			} catch (\PDOException $e) {
				$this -> model -> getModelState() -> crudNotice('DeleteResidentPrerefence', false, $this, $e, null);
			}
		} else {
			$this -> model -> getModelState() -> disconnected($this -> provider);
		}

		return $result;
	}

   	public function insert()
	{
		$result = false;
		if (!$this -> provider -> isConnected()) {
			$this -> provider -> open();
		}
		if ($this -> provider -> isConnected()) {
			try {

				//Prepare the statement
				$insertSql = 'INSERT INTO Resident (Name, RoomNumber, Diabetes, Diet, Extra, IdGroup) VALUES (:name, :roomnumber, :diabetes, :diet, :extra, :idgroup)';
				$preparedStatement = $this -> provider -> getPdo() -> prepare($insertSql);
				$preparedStatement -> bindValue(':name', $this -> model -> getName(), \PDO::PARAM_STR);
				$preparedStatement -> bindValue(':roomnumber', $this -> model -> getRoomNumber(), \PDO::PARAM_STR);
				$preparedStatement -> bindValue(':diabetes', $this -> model -> getDiabetes(), \PDO::PARAM_BOOL);
				$preparedStatement -> bindValue(':diet', $this -> model -> getDiet(), \PDO::PARAM_STR);
				$preparedStatement -> bindValue(':extra', $this -> model -> getExtra(), \PDO::PARAM_STR);
				$preparedStatement -> bindValue(':idgroup', $this -> model -> getIdGroup(), \PDO::PARAM_INT);

				//Execute statement
				$result = $preparedStatement -> execute();

				if($result){
					$this->model->showMessage('Resident', 'InsertSuccessMessage', true);
				}else {
					$this->model->showMessage('Resident', 'InsertFailedMessage', false);
				}

			} catch (\PDOException $e) {
				$this -> model -> getModelState() -> crudNotice('Insert', false, $this, $e, null);
			}
		} else {
			$this -> model -> getModelState() -> disconnected($this -> provider);
		}

		return $result;
	}

	public function insertResidentPreference($preferenceToAdd, $alternative){
		
		$result = false;
		if (!$this -> provider -> isConnected()) {
			$this -> provider -> open();
		}
		if ($this -> provider -> isConnected()) {
			try {
							
				$idAlternative = ($alternative == -1)? NULL : $alternative;  

				//Prepare the statement
				$insertSql = 'INSERT INTO ResidentPreferences (IdResident, IdPreference, DishAlternative) VALUES (:idresident, :idpreference, :dishalternative)';
				$preparedStatement = $this -> provider -> getPdo() -> prepare($insertSql);
				$preparedStatement -> bindValue(':idresident', $this -> model -> getId(), \PDO::PARAM_INT);
				$preparedStatement -> bindParam(':idpreference', $preferenceToAdd, \PDO::PARAM_INT);
				$preparedStatement -> bindParam(':dishalternative', $idAlternative, \PDO::PARAM_INT);

				//Execute statement
				$result = $preparedStatement -> execute();
				$this -> model -> getModelState() -> crudNotice('InsertResidentPreference', false, $this, null, null);

			} catch (\PDOException $e) {
				$this -> model -> getModelState() -> crudNotice('InsertResidentPreference', false, $this, $e, null);
			}
		} else {
			$this -> model -> getModelState() -> disconnected($this -> provider);
		}

		return $result;
	}
	
	public function readingOneWithPreferenceList() {

		if (!$this -> provider -> isConnected()) {
			$this -> provider -> open();
		}
		if ($this -> provider -> isConnected()) {
			try {
				
				//Call parent method for reading basic resident details
				parent::readingOne();
				
				//Read ResidentPreferenceList of Resident
				$pdo = $this -> provider -> getPdo();
				$idResident = $this->model->getId();
				$selectSQLResidentPreferenceList = 'SELECT * FROM vwSelectResidentPreferenceList WHERE IdResident = :idresident ORDER BY NamePreference';
				$preparedSQLResidentPreferenceList = $pdo -> prepare($selectSQLResidentPreferenceList);
				$preparedSQLResidentPreferenceList -> bindValue(':idresident', $idResident, \PDO::PARAM_INT);

				$preparedSQLResidentPreferenceList -> execute();

				$ResidentPreferenceList = $preparedSQLResidentPreferenceList -> fetchAll();

				$this -> model -> setResidentPreferenceList($ResidentPreferenceList);				

			} catch (\PDOException $e) {
				$this -> model -> getModelState() -> crudNotice('ReadingOneWithPreferenceList', false, $this, $e, null);
			}
		} else {
			$this -> model -> getModelState() -> disconnected($this -> provider);
		}
	}

	public function update()
	{
		$result = false;
		if (!$this -> provider -> isConnected()) {
			$this -> provider -> open();
		}
		if ($this -> provider -> isConnected()) {
			try {
				
				//Prepare the statement
				$updateSql = 'Update Resident SET Name = :name, RoomNumber = :roomnumber, Diabetes = :diabetes, Diet = :diet, Extra = :extra, IdGroup = :idgroup WHERE Id = :id';
				$preparedStatement = $this -> provider -> getPdo() -> prepare($updateSql);
				$preparedStatement -> bindValue(':name', $this -> model -> getName(), \PDO::PARAM_STR);
				$preparedStatement -> bindValue(':roomnumber', $this -> model -> getRoomNumber(), \PDO::PARAM_STR);
				$preparedStatement -> bindValue(':diabetes', $this -> model -> getDiabetes(), \PDO::PARAM_BOOL);
				$preparedStatement -> bindValue(':diet', $this -> model -> getDiet(), \PDO::PARAM_STR);
				$preparedStatement -> bindValue(':extra', $this -> model -> getExtra(), \PDO::PARAM_STR);
				$preparedStatement -> bindValue(':idgroup', $this -> model -> getIdGroup(), \PDO::PARAM_INT);
				$preparedStatement -> bindValue(':id', $this -> model -> getId(), \PDO::PARAM_INT);

				//Execute statement
				$result = $preparedStatement -> execute();

				if($result){
					$this->model->showMessage('Resident', 'UpdateSuccessMessage', true);
				}else {
					$this->model->showMessage('Resident', 'UpdateFailedMessage', false);
				}

			} catch (\PDOException $e) {
				$this -> model -> getModelState() -> crudNotice('Insert', false, $this, $e, null);
			}
		} else {
			$this -> model -> getModelState() -> disconnected($this -> provider);
		}

		return $result;
	}
}
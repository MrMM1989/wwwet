USE wwwet;
-- -----------------------------------------------------
-- Create View vwSelectOneAttendant
-- -----------------------------------------------------
DROP VIEW IF EXISTS vwSelectOneAttendant;
CREATE VIEW vwSelectOneAttendant AS
SELECT a.Id, a.Name, a.UserName, a.IdRole,r.Name AS NameRole
FROM Attendant AS a
INNER JOIN Role AS r
ON a.IdRole = r.Id;
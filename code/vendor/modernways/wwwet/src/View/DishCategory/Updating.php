<?php $partialView('Shared', 'AdminHeader'); ?>
    <!--DishCategory/Updating-->
    <main>
        <section id="content">
            <h2>Categorie wijzigen: <?php echo $model->getName(); ?></h2>
            <form action="index.php?uc=DishCategory-Update" method="post">
                <ul>
                    <li>
                        <input id="DishCategory-Id" type="hidden" name="DishCategory-Id"
                               value="<?php echo $model->getId();?>" />
                    </li>
                    <li>
                        <label for="DishCategory-Name">Naam</label>
                        <input id="DishCategory-Name" name="DishCategory-Name" type="text"
                               value="<?php echo $model->getName();?>" />
                    </li>
                    <li>
                        <label for="DishCategory-Order">Volgorde</label>
                        <input id="DishCategory-Order" name="DishCategory-Order" type="text"
                               value="<?php echo $model->getOrder();?>"/>
                    </li>
                </ul>

                <ul>
                    <li class="buttonbarbottom">
                        <button class="buttongreen" name="submit" type="submit">
                            <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Opslaan
                        </button>
                        <a class="button buttonspaceleft buttonred" href="index.php?uc=DishCategory-index">
                            <i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Annuleren
                        </a>
                    </li>
                </ul>
            </form>
        </section>
    </main>
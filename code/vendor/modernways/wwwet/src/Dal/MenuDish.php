<?php
/**
 * Created by PhpStorm.
 * User: Kurt
 * Date: 1/05/2016
 * Time: 13:34
 */

namespace ModernWays\WWWET\Dal;


class MenuDish extends \ModernWays\AnOrmApart\Dal
{
    public function __construct(\ModernWays\WWWET\Model\MenuDish $model, \ModernWays\AnOrmApart\Provider $provider)
    {
        $this->model = $model;
        parent::__construct($provider);
    }

    public function readMenus() {
        if (!$this->provider->isConnected()) {
            $this->provider->open();
        }
        if ($this->provider->isConnected()) {
            try {
                $preparedStatement = $this->provider->getPdo()->prepare("CALL MenuSelectAll()");
                $preparedStatement->execute();
                $this->rowCount = $preparedStatement->rowCount();
                $this->model->setMenuList($preparedStatement->fetchAll());

                if ($this->model->getList()) {
                    $this->model->getModelState()->crudNotice('MenuDish: readMenus', true, $this, null, $preparedStatement->errorInfo());
                } else {
                    $this->model->getModelState()->crudNotice('MenuDish: readMenus', false, $this, null, $preparedStatement->errorInfo());
                }

            } catch (\PDOException $e) {
                $this->model->getModelState()->crudNotice("MenuDish: readMenus, Error: {$e}", true, $this, $e);
            }
        }
    }

    public function readDishes() {
        if (!$this->provider->isConnected()) {
            $this->provider->open();
        }
        if ($this->provider->isConnected()) {
            try {
                $preparedStatement = $this->provider->getPdo()->prepare("CALL DishSelectAll()");
                $preparedStatement->execute();
                $this->rowCount = $preparedStatement->rowCount();
                $this->model->setDishList($preparedStatement->fetchAll());

                if ($this->model->getList()) {
                    $this->model->getModelState()->crudNotice('MenuDish: readDishes', true, $this, null, $preparedStatement->errorInfo());
                } else {
                    $this->model->getModelState()->crudNotice('MenuDish: readDishes', false, $this, null, $preparedStatement->errorInfo());
                }

            } catch (\PDOException $e) {
                $this->model->getModelState()->crudNotice("MenuDish: readMenus, Error: {$e}", true, $this, $e);
            }
        }
    }
}
use wwwet;
DELIMITER ;;
CREATE PROCEDURE `DishGetDietaryInfo`(
	IN pId INT
)
BEGIN
	select DietaryInfo.Id, Description from DietaryInfo
	inner join DishDiet
		on DishDiet.IdDietaryInfo = DietaryInfo.Id
	where idDish = pId;
END ;;
<?php
namespace ModernWays\WWWET\Controller;

class Resident extends \ModernWays\WWWET\Controller\AppController {
		
	public function __construct($route, $noticeBoard)
	{
		// Call parent constructor
		parent::__construct($route, $noticeBoard);
		
		// Set modelName & load model and DAL
		$this->modelName = $this->initModelName($this);
		$this->loadModel($this->noticeBoard);
		$this->loadDal($this->noticeBoard);
	}
	
	public function addResidentPreference(){
		
		if ($this -> isAuthenticated()) {
			
			//Add preference to list
			$this->model->setId(filter_input(INPUT_POST, 'resident-id', FILTER_SANITIZE_NUMBER_INT));
			
			$preferenceToAdd = filter_input(INPUT_POST, 'preferences', FILTER_SANITIZE_NUMBER_INT);
			$alternative = filter_input(INPUT_POST, 'preference-alternative', FILTER_SANITIZE_NUMBER_INT);
			
			$this->dal->insertResidentPreference($preferenceToAdd, $alternative);
			
			//Return to page for alterering preferences
			$this -> model -> setPreferenceList($this -> dal -> reading('Preference', 'Name, Id', null, false));
			$this -> model -> setDishList($this -> dal -> reading('Dish', 'Name, Id', null, false));
			
			
			$this->dal->readingOneWithPreferenceList();
			
			return $this->view($this->modelName, 'AlterPreferenceList', $this->model); 
			
		} else {
			
			return $this -> view('Home', 'Index', null);
		}
		
	}
	
	public function alterPreferenceList() {
		if ($this -> isAuthenticated()) {
			
			$this->model->setId($this->route->getId());
			$this -> model -> setPreferenceList($this -> dal -> reading('Preference', 'Name, Id', null, false));
			$this -> model -> setDishList($this -> dal -> reading('Dish', 'Name, Id', null, false));
			
			$this->dal->readingOneWithPreferenceList();
			
			return $this->view($this->modelName, 'AlterPreferenceList', $this->model); 
			
		} else {
			
			return $this -> view('Home', 'Index', null);
		}
	}
	
	public function delete() {
		if ($this -> isAuthenticated()) {
			
			$this->model->setId($this->route->getId());
			
			$this->dal->delete();
			$this->dal->readingAll();
			
			return $this->view($this->modelName, 'Index', $this->model); 
			
		} else {
			
			return $this -> view('Home', 'Index', null);
		}
	}
	
	public function deleting() {
		if ($this -> isAuthenticated()) {
			
			$this->model->setId($this->route->getId());
			
			$this->dal->readingOne();
			
			return $this->view($this->modelName, 'Deleting', $this->model); 
			
		} else {
			
			return $this -> view('Home', 'Index', null);
		}
	}	
		
	public function index() {
		if ($this -> isAuthenticated()) {
			
			$this->dal->readingAll();
			
			return $this->view($this->modelName, 'Index', $this->model); 
			
		} else {
			
			return $this -> view('Home', 'Index', null);
		}
	}
	
	public function insert() {
		if ($this -> isAuthenticated()) {
			
			$this->model->setName(filter_input(INPUT_POST, 'resident-name', FILTER_SANITIZE_STRING));
			$this->model->setRoomNumber(filter_input(INPUT_POST, 'resident-roomnumber', FILTER_SANITIZE_STRING));
			$this->model->setIdGroup(filter_input(INPUT_POST, 'resident-group', FILTER_SANITIZE_NUMBER_INT));
			$this->model->setDiabetes(filter_input(INPUT_POST, 'resident-diabetes', FILTER_VALIDATE_BOOLEAN));
			$this->model->setDiet(filter_input(INPUT_POST, 'resident-diet', FILTER_SANITIZE_STRING));
			$this->model->setExtra(filter_input(INPUT_POST, 'resident-extra', FILTER_SANITIZE_STRING));
			
			$this->dal->insert();
			$this->dal->readingAll();			
			
			return $this->view($this->modelName, 'Index', $this->model); 
			
		} else {
			
			return $this -> view('Home', 'Index', null);
		}
	}
	
	public function inserting() {
		if ($this -> isAuthenticated()) {
			
			$this -> model -> setGroupList($this -> dal -> reading('`Group`', 'Name, Id', null, false));
			
			return $this->view($this->modelName, 'Inserting', $this->model); 
			
		} else {
			
			return $this -> view('Home', 'Index', null);
		}
	}
	
	public function readingOne() {
		if ($this -> isAuthenticated()) {
			
			$this->model->setId($this->route->getId());
			
			$this->dal->readingOne();
			
			return $this->view($this->modelName, 'ReadingOne', $this->model); 
			
		} else {
			
			return $this -> view('Home', 'Index', null);
		}
	}
	
	public function removeResidentPreference(){
		
		if ($this -> isAuthenticated()) {
			
			$this->model->setId($this->route->getId());
			$this -> model -> setPreferenceList($this -> dal -> reading('Preference', 'Name, Id', null, false));
			$this -> model -> setDishList($this -> dal -> reading('Dish', 'Name, Id', null, false));
			
			$preferenceToRemove = filter_input(INPUT_GET, 'pid', FILTER_SANITIZE_NUMBER_INT);
			
			$this->dal->deleteResidentPreference($preferenceToRemove);			
			$this->dal->readingOneWithPreferenceList();
			
			return $this->view($this->modelName, 'AlterPreferenceList', $this->model); 
			
		} else {
			
			return $this -> view('Home', 'Index', null);
		}		
	}
	
	public function update(){
		
		if ($this -> isAuthenticated()) {
			
			$this->model->setId(filter_input(INPUT_POST, 'resident-id', FILTER_SANITIZE_NUMBER_INT));
			$this->model->setName(filter_input(INPUT_POST, 'resident-name', FILTER_SANITIZE_STRING));
			$this->model->setRoomNumber(filter_input(INPUT_POST, 'resident-roomnumber', FILTER_SANITIZE_STRING));
			$this->model->setIdGroup(filter_input(INPUT_POST, 'resident-group', FILTER_SANITIZE_NUMBER_INT));
			$this->model->setDiabetes(filter_input(INPUT_POST, 'resident-diabetes', FILTER_VALIDATE_BOOLEAN));
			$this->model->setDiet(filter_input(INPUT_POST, 'resident-diet', FILTER_SANITIZE_STRING));
			$this->model->setExtra(filter_input(INPUT_POST, 'resident-extra', FILTER_SANITIZE_STRING));
			
			$this->dal->update();
			$this->dal->readingOne();			
			
			return $this->view($this->modelName, 'ReadingOne', $this->model); 
			
		} else {
			
			return $this -> view('Home', 'Index', null);
		}
	}
	
	public function updating(){
		
		if ($this -> isAuthenticated()) {
			
			$this -> model -> setId($this->route->getId());
			$this -> model -> setGroupList($this -> dal -> reading('`Group`', 'Name, Id', null, false));
			
			$this->dal->readingOne();
			
			return $this->view($this->modelName, 'Updating', $this->model); 
			
		} else {
			
			return $this -> view('Home', 'Index', null);
		}		
	}
}
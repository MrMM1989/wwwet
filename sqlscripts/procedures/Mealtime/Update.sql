use wwwet;

-- Update
DELIMITER ;;
CREATE PROCEDURE `MealtimeUpdate`(
	pId INT ,
	pName NVARCHAR (255) 
	-- ,pUpdatedBy NVARCHAR (255) 
)
BEGIN
UPDATE `Mealtime`
	SET
		`Name` = pName,
		-- `UpdatedBy` = pUpdatedBy,
		`UpdatedOn` = NOW()
	WHERE `Mealtime`.`Id` = pId;
END ;;
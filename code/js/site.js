//Init attendantGroupAdd button on Attendant inserting & updating page
function initAttendantGroupAdd() {
	var addButton = document.getElementById('attendant-group-add');

	addButton.addEventListener('click', function(event) {

		var select = document.getElementById('attendant-group');
		var id = select.options[select.selectedIndex].value;
		var name = select.options[select.selectedIndex].text;

		serverAdd('Attendant', 'attendantGroupAdd', id, name, 'readList');

	});
	
	serverRead('Attendant', 'readList');
}

function initDietaryInfoAdd() {
	serverRead('Dish', 'readList');
	var addDIButton = document.getElementById('dietary-info-add');

	addDIButton.addEventListener('click', function(event) {
		var select = document.getElementById('dietary-info');
		var id = select.options[select.selectedIndex].value;
		var name = select.options[select.selectedIndex].text;

		serverAdd('Dish', 'dietaryInfoAdd', id, name, 'readList');
	});
	
	serverRead('Dish', 'readList');
}

function initDishAdd() {
	serverRead('Menu', 'readlist');
	var addDButtons = document.getElementsByName('menu-dish-add');

	for(var i = 0; i < addDButtons.length; i++) {
		var button = addDButtons[i];

		button.addEventListener('click', function(event) {
			var me = (this);
			var tmpId = me.id.replace("menu-dish-add", "");

			var select = document.getElementById('menu-dishes' + tmpId);
			var id = select.options[select.selectedIndex].value;
			var name = select.options[select.selectedIndex].text;

			serverAdd('Menu', 'dishAdd', id, name, 'readList');
		});
	}

	serverRead('Menu', 'readlist');
}

//Ajax function for adding data to an array on the server
function serverAdd(entity, action, id, name, readAction) {
	var url = 'index.php?uc=' + entity + '-' + action;

	var httpRequest = new XMLHttpRequest();

	if (!httpRequest) {
		return false;
	}

	httpRequest.onreadystatechange = function() {

		if (httpRequest.readyState === XMLHttpRequest.DONE) {
			if (httpRequest.status === 200) {
				serverRead(entity, readAction);
			}
		}
	};
	httpRequest.open('POST', url, true);
	httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	httpRequest.send('id=' + encodeURIComponent(id) + '&name=' + encodeURIComponent(name));
}

//Ajax function for removing data from an array on the server
function serverRemove(entity, action, id, readAction) {
	var url = 'index.php?uc=' + entity + '-' + action;

	var httpRequest = new XMLHttpRequest();

	if (!httpRequest) {
		return false;
	}

	httpRequest.onreadystatechange = function() {

		if (httpRequest.readyState === XMLHttpRequest.DONE) {
			if (httpRequest.status === 200) {
				serverRead(entity, readAction);
			}
		}
	};
	httpRequest.open('POST', url, true);
	httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	httpRequest.send('id=' + encodeURIComponent(id));
}

//Ajax function for reading data from server
function serverRead(entity, action) {

	var url = 'index.php?uc=' + entity + '-' + action;

	var httpRequest = new XMLHttpRequest();

	if (!httpRequest) {
		return false;
	}

	httpRequest.onreadystatechange = updateList;
	httpRequest.open('GET', url, true);
	httpRequest.send();

	function updateList() {
		if (httpRequest.readyState === XMLHttpRequest.DONE) {
			if (httpRequest.status === 200) {

				var ul = document.getElementById("ajax");

				//Loop through list and remove all the current li-tags
				if (ul) {
					while (ul.firstChild) {
						ul.removeChild(ul.firstChild);
					}
				}

				//Create the elements
				var response = JSON.parse(httpRequest.responseText);
				for (index in response) {

					var button = document.createElement('button');
					button.id = response[index].Id;
					button.innerHTML = '<i class="fa fa-minus" aria-hidden="true"></i>';
					button.className = 'horizontal buttonred buttonspaceleft';
					button.setAttribute('type', 'button');
					button.addEventListener('click', function(){
						serverRemove(entity, 'removeItem', this.id, 'readList');
					});

					var li = document.createElement('li');
					li.innerHTML = response[index].Name;
					li.appendChild(button);

					ul.appendChild(li);
				}
			}
		}
	}
}

//Wait for the DOM loaded to add events to elements
document.addEventListener('DOMContentLoaded', function(event) {
	//Only init functions in here!
	if(document.getElementById('attendant-group-add')) {
		initAttendantGroupAdd();
	} else if(document.getElementById('dietary-info-add')) {
		initDietaryInfoAdd();
	} else if(document.getElementsByName('menu-dish-add')) {
		initDishAdd();
	}
});

<?php
/**
 * Created by PhpStorm.
 * User: Kurt
 * Date: 1/05/2016
 * Time: 12:57
 */
namespace ModernWays\WWWET\Dal;
class Attendant extends \ModernWays\AnOrmApart\Dal {
	public function __construct(\ModernWays\WWWET\Model\Attendant $model, \ModernWays\AnOrmApart\Provider $provider) {
		$this -> model = $model;
		parent::__construct($provider);
	}
	
	public function delete() {
		$result = false;
		if (!$this -> provider -> isConnected()) {
			$this -> provider -> open();
		}
		if ($this -> provider -> isConnected()) {
			try {

				$pdo = $this -> provider -> getPdo();
				
				$idAttendant = $this -> model -> getId();

				//Turn off autocommit
				$pdo -> beginTransaction();

				//Delete part 1
				$deleteSQLAttendantGroupList = 'DELETE FROM AttendantGroup WHERE IdAttendant = :idattendant';
				$preparedDeleteSQLAttendantGroupList = $pdo -> prepare($deleteSQLAttendantGroupList);
				$preparedDeleteSQLAttendantGroupList -> bindValue(':idattendant', $idAttendant, \PDO::PARAM_INT);
				
				$preparedDeleteSQLAttendantGroupList -> execute();

				//Delete part 2
				$deleteSQLAttendant = 'DELETE FROM Attendant WHERE Id = :idattendant';
				$preparedDeleteSQLAttendant = $pdo -> prepare($deleteSQLAttendant);
				$preparedDeleteSQLAttendant -> bindValue(':idattendant', $idAttendant, \PDO::PARAM_INT);
				
				$preparedDeleteSQLAttendant -> execute();

				$result = $pdo -> commit();

				if ($result) {
					$this -> model -> showMessage('Attendant', 'DeleteSuccessMessage', true);
				} else {
					$this -> model -> showMessage('Attendant', 'DeleteFailedMessage', false);
				}

			} catch (\PDOException $e) {
				$this -> model -> getModelState() -> crudNotice('Insert', false, $this, $e, null);
			}
		} else {
			$this -> model -> getModelState() -> disconnected($this -> provider);
		}

		return $result;
	}
	

	public function insert() {
		$result = false;
		if (!$this -> provider -> isConnected()) {
			$this -> provider -> open();
		}
		if ($this -> provider -> isConnected()) {
			try {

				$pdo = $this -> provider -> getPdo();

				//Turn off autocommit
				$pdo -> beginTransaction();

				//Insert part 1
				$insertSQLAttendant = 'INSERT INTO Attendant (Name, UserName, Password, IdRole) VALUES (:name, :username, :password, :idrole)';
				$preparedSQLAttendant = $pdo -> prepare($insertSQLAttendant);
				$preparedSQLAttendant -> bindValue(':name', $this -> model -> getName(), \PDO::PARAM_STR);
				$preparedSQLAttendant -> bindValue(':username', $this -> model -> getUserName(), \PDO::PARAM_STR);
				$preparedSQLAttendant -> bindValue(':password', $this -> model -> getPassword(), \PDO::PARAM_STR);
				$preparedSQLAttendant -> bindValue(':idrole', $this -> model -> getIdRole(), \PDO::PARAM_INT);

				$preparedSQLAttendant -> execute();

				//Insert part 2 - only if the list of assigned groups isn't empty
				if (!empty($this -> model -> getAttendantGroupList())) {

					$idAttendant = $pdo -> lastInsertId();

					$value = null;

					$insertSQLAttendantGroup = 'INSERT INTO AttendantGroup (IdAttendant, IdGroup) VALUES (:idattendant, :idgroup)';
					$preparedSQLAttendantGroup = $pdo -> prepare($insertSQLAttendantGroup);
					$preparedSQLAttendantGroup -> bindValue(':idattendant', $idAttendant, \PDO::PARAM_INT);

					foreach ($this->model->getAttendantGroupList() as $value) {

						$preparedSQLAttendantGroup -> bindValue(':idgroup', $value['Id'], \PDO::PARAM_INT);
						$preparedSQLAttendantGroup -> execute();
					}
				}

				$result = $pdo -> commit();

				if ($result) {
					$this -> model -> showMessage('Attendant', 'InsertSuccessMessage', true);
				} else {
					$this -> model -> showMessage('Attendant', 'InsertFailedMessage', false);
				}

			} catch (\PDOException $e) {
				$this -> model -> getModelState() -> crudNotice('Insert', false, $this, $e, null);
			}
		} else {
			$this -> model -> getModelState() -> disconnected($this -> provider);
		}

		return $result;
	}

	public function readingOne() {

		if (!$this -> provider -> isConnected()) {
			$this -> provider -> open();
		}
		if ($this -> provider -> isConnected()) {
			try {

				$pdo = $this -> provider -> getPdo();

				$idAttendant = $this -> model -> getId();

				//Read Attendant Details
				$selectSQLOneAttendant = 'SELECT * FROM vwSelectOneAttendant WHERE Id = :idattendant';
				$preparedSQLOneAttendant = $pdo -> prepare($selectSQLOneAttendant);
				$preparedSQLOneAttendant -> bindValue(':idattendant', $idAttendant, \PDO::PARAM_INT);

				$preparedSQLOneAttendant -> execute();

				$AttendantDetails = $preparedSQLOneAttendant -> fetch(\PDO::FETCH_ASSOC);

				$this -> model -> setName($AttendantDetails['Name']);
				$this -> model -> setUserName($AttendantDetails['UserName']);
				$this -> model -> setIdRole($AttendantDetails['IdRole']);
				$this -> model -> setNameRole($AttendantDetails['NameRole']);

				//Read AttendantGroupList of Attendant
				$selectSQLAttendantGroup = 'SELECT * FROM vwSelectAttendantGroupList WHERE IdAttendant = :idattendant ORDER BY Name';
				$preparedSQLAttendantGroup = $pdo -> prepare($selectSQLAttendantGroup);
				$preparedSQLAttendantGroup -> bindValue(':idattendant', $idAttendant, \PDO::PARAM_INT);

				$preparedSQLAttendantGroup -> execute();

				$attendantGroupList = $preparedSQLAttendantGroup -> fetchAll();

				$this -> model -> setAttendantGroupList($attendantGroupList);

			} catch (\PDOException $e) {
				$this -> model -> getModelState() -> crudNotice('ReadingOne', false, $this, $e, null);
			}
		} else {
			$this -> model -> getModelState() -> disconnected($this -> provider);
		}
	}

	public function readAttendantCredentials($userName){
		
		$result = false;
		if (!$this -> provider -> isConnected()) {
			$this -> provider -> open();
		}
		if ($this -> provider -> isConnected()) {
			try {

				//Prepare statement
				$credentialSql = 'SELECT Id, Name, UserName, Password FROM Attendant WHERE UserName = :username LIMIT 1';
				$preparedStatement = $this -> provider -> getPdo() -> prepare($credentialSql);
				$preparedStatement -> bindValue(':username', $userName, \PDO::PARAM_STR);

				//Execute statement
				$preparedStatement -> execute();

				//Fetch result
				$result = $preparedStatement -> fetch(\PDO::FETCH_ASSOC);
				$this -> model -> getModelState() -> crudNotice("ReadAttendantCredentials UserName {$this->model->getUserName()}", true, $this);
			} catch (\PDOException $e) {
				$this -> model -> getModelState() -> crudNotice("ReadAttendantCredentials UserName {$this->model->getUserName()}", false, $this, $e);
			}
		} else {
			$this -> model -> getModelState() -> notconnected($this -> provider);
		}
		return $result;
	}

	public function update() {
		$result = false;
		if (!$this -> provider -> isConnected()) {
			$this -> provider -> open();
		}
		if ($this -> provider -> isConnected()) {
			try {

				//Phase 1: Preparation for update
							
				//First check if the current list isn't empty and if there was something in the database before in originalList
				$currentList = $this -> model -> getAttendantGroupList();
				
				if (!empty($currentList)) {

					if (isset($_SESSION['OriginalAttendantGroupList']) && !empty($_SESSION['OriginalAttendantGroupList'])){
						
						$originalList = $_SESSION['OriginalAttendantGroupList'];
						
						$currentListKeys = array_keys($currentList);
						$originalListKeys = array_keys($originalList);

						$insertDifference = array_diff($currentListKeys, $originalListKeys);

						$deleteDifference = array_diff($originalListKeys, $currentListKeys);
					}
					else{
						$currentListKeys = array_keys($currentList);
						$insertDifference = $currentListKeys;
					}
				}
				
				//Set the idAttendant and start pdo transaction
				$idAttendant = $this -> model -> getId();
				
				$pdo = $this -> provider -> getPdo();

				$pdo -> beginTransaction();
				//End Phase 1

				//Phase 2: Update main attendant details
				$updateSQLAttendant = 'UPDATE Attendant SET Name = :name, UserName = :username, IdRole = :idrole WHERE Id = :id';
				$preparedUpdateSQLAttendant = $pdo -> prepare($updateSQLAttendant);
				$preparedUpdateSQLAttendant -> bindValue(':name', $this -> model -> getName(), \PDO::PARAM_STR);
				$preparedUpdateSQLAttendant -> bindValue(':username', $this -> model -> getUserName(), \PDO::PARAM_STR);
				$preparedUpdateSQLAttendant -> bindValue(':idrole', $this->model->getIdRole(), \PDO::PARAM_INT);
				$preparedUpdateSQLAttendant -> bindValue(':id', $idAttendant, \PDO::PARAM_INT);
				
				$preparedUpdateSQLAttendant -> execute();
				//End Phase 2

				//Phase 3: Update attendant group list

				//Only insert the new added group Id's
				if (!empty($insertDifference)) {

					$insertSQLAttendantGroup = 'INSERT INTO AttendantGroup (IdAttendant, IdGroup) VALUES (:idattendant, :idgroup)';
					$preparedSQLAttendantGroupInsert = $pdo -> prepare($insertSQLAttendantGroup);
					$preparedSQLAttendantGroupInsert -> bindValue(':idattendant', $idAttendant, \PDO::PARAM_INT);

					foreach ($insertDifference as $value) {

						$preparedSQLAttendantGroupInsert -> bindValue(':idgroup', $value, \PDO::PARAM_INT);
						$preparedSQLAttendantGroupInsert -> execute();
					}
				}

				//Remove all group Id's not in the current list anymore
				if(!empty($deleteDifference)){
					$deleteSQLAttendantGroup = 'DELETE FROM AttendantGroup WHERE IdAttendant = :idattendant AND IdGroup = :idgrouplist';
					$preparedSQLAttendantGroupDelete = $pdo -> prepare($deleteSQLAttendantGroup);
					$preparedSQLAttendantGroupDelete -> bindValue(':idattendant', $idAttendant, \PDO::PARAM_INT);
					
					foreach($deleteDifference as $value){
						$preparedSQLAttendantGroupDelete -> bindValue(':idgrouplist', $value, \PDO::PARAM_STR);
						$preparedSQLAttendantGroupDelete -> execute();
					}
				}
				
				$result = $pdo -> commit();
				
				//End Phase 3

				if ($result) {
					$this -> model -> showMessage('Attendant', 'UpdateSuccessMessage', true);
				} else {
					$this -> model -> showMessage('Attendant', 'UpdateFailedMessage', false);
				}

			} catch (\PDOException $e) {
				$this -> model -> getModelState() -> crudNotice('Insert', false, $this, $e, null);
			}
		} else {
			$this -> model -> getModelState() -> disconnected($this -> provider);
		}

		return $result;
	}

}

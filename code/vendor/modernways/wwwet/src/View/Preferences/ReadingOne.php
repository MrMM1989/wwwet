<?php
$succesMessage = false;
$title = '';
$message = '';
foreach($model->getModelState()->getBoard() as $boardItem){
    if($boardItem->getType() === 'SUCCESS'){
        $title = $boardItem->getName();
        $message = $boardItem->getText();
        $succesMessage = true;
    }
}
$partialView('Shared', 'AdminHeader');
?>
<!--Preferences\Index-->
<main>
    <section id="content">
        <h2>Voorkeuren</h2>
        <?php if($succesMessage): ?>
            <p class="message messagegreen">
                <strong><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<?php echo $title; ?></strong> <?php echo $message; ?>
            </p>
        <?php endif; ?>
        <p class="buttonbartop">
            <a class="button buttonred" href="?uc=Preferences-deleting_<?php echo $model->getId();?>">
                <i class="fa fa-trash" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Verwijderen
            </a>
        </p>

        <p><strong>Naam: </strong><?php echo $model->getName();?></p>
        <p>
            <strong>Voorkeurstype: </strong>
                <?php
                if ($model->getPreferenceTypeList())
                {
                    foreach ($model->getPreferenceTypeList() as $item)
                    {
                        if($item['Id'] == $model->getIdPreferenceType()):?>
                            <?php echo $item['Name'];?>
                        <?php endif; ?>
                        <?php
                    }
                }
                ?>
            </select>
        </p>
        <p><strong>Ja of Nee: </strong><?php echo $model->getYesNo() ? 'Ja' : 'Nee';?></p>
        <?php if(!empty($model->getIdDish())): ?>
            <p>
                <strong>Gerecht: </strong>
                    <?php
                    if ($model->getDishList())
                    {
                        foreach ($model->getDishList() as $item)
                        {
                            if($item['Id'] == $model->getIdDish()):?>
                                <?php echo $item['Name'];?>
                    <?php endif;
                        }
                    }
                    ?>
             </p>
        <?php endif; ?>
        <?php if(!empty($model->getIdDishCategory())): ?>
            <p>
                <strong>Type Gerecht: </strong>
                    <?php
                    if ($model->getDishCategoryList())
                    {
                        foreach ($model->getDishCategoryList() as $item)
                        {
                            if($item['Id'] == $model->getIdDishCategory()): ?>
                                <?php echo $item['Name'];?>
                            <?php endif;
                        }
                    }
                    ?>
            </p>
        <?php endif; ?>
        <?php if(!empty($model->getIdDietaryInfo())): ?>
            <p>
                <strong>Dieetinformatie: </strong>
                    <?php
                    if ($model->getDietaryInfoList())
                    {
                        foreach ($model->getDietaryInfoList() as $item)
                        {
                            if($item['Id'] == $model->getIdDietaryInfo()):?>
                                <?php echo $item['Description'];?>
                    <?php endif;
                        }
                    }
                    ?>
            </p>
        <?php endif; ?>
        <?php if(!empty($model->getPreperation())):?>
            <p><strong>Bereiding: </strong><?php echo $model->getPreperation(); ?></p>
        <?php endif; ?>
        <?php if(!empty($model->getPortionSize())):?>
            <p><strong>Portie: </strong><?php echo $model->getPortionSize(); ?></p>
        <?php endif; ?>

        <p><a href="index.php?uc=Preferences-index">Terug naar overzicht Voorkeuren</a></p>
    </section>
</main>
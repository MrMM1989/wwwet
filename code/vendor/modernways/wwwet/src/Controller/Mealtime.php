<?php
namespace ModernWays\WWWET\Controller;

class Mealtime extends \ModernWays\Mvc\Controller
{
    public function index()
    {
        $model = new \ModernWays\WWWET\Model\Mealtime($this->noticeBoard);
        $provider = new \ModernWays\WWWET\Dal\Provider('wwwet', $this->noticeBoard);
        $dal = new \ModernWays\WWWET\Dal\Mealtime($model, $provider);

        $dal->readingAll();
        return $this->view('Mealtime', 'Index', $model);
    }

    public function readingOne()
    {
        $model = new \ModernWays\WWWET\Model\Mealtime($this->noticeBoard);
        $provider = new \ModernWays\WWWET\Dal\Provider('wwwet', $this->noticeBoard);
        $dal = new \ModernWays\WWWET\Dal\Mealtime($model, $provider);

        if(isset($_POST['Mealtime-Id'])) {
            $model->setId(filter_input(INPUT_POST, 'Mealtime-Id', FILTER_SANITIZE_NUMBER_INT));
        } else {
            $model->setId($this->route->getId());
        }

        $dal->readingOne();
        return $this->view('Mealtime', 'ReadingOne', $model);
    }

    public function deleting() {
        $model = new \ModernWays\WWWET\Model\Mealtime($this->noticeBoard);
        $provider = new \ModernWays\WWWET\Dal\Provider('wwwet', $this->noticeBoard);
        $dal = new \ModernWays\WWWET\Dal\Mealtime($model, $provider);

        if(isset($_POST['Dish-Id'])) {
            $model->setId(filter_input(INPUT_POST, 'Dish-Id', FILTER_SANITIZE_NUMBER_INT));
        } else {
            $model->setId($this->route->getId());
        }

        $dal->readingOne();

        return $this->view('Mealtime', 'Deleting', $model);
    }

    public function delete() {
        $model = new \ModernWays\WWWET\Model\Mealtime($this->noticeBoard);
        $provider = new \ModernWays\WWWET\Dal\Provider('wwwet', $this->noticeBoard);
        $dal = new \ModernWays\WWWET\Dal\Mealtime($model, $provider);

        // Delete
        if(isset($_POST['Mealtime-Id'])) {
            $model->setId(filter_input(INPUT_POST, 'Mealtime-Id', FILTER_SANITIZE_NUMBER_INT));
        } else {
            $model->setId($this->route->getId());
        }

        $dal->deleteOne();
        $dal->readingAll();
        return $this->view('Mealtime', 'Index', $model);
    }

    public function Inserting() {
        $model = new \ModernWays\WWWET\Model\Mealtime($this->noticeBoard);

        return $this->view('Mealtime', 'Inserting', $model);
    }

    public function Insert() {
        $model = new \ModernWays\WWWET\Model\Mealtime($this->noticeBoard);
        $provider = new \ModernWays\WWWET\Dal\Provider('wwwet', $this->noticeBoard);
        $dal = new \ModernWays\WWWET\Dal\Mealtime($model, $provider);

        // Maken
        $model->setName(filter_input(INPUT_POST, 'Mealtime-Name', FILTER_SANITIZE_STRING));
        // Inserted By ??

        // Inserten
        $dal->createOne();
        // Refresh
        $dal->readingAll();
        // View
        return $this->view('Mealtime', 'Index', $model);

    }

    public function updating() {
        $model = new \ModernWays\WWWET\Model\Mealtime($this->noticeBoard);
        $provider = new \ModernWays\WWWET\Dal\Provider('wwwet', $this->noticeBoard);
        $dal = new \ModernWays\WWWET\Dal\Mealtime($model, $provider);

        // Select te updaten
        if(isset($_POST['Mealtime-Id'])) {
            $model->setId(filter_input(INPUT_POST, 'Mealtime-Id', FILTER_SANITIZE_NUMBER_INT));
        } else {
            $model->setId($this->route->getId());
        }
        $dal->readingOne();;

        return $this->view('Mealtime', 'Updating', $model);
    }

    public function update() {
        $model = new \ModernWays\WWWET\Model\Mealtime($this->noticeBoard);
        $provider = new \ModernWays\WWWET\Dal\Provider('wwwet', $this->noticeBoard);
        $dal = new \ModernWays\WWWET\Dal\Mealtime($model, $provider);

        // Model updaten
        $model->setId(filter_input(INPUT_POST, 'Mealtime-Id', FILTER_SANITIZE_NUMBER_INT));
        $model->setName(filter_input(INPUT_POST, 'Mealtime-Name', FILTER_SANITIZE_STRING));
        // Updated By ??

        //  updaten in database
        $dal->updateOne();

        // Refresh
        $dal->readingAll();
        return $this->view('Mealtime', 'ReadingOne', $model);
    }
}
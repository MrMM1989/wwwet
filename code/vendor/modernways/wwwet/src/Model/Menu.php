<?php
/**
 * Created by PhpStorm.
 * User: Kurt
 * Date: 1/05/2016
 * Time: 13:25
 */

namespace ModernWays\WWWET\Model;


class Menu extends \ModernWays\WWWET\Model\MessageModel
{
    private $id;
    private $date;
    private $name;
    private $idMenuType;
    private $idMealtime;

    protected $mealtimeList;        // Alle tijdstippen
    protected $dishList;            // Alle gerechten
    protected $dateList;            // Mogelijke datums
    protected $menuDishList;        // Gerechten in dit menu
    protected $kitchenMenuList; // Keukenmenus

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getIdMenuType()
    {
        return $this->idMenuType;
    }

    /**
     * @param mixed $idMenuType
     */
    public function setIdMenuType($idMenuType)
    {
        $this->idMenuType = $idMenuType;
    }


    /**
     * @return mixed
     */
    public function getIdMealtime()
    {
        return $this->idMealtime;
    }

    /**
     * @param mixed $idMealtime
     */
    public function setIdMealtime($idMealtime)
    {
        $this->idMealtime = $idMealtime;
    }

    /**
     * @return mixed
     */
    public function getMealtimeList()
    {
        return $this->mealtimeList;
    }

    /**
     * @param mixed $mealtimeList
     */
    public function setMealtimeList($mealtimeList)
    {
        $this->mealtimeList = $mealtimeList;
    }

    /**
     * @return mixed
     */
    public function getDishList()
    {
        return $this->dishList;
    }

    /**
     * @param mixed $dishList
     */
    public function setDishList($dishList)
    {
        $this->dishList = $dishList;
    }

    /**
     * @return mixed
     */
    public function getDateList()
    {
        return $this->dateList;
    }

    /**
     * @param mixed $dateList
     */
    public function setDateList($dateList)
    {
        $this->dateList = $dateList;
    }

    /**
     * @return mixed
     */
    public function getMenuDishList()
    {
        return $this->menuDishList;
    }

    /**
     * @param mixed $menuDishList
     */
    public function setMenuDishList($menuDishList)
    {
        $this->menuDishList = $menuDishList;
    }

    /**
     * @return mixed
     */
    public function getKitchenMenuList()
    {
        return $this->kitchenMenuList;
    }

    /**
     * @param mixed $kitchenMenuList
     */
    public function setKitchenMenuList($kitchenMenuList)
    {
        $this->kitchenMenuList = $kitchenMenuList;
    }


}
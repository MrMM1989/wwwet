-- Preference --
use wwwet;
-- Stored Procedures --
-- Insert --
DELIMITER ;;
CREATE PROCEDURE `PreferencesInsert`(
	OUT pId INT ,
    IN pName NVARCHAR(255),
    IN pIdDish INT,
    IN pIdDietaryInfo INT,
    IN pIdDishCategory INT,
    IN pIdPrefrenceType INT,
    IN pPreperation NVARCHAR(45),
    IN pPortionSize NVARCHAR(45)
)
BEGIN
INSERT INTO `wwwet`.`Preference`
	(
		`Name`,
		`IdDish`,
		`IdDietaryInfo`,
		`IdDishCategory`,
		`IdPreferenceType`,
		`Preperation`,
		`PortionSize`,
		`InsertedOn`
	)
VALUES
	(
		pName,
		pIdDish,
        pIdDietaryInfo,
        pIdDishCategory,
        pIdPrefrenceType,
        pPreperation,
        pPortionSize,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END ;;
DELIMITER ;;
-- DishCategory
use wwwet;

-- Update
DELIMITER ;;
CREATE PROCEDURE `DishCategoryUpdate`(
	pId INT ,
	pName NVARCHAR (255) ,
	pOrder INT
)
BEGIN
UPDATE `DishCategory`
	SET
		`Name` = pName,
		`Order` = pOrder,
		`UpdatedOn` = NOW()
	WHERE `DishCategory`.`Id` = pId;
END ;;
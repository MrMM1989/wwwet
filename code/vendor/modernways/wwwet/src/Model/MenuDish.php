<?php
/**
 * Created by PhpStorm.
 * User: Kurt
 * Date: 1/05/2016
 * Time: 13:32
 */

namespace ModernWays\WWWET\Model;


class MenuDish extends \ModernWays\Mvc\Model
{
    private $id;
    private $idMenu;
    private $idDish;
    private $optional;
    private $meal;
    protected $menuList;
    protected $dishList;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdMenu()
    {
        return $this->idMenu;
    }

    /**
     * @param mixed $idMenu
     */
    public function setIdMenu($idMenu)
    {
        $this->idMenu = $idMenu;
    }

    /**
     * @return mixed
     */
    public function getIdDish()
    {
        return $this->idDish;
    }

    /**
     * @param mixed $idDish
     */
    public function setIdDish($idDish)
    {
        $this->idDish = $idDish;
    }

    /**
     * @return mixed
     */
    public function getOptional()
    {
        return $this->optional;
    }

    /**
     * @param mixed $optional
     */
    public function setOptional($optional)
    {
        $this->optional = $optional;
    }

    /**
     * @return mixed
     */
    public function getMeal()
    {
        return $this->meal;
    }

    /**
     * @param mixed $meal
     */
    public function setMeal($meal)
    {
        $this->meal = $meal;
    }

    /**
     * @return mixed
     */
    public function getMenuList()
    {
        return $this->menuList;
    }

    /**
     * @param mixed $menuList
     */
    public function setMenuList($menuList)
    {
        $this->menuList = $menuList;
    }

    /**
     * @return mixed
     */
    public function getDishList()
    {
        return $this->dishList;
    }

    /**
     * @param mixed $dishList
     */
    public function setDishList($dishList)
    {
        $this->dishList = $dishList;
    }
}
<?php
namespace ModernWays\WWWET\Controller;

class Menu extends \ModernWays\WWWET\Controller\AppController
{
    // IdMenu 1 = Keukenmenu
    // idMenu 2 = Standaardmenu
    // idMenu 3 = Aangepast menu

    public function __construct($route, $noticeBoard)
    {
        // Call parent constructor
        parent::__construct($route, $noticeBoard);

        // Set modelName & load model and DAL
        $this->modelName = $this->initModelName($this);
        $this->loadModel($this->noticeBoard);
        $this->loadDal($this->noticeBoard);
    }

    // Dishes
    public function dishAdd() {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

        $_SESSION['DishList'][$id]['Id']  = $id;
        $_SESSION['DishList'][$id]['Name'] = $name;
    }

    public function readList(){

        echo json_encode($_SESSION['DishList']);

        exit();
    }

    public function removeItem(){
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);

        unset($_SESSION['DishList'][$id]);
    }

    // Algemene menu functies
    public function readingOne() {
        $this->model->setId($this->route->getId());

        $this->dal->readingOne();
        $this->dal->readMealtime();
        $this->dal->readLinkedDishes();
    }

    public function delete(){
        $this->model->setId($this->route->getId());
        $this->dal->deleteOne();
    }

    // Keukenmenu functies
    public function kitchenMenuIndex()
    {
        if($this->isAuthenticated())
        {
            $this->dal->readingAllKitchen();
            return $this->view('KitchenMenu', 'Index', $this->model);
        }
        else
        {
            return $this -> view('Home', 'Index', null);
        }
    }

    public function kitchenMenuReadingOne() {
        if($this->isAuthenticated())
        {
            $this->readingOne();
            return $this->view('KitchenMenu', 'ReadingOne', $this->model);
        }
        else
        {
            return $this -> view('Home', 'Index', null);
        }

    }

    public function kitchenMenuInserting()
    {
        if($this->isAuthenticated())
        {
            $dateHelper = new \ModernWays\WWWET\Helpers\DateHelper();

            $_SESSION['DishList'] = array();
            $this->dal->readMealtime();
            $this->model->setDishList($this->dal->reading('`Dish`', 'Name, Id', null, false));
            $this->model->setDateList($dateHelper->getDateList());

            return $this->view('KitchenMenu', 'Inserting', $this->model);
        }
        else
        {
            return $this->view('Home', 'Index', null);
        }
    }

    public function kitchenMenuInsert()
    {
        if($this->isAuthenticated())
        {
            $this->model->setIdMenuType(1);
            $this->model->setIdMealtime(filter_input(INPUT_POST, 'kitchenMenu-idMealtime', FILTER_SANITIZE_NUMBER_INT));
            $this->model->setDate(filter_input(INPUT_POST, 'kitchenMenu-date', FILTER_SANITIZE_STRING));

            //Insert
            $result = $this->dal->createOne();
            if($result) {
                $result = $this->dal->linkDishes();
            }

            if ($result) {
                $this->model->showMessage('Menu', 'InsertSuccessMessage', true);
            } else {
                $this->model->showMessage('Menu', 'InsertFailedMessage', false);
            }

            // Refresh
            $this->dal->readingAllKitchen();
            return $this->view('KitchenMenu', 'Index', $this->model);
        }
        else
        {
            return $this->view('Home', 'Index', null);
        }
    }

    public function kitchenMenuDeleting() {
        if($this->isAuthenticated())
        {
            $this->readingOne();
            return $this->view('KitchenMenu', 'Deleting', $this->model);
        }
        else
        {
            return $this->view('Home', 'Index', null);
        }
    }

    public function kitchenMenuDelete()
    {
        if($this->isAuthenticated())
        {
            $this->delete();
            $this->dal->readingAllKitchen();
            return $this->view('KitchenMenu', 'Index', $this->model);
        }
        else
        {
            return $this->view('Home', 'Index', null);
        }
    }

    // Standaardmenu functies
    public function standardMenuIndex()
    {
        if($this->isAuthenticated())
        {
            $this->dal->readingAllStandard();
            return $this->view('StandardMenu', 'Index', $this->model);
        }
        else
        {
            return $this -> view('Home', 'Index', null);
        }
    }

    public function standardMenuInserting()
    {
        if($this->isAuthenticated())
        {
            $dateHelper = new \ModernWays\WWWET\Helpers\DateHelper();

            $_SESSION['DishList'] = array();
            $this->dal->readMealtime();
            $this->dal->readKitchenMenus();
            $this->model->setDateList($dateHelper->getDateList());

            return $this->view('StandardMenu', 'Inserting', $this->model);
        }
        else
        {
            return $this->view('Home', 'Index', null);
        }
    }

    public function standardMenuInsert()
    {
        if($this->isAuthenticated())
        {
            $this->model->setIdMenuType(2);
            $this->model->setIdMealtime(filter_input(INPUT_POST, 'standardMenu-idMealtime', FILTER_SANITIZE_NUMBER_INT));
            $this->model->setDate(filter_input(INPUT_POST, 'standardMenu-date', FILTER_SANITIZE_STRING));
            $this->model->setName(filter_input(INPUT_POST, 'standardMenu-name', FILTER_SANITIZE_STRING));

            //Insert
            $result = $this->dal->createOne();
            if($result) {
                $result = $this->dal->linkDishes();
            }

            if ($result) {
                $this->model->showMessage('Menu', 'InsertSuccessMessage', true);
            } else {
                $this->model->showMessage('Menu', 'InsertFailedMessage', false);
            }

            // Refresh
            $this->dal->readingAllStandard();
            return $this->view('StandardMenu', 'Index', $this->model);
        }
        else
        {
            return $this->view('Home', 'Index', null);
        }
    }

    public function standardMenuDeleting() {
        if($this->isAuthenticated())
        {
            $this->readingOne();
            return $this->view('StandardMenu', 'Deleting', $this->model);
        }
        else
        {
            return $this->view('Home', 'Index', null);
        }
    }

    public function standardMenuDelete()
    {
        if($this->isAuthenticated())
        {
            $this->delete();
            $this->dal->readingAllStandard();
            return $this->view('StandardMenu', 'Index', $this->model);
        }
        else
        {
            return $this->view('Home', 'Index', null);
        }
    }

    public function standardMenuReadingOne() {
        if($this->isAuthenticated())
        {
            $this->readingOne();
            return $this->view('StandardMenu', 'ReadingOne', $this->model);
        }
        else
        {
            return $this -> view('Home', 'Index', null);
        }

    }

}
USE wwwet;
-- -----------------------------------------------------
-- Create Procedure RoleReadingOne
-- -----------------------------------------------------
DROP PROCEDURE IF EXISTS RoleSelectOne;
DELIMITER //
CREATE PROCEDURE RoleSelectOne(
	pId INT
)
BEGIN
SELECT Id, Name 
FROM Role
WHERE Id = pId;
END //
DELIMITER ;
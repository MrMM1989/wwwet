-- Menu --
use wwwet;
-- Stored Procedures --
-- Insert --
DELIMITER ;;
CREATE PROCEDURE `MenuSelectOne`(
	pId INT
)
BEGIN
	SELECT * from Menu
	WHERE Menu.id = pId;
END ;;
DELIMITER ;;
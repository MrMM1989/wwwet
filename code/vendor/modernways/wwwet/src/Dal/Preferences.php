<?php
namespace ModernWays\WWWET\Dal;
class Preferences extends \ModernWays\AnOrmApart\Dal
{
    public function __construct(\ModernWays\WWWET\Model\Preferences $model, \ModernWays\AnOrmApart\Provider $provider)
    {
        $this->model = $model;
        parent::__construct($provider);
    }

    public function deleteOne() {
        $result = parent::DeleteOne();

        if($result) {
            $this->model->showMessage('Preferences', 'DeleteSuccessMessage', true);
        } else {
            $this->model->showMessage('Preferences', 'DeleteFailedMessage', false);
        }
    }

    public function insert()
    {
        $result = false;
        if (!$this -> provider -> isConnected()) {
            $this -> provider -> open();
        }
        if ($this -> provider -> isConnected()) {
            try {

                //Prepare the statement
                $insertSql = 'INSERT INTO Preference (Name, YesNo, IdDish, IdDietaryInfo, IdDishCategory, IdPreferenceType, Preperation, PortionSize)
                              VALUES (:name, :yesNo, :idDish, :idDietaryInfo, :idDishCategory, :idPreferenceType, :preperation, :portionSize)';
                $preparedStatement = $this -> provider -> getPdo() -> prepare($insertSql);
                $preparedStatement -> bindValue(':name', $this -> model -> getName(), \PDO::PARAM_STR);
                $preparedStatement -> bindValue(':yesNo', $this->model->getYesNo(), \PDO::PARAM_BOOL);
                $preparedStatement -> bindValue(':idDish', $this -> model -> getIdDish(), \PDO::PARAM_INT);
                $preparedStatement -> bindValue(':idDietaryInfo', $this -> model -> getIdDietaryInfo(), \PDO::PARAM_INT);
                $preparedStatement -> bindValue(':idDishCategory', $this -> model -> getIdDishCategory(), \PDO::PARAM_INT);
                $preparedStatement -> bindValue(':idPreferenceType', $this -> model -> getIdPreferenceType(), \PDO::PARAM_INT);
                $preparedStatement -> bindValue(':preperation', $this -> model -> getPreperation(), \PDO::PARAM_STR);
                $preparedStatement -> bindValue(':portionSize', $this -> model -> getPortionSize(), \PDO::PARAM_STR);

                //Execute statement
                $result = $preparedStatement -> execute();

                if ($result) {
                    $this->model->showMessage('Preferences', 'InsertSuccessMessage', true);
                } else {
                    $this->model->showMessage('Preferences', 'InsertFailedMessage', true);
                }


            } catch (\PDOException $e) {
                $this -> model -> getModelState() -> crudNotice('Insert', false, $this, $e, null);
            }
        } else {
            $this -> model -> getModelState() -> disconnected($this -> provider);
        }

        return $result;
    }

    public function createOne()
    {
        $result = parent::createOne();

        if ($result) {
            $this->model->showMessage('Preferences', 'InsertSuccessMessage', true);
        } else {
            $this->model->showMessage('Preferences', 'InsertFailedMessage', false);
        }
    }

    public function updateOne() {
        $result = parent::updateOne();

        if ($result) {
            $this->model->showMessage('Preferences', 'UpdateSuccessMessage', true);
        } else {
            $this->model->showMessage('Preferences', 'UpdateFailedMessage', false);
        }
    }
}
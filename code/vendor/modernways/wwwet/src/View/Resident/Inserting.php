<?php $partialView('Shared', 'AdminHeader'); ?>
<main>
	<section id="content">
		<h2>Een nieuwe bewoner toevoegen</h2>
		<form action="index.php?uc=Resident-insert" method="post">
			<h3>Gegevens</h3>
			<ul>
				<li>
					<label for="resident-name">Naam: </label>
					<input id="resident-name" name="resident-name" type="text" required />
				</li>
				<li>
					<label for="resident-roomnumber">Kamernummer: </label>
					<input id="resident-roomnumber" name="resident-roomnumber" type="text" required />
				</li>
				<li>
					<label for="resident-group">Groep: </label>
					<select id="resident-group" name="resident-group">
						<option disabled="disabled">--Selecteer een groep--</option>
						<?php if(!empty($model->getGroupList())): ?>
							<?php foreach($model->getGroupList() as $item): ?>
								<option value="<?php echo $item['Id']; ?>"><?php echo $item['Name']; ?></option>
							<?php endforeach; ?>
						<?php endif; ?>
					</select>
				</li>
				<li>
					<label>Diabetes: </label>
					<input id="resident-diabetesyes" name="resident-diabetes" type="radio" value="true" required />
					<label for="resident-diabetesyes" class="autowidth">Ja</label>
					<input id="resident-diabetesno" name="resident-diabetes" type="radio" value="false" required checked/>
					<label for="resident-diabetesno" class="autowidth">Nee</label>
				</li>
				<li>
					<label for="resident-diet">Dieet: </label>
					<textarea id="resident-diet" name="resident-diet"></textarea>
				</li>
				<li>
					<label for="resident-extra">Extra: </label>
					<textarea id="resident-extra" name="resident-extra"></textarea>
				</li>
				<li class="buttonbarbottom">
					<button class="buttongreen" name="submit" type="submit">
						<i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Toevoegen
					</button>
					<a class="button buttonspaceleft buttonred" href="index.php?uc=Resident-index">
						<i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Annuleren
					</a>
				</li>
			</ul>
		</form>		
	</section>
</main>
USE wwwet;
-- -----------------------------------------------------
-- Create Procedure ResidentReadingAll
-- -----------------------------------------------------
DROP PROCEDURE IF EXISTS ResidentSelectAll;
DELIMITER //
CREATE PROCEDURE ResidentSelectAll()
BEGIN
SELECT Id, Name, NameGroup 
FROM vwSelectResidentGroup
ORDER BY Id;
END //
DELIMITER ;
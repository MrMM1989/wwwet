<?php 
	$succesMessage = false;
	$title = '';
	$message = '';
	foreach($model->getModelState()->getBoard() as $boardItem){
		if($boardItem->getType() === 'SUCCESS'){
				$title = $boardItem->getName();
				$message = $boardItem->getText();
				$succesMessage = true;			
		}
	} 
	$partialView('Shared', 'AdminHeader'); 
?>
<main>
	<section id="content">
		<h2>Details van een rol bekijken: <?php echo $model->getName(); ?></h2>
		<?php if($succesMessage): ?>
			<p class="message messagegreen">
				<strong><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<?php echo $title; ?></strong> <?php echo $message; ?>
			</p>
		<?php endif; ?>
		<p class="buttonbartop">
			<a class="button buttonyellow" href="index.php?uc=Role-updating_<?php echo $model->getId(); ?>">
				<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Rol wijzigen
			</a>
			<a class="button buttonspaceleft buttonred" href="index.php?uc=Role-deleting_<?php echo $model->getId(); ?>">
				<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Rol verwijderen
			</a>
		</p>
		<p><strong>Naam: </strong><?php echo $model->getName(); ?></p>
		<p><a href="index.php?uc=Role-index">Terug naar overzicht rollen</a></p>
	</section>
</main>
<?php
namespace ModernWays\WWWET\Controller;

class Admin extends \ModernWays\WWWET\Controller\AppController {
		
	public function index() {
		if ($this -> isAuthenticated()) {
			
			return $this -> view('Admin', 'Index', null);
		} else {
			
			return $this -> view('Home', 'Index', null);
		}
	}
}

-- Preference Type Select One --
use wwwet;
DELIMITER ;;
CREATE PROCEDURE `PreferenceTypeSelectOne`(
	pId INT
)
BEGIN
SELECT * FROM `PreferenceType`
	WHERE `PreferenceType`.`Id` = pId;
END ;;
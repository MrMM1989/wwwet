<?php $partialView('Shared', 'AdminHeader'); ?>
<!--Preferences/Inserting-->
<main>
    <section id="content">
        <h2>Een nieuwe voorkeur toevoegen</h2>
        <form action="index.php?uc=Preferences-Insert" method="post">
            <ul>
                <li>
                    <label for="preferences-name">Naam</label>
                    <input type="text" id="preferences-name" name="preferences-name" required/>
                </li>
                <li>
                    <p>vb. Geen suiker, geen frieten, grote portie,...</p>
                </li>
                <li id="preferences-yesNo" style="display:block;">
                    <fieldset>
                        <input type="radio" name="preferences-yesNo" value="0" id="rn"><label for="rn">Nee</label>
                        <input type="radio" name="preferences-yesNo" value="1" id="ry"><label for="ry">Ja</label>
                    </fieldset>
                </li>
                <li>
                    <label for="preferences-idPreferenceType">Type voorkeur</label>
                    <?php if($model->getPreferenceTypeList()): ?>
                        <select name="preferences-idPreferenceType" id="preferences-idPreferenceType">
                            <option disabled="disabled">--Selecteer een voorkeurstype--</option>
                            <?php foreach($model->getPreferenceTypeList() as $item): ?>
                                <option value="<?php echo $item['Id']?>"><?php echo $item['Name'];?></option>
                            <?php endforeach; ?>
                        </select>
                    <?php else: ?>
                        Geen voorkeuren gevonden in de database
                    <?php endif; ?>
                </li>

                <li id="Ingredient" style="display: none;">
                    <label for="preferences-idDietaryInfo">Dieetinformatie</label>
                    <?php if($model->getDietaryInfoList()): ?>
                        <select name="preferences-idDietaryInfo" id="preferences-idDietaryInfo">
                            <option disabled="disabled">--Selecteer een ingrediënt--</option>
                            <?php foreach($model->getDietaryInfoList() as $item): ?>
                                <option value="<?php echo $item['Id']?>"><?php echo $item['Description'];?></option>
                            <?php endforeach; ?>
                        </select>
                    <?php else: ?>
                        Geen dieetinformatie gevonden in de database
                    <?php endif; ?>
                </li>

                <li id="Gerecht" style="display: none;">
                    <label for="preferences-idDish">Gerecht</label>
                    <?php if($model->getDishList()): ?>
                        <select name="preferences-idDish" id="preferences-idDish">
                            <option disabled="disabled">--Selecteer een gerecht--</option>
                            <?php foreach($model->getDishList() as $item): ?>
                                <option value="<?php echo $item['Id']?>"><?php echo $item['Name'];?></option>
                            <?php endforeach; ?>
                        </select>
                    <?php else: ?>
                        Geen gerechten gevonden in de database
                    <?php endif; ?>
                </li>

                <li id="Type Gerecht" style="display: none;">
                    <label for="preferences-idDishCategory">Type gerecht</label>
                    <?php if($model->getDishCategoryList()): ?>
                        <select name="preferences-idDishCategory" id="preferences-idDishCategory">
                            <option disabled="disabled">--Selecteer een gerechtstype--</option>
                            <?php foreach($model->getDishCategoryList() as $item): ?>
                                <option value="<?php echo $item['Id']?>"><?php echo $item['Name'];?></option>
                            <?php endforeach; ?>
                        </select>
                    <?php else: ?>
                        Geen gerechtstypes gevonden in de database
                    <?php endif; ?>
                </li>

                <li id="Bereiding" style="display: none;">
                    <label for="preferences-preperation">Bereiding</label>
                    <input type="text" id="preferences-preperation" name="preferences-preperation"/>
                </li>

                <li id="Portie" style="display: none;">
                    <label for="preferences-portionSize">Portie</label>
                    <input type="text" id="preferences-portionSize" name="preferences-portionSize"/>
                </li>

                <li id="GerechtOfType" style="display:none;">
                    <fieldset>
                        <input type="radio" name="preferences-gerechtOfType" value="Gerecht">Gerecht<br>
                        <input type="radio" name="preferences-gerechtOfType" value="Type Gerecht">Type Gerecht<br>
                    </fieldset>
                </li>

                <li class="buttonbarbottom">
                    <button class="buttongreen" name="submit" type="submit">
                        <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Toevoegen
                    </button>
                    <a class="button buttonspaceleft buttonred" href="index.php?uc=Preferences-index">
                        <i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Annuleren
                    </a>
                </li>
            </ul>
        </form>
        <script src="/js/showhide-preferences.js"></script>
    </section>
</main>
<?php
/**
 * Created by PhpStorm.
 * User: Kurt
 * Date: 30/04/2016
 * Time: 14:28
 */
namespace ModernWays\WWWET\Model;

class Dish extends \ModernWays\WWWET\Model\MessageModel
{
    private $id;
    private $idDishCategory;
    private $name;
    private $byDefault;
    private $margin;
    private $monday;
    private $tuesday;
    private $wednesday;
    private $thursday;
    private $friday;
    private $saturday;
    private $sunday;

    protected $dishCategoryList;
    protected $dietaryInfoList;
    protected $linkedDietaryInfo;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        if ($this->validRequired($id, "DishId")) {
            $this->id = $id;
        }
    }

    /**
     * @return mixed
     */
    public function getIdDishCategory()
    {
        return $this->idDishCategory;
    }

    /**
     * @param mixed $idDishCategory
     */
    public function setIdDishCategory($idDishCategory)
    {
        if ($this->validRequired($idDishCategory, "DishCategory")) {
            $this->idDishCategory = $idDishCategory;
        }
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getByDefault()
    {
        return $this->byDefault;
    }

    /**
     * @param mixed $byDefault
     */
    public function setByDefault($byDefault)
    {
        $this->byDefault = ($byDefault == 1 ? true : false);
    }

    /**
     * @return mixed
     */
    public function getMargin()
    {
        return $this->margin;
    }

    /**
     * @param mixed $margin
     */
    public function setMargin($margin)
    {
        $this->margin = $margin;
    }

    /**
     * @return mixed
     */
    public function getMonday()
    {
        return $this->monday;
    }

    /**
     * @param mixed $monday
     */
    public function setMonday($monday)
    {
        $this->monday = ($monday == 1 ? true : false);
    }

    /**
     * @return mixed
     */
    public function getTuesday()
    {
        return $this->tuesday;
    }

    /**
     * @param mixed $tuesday
     */
    public function setTuesday($tuesday)
    {
        $this->tuesday = ($tuesday == 1 ? true : false);
    }

    /**
     * @return mixed
     */
    public function getWednesday()
    {
        return $this->wednesday;
    }

    /**
     * @param mixed $wednesday
     */
    public function setWednesday($wednesday)
    {
        $this->wednesday = ($wednesday == 1 ? true : false);
    }

    /**
     * @return mixed
     */
    public function getThursday()
    {
        return $this->thursday;
    }

    /**
     * @param mixed $thursday
     */
    public function setThursday($thursday)
    {
        $this->thursday = ($thursday == 1 ? true : false);
    }

    /**
     * @return mixed
     */
    public function getFriday()
    {
        return $this->friday;
    }

    /**
     * @param mixed $friday
     */
    public function setFriday($friday)
    {
        $this->friday = ($friday == 1 ? true : false);
    }

    /**
     * @return mixed
     */
    public function getSaturday()
    {
        return $this->saturday;
    }

    /**
     * @param mixed $saturday
     */
    public function setSaturday($saturday)
    {
        $this->saturday = ($saturday == 1 ? true : false);
    }

    /**
     * @return mixed
     */
    public function getSunday()
    {
        return $this->sunday;
    }

    /**
     * @param mixed $sunday
     */
    public function setSunday($sunday)
    {
        $this->sunday = ($sunday == 1 ? true : false);
    }

    /**
     * @return mixed
     */
    public function getDishCategoryList()
    {
        return $this->dishCategoryList;
    }

    /**
     * @param mixed $dishCategoryList
     */
    public function setDishCategoryList($dishCategoryList)
    {
        $this->dishCategoryList = $dishCategoryList;
    }

    /**
     * @return mixed
     */
    public function getDietaryInfoList()
    {
        return $this->dietaryInfoList;
    }

    /**
     * @param mixed $dietaryInfoList
     */
    public function setDietaryInfoList($dietaryInfoList)
    {
        $this->dietaryInfoList = $dietaryInfoList;
    }

    /**
     * @return mixed
     */
    public function getLinkedDietaryInfo()
    {
        return $this->linkedDietaryInfo;
    }

    /**
     * @param mixed $linkedDietaryInfo
     */
    public function setLinkedDietaryInfo($linkedDietaryInfo)
    {
        $this->linkedDietaryInfo = $linkedDietaryInfo;
    }



}
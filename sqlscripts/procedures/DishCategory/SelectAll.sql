-- DishCategory
use wwwet;

-- SelectAll
DELIMITER ;;
CREATE PROCEDURE `DishCategorySelectAll`(
)
BEGIN
SELECT *
	FROM `DishCategory`
	ORDER BY `Order`;
END ;;
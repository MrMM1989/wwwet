<?php
/**
 * Created by PhpStorm.
 * User: Kurt
 * Date: 1/05/2016
 * Time: 13:18
 */
namespace ModernWays\WWWET\Dal;
class Meal extends \ModernWays\AnOrmApart\Dal
{
    public function __construct(\ModernWays\WWWET\Model\Meal $model, \ModernWays\AnOrmApart\Provider $provider)
    {
        $this->model = $model;
        parent::__construct($provider);
    }

    public function readResidents() {
        if (!$this->provider->isConnected()) {
            $this->provider->open();
        }
        if ($this->provider->isConnected()) {
            try {
                $preparedStatement = $this->provider->getPdo()->prepare("CALL ResidentSelectAll()");
                $preparedStatement->execute();
                $this->rowCount = $preparedStatement->rowCount();
                $this->model->setResidentList($preparedStatement->fetchAll());

                if ($this->model->getList()) {
                    $this->model->getModelState()->crudNotice('Meal: readResidents', true, $this, null, $preparedStatement->errorInfo());
                } else {
                    $this->model->getModelState()->crudNotice('Meal: readResidents', false, $this, null, $preparedStatement->errorInfo());
                }

            } catch (\PDOException $e) {
                $this->model->getModelState()->crudNotice("Meal: readResidents, Error: {$e}", true, $this, $e);
            }
        }
    }

    public function readMenus() {
        if (!$this->provider->isConnected()) {
            $this->provider->open();
        }
        if ($this->provider->isConnected()) {
            try {
                $preparedStatement = $this->provider->getPdo()->prepare("CALL MenuSelectAll()");
                $preparedStatement->execute();
                $this->rowCount = $preparedStatement->rowCount();
                $this->model->setMenuList($preparedStatement->fetchAll());

                if ($this->model->getList()) {
                    $this->model->getModelState()->crudNotice('Meal: readMenus', true, $this, null, $preparedStatement->errorInfo());
                } else {
                    $this->model->getModelState()->crudNotice('Meal: readMenus', false, $this, null, $preparedStatement->errorInfo());
                }

            } catch (\PDOException $e) {
                $this->model->getModelState()->crudNotice("Meal: readMenus, Error: {$e}", true, $this, $e);
            }
        }
    }
}
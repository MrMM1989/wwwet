USE wwwet;
-- -----------------------------------------------------
-- Create Procedure AttendantReadingAll
-- -----------------------------------------------------
DROP PROCEDURE IF EXISTS AttendantSelectAll;
DELIMITER //
CREATE PROCEDURE AttendantSelectAll()
BEGIN
SELECT Id, Name, UserName 
FROM Attendant
ORDER BY Id;
END //
DELIMITER ;
-- Menu --
use wwwet;
-- Stored Procedures --
-- Insert --
DELIMITER ;;
CREATE PROCEDURE `MenuInsert`(
	OUT pId INT ,
	IN pDate date ,
	IN pName NVARCHAR (255) ,
    IN pIdMenuType INT ,
	IN pIdMealtime INT
)
BEGIN
INSERT INTO Menu
(
	Menu.Date,
    Menu.Name,
    Menu.IdMenuType,
    Menu.IdMealtime
)
VALUES
(
	pDate,
    pName,
    pIdMenuType,
    pIdMealtime
);
SELECT LAST_INSERT_ID() INTO pId;
END ;;
DELIMITER ;;
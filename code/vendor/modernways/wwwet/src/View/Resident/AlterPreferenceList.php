<?php $partialView('Shared', 'AdminHeader'); ?>
<main>
	<section id="content">
		<h2>Voorkeuren bewerken van bewoner: <?php echo $model -> getName(); ?></h2>
		<h3>Toegewezen voorkeuren</h3>
		<?php if(!empty($model->getResidentPreferenceList())): ?>
			<ul class="read-list">
			<?php foreach($model->getResidentPreferenceList() as $item): ?>	
					<li>
						<?php echo $item['NamePreference']; ?> - 
						<?php if(!empty($item['NameDish'])): ?>Alternatief: <?php echo $item['NameDish']; ?> - 
						<?php endif; ?>
						<a href="index.php?uc=Resident-removeResidentPreference_<?php echo $model->getId(); ?>&pid=<?php echo $item['IdPreference']; ?>">Verwijderen</a>
					</li>			
		<?php endforeach; ?> 
			</ul>
		<?php else: ?>
			<p>Geen toegewezen voorkeuren</p>
		<?php endif; ?>
		<h3>Voorkeur Toewijzen</h3>
		<form method="post" action="index.php?uc=Resident-addResidentPreference">
			<ul>
				<li>
					<input name="resident-id" type="hidden" value="<?php echo $model -> getId(); ?>" />
					<label for="preferences">Voorkeur: </label>
					<select id="preferences" name="preferences">
						<option disabled="disabled">--Selecteer een voorkeur--</option>
						<?php if(!empty($model->getPreferenceList())): ?>
							<?php foreach($model->getPreferenceList() as $item): ?>
								<option value="<?php echo $item['Id']; ?>"><?php echo $item['Name']; ?></option>
							<?php endforeach; ?>
						<?php endif; ?>
					</select>
				</li>
				<li>
					<label for="preference-alternative">Alternatief: </label>
					<select id="preference-alternative" name="preference-alternative">
						<option disabled="disabled">--Selecteer een alternatief--</option>
						<option value="-1">Geen alternatief</option>
						<?php if(!empty($model->getDishList())): ?>
							<?php foreach($model->getDishList() as $item): ?>
								<option value="<?php echo $item['Id']; ?>"><?php echo $item['Name']; ?></option>
							<?php endforeach; ?>
						<?php endif; ?>
					</select>
				</li>
               <li class="buttonbarbottom">
                 <button class="buttongreen" name="submit" type="submit">
                    <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Voorkeur toewijzen
                 </button>
               </li>
            </ul>
		</form>
		<p><a href="index.php?uc=Resident-readingOne_<?php echo $model -> getId(); ?>">Terug naar details bewoner</a></p>
	</section>
</main>
USE wwwet;
-- -----------------------------------------------------
-- Create Procedure ResidentReadingOne
-- -----------------------------------------------------
DROP PROCEDURE IF EXISTS ResidentSelectOne;
DELIMITER //
CREATE PROCEDURE ResidentSelectOne(
	pId INT
)
BEGIN
SELECT * 
FROM vwSelectResidentGroup
WHERE Id = pId;
END //
DELIMITER ;
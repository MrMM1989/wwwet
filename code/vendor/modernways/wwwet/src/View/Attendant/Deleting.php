<?php $partialView('Shared', 'AdminHeader'); ?>
<main>
	<section id="content">
		<h2>Een beheerder verwijderen: <?php echo $model->getName(); ?></h2>
		<p class="warning">Weet je zeker dat je deze beheerder wilt verwijderen?</p>
		<p class="buttonbartop">
			<a class="button buttongreen" href="index.php?uc=Attendant-delete_<?php echo $model->getId(); ?>">
				<i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Ja
			</a>
			<a class="button buttonspaceleft buttonred" href="index.php?uc=Attendant-readingOne_<?php echo $model->getId(); ?>">
				<i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Nee
			</a>
		</p>
		<p>
			<strong>Naam: </strong><?php echo $model->getName(); ?>
		</p>
		<p>
			<strong>Gebruikersnaam: </strong><?php echo $model->getUserName(); ?>
		</p>
		<p>
			<strong>Wachtwoord: </strong> <a href="index.php?uc=Attendant-changingPassword_<?php echo $model->getId() ?>">Wijzig wachtwoord</a> 
		</p>
		<p>
			<strong>Rol: </strong><?php echo $model->getNameRole(); ?>
		</p>
		<?php if(!empty($model->getAttendantGroupList())): ?>
			<p><strong>Groepen in beheer door deze beheerder</strong></p>
			<ul class="read-list">
			<?php foreach($model->getAttendantGroupList() as $item): ?>
				<li><?php echo $item['Name']; ?></li>
			<?php endforeach; ?>
			</ul>
		<?php endif; ?>
	</section>
</main>
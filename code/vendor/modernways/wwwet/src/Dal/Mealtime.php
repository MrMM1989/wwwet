<?php
/**
 * Created by PhpStorm.
 * User: Kurt
 * Date: 1/05/2016
 * Time: 13:29
 */

namespace ModernWays\WWWET\Dal;


class Mealtime extends \ModernWays\AnOrmApart\Dal
{
    public function __construct(\ModernWays\WWWET\Model\Mealtime $model, \ModernWays\AnOrmApart\Provider $provider)
    {
        $this->model = $model;
        parent::__construct($provider);
    }

    public function createOne()
    {
        $result = parent::createOne();

        if ($result) {
            $this->model->showMessage('Mealtime', 'InsertSuccessMessage', true);
        } else {
            $this->model->showMessage('Mealtime', 'InsertFailedMessage', false);
        }
    }

    public function updateOne() {
        $result = parent::updateOne();

        if ($result) {
            $this->model->showMessage('Mealtime', 'UpdateSuccessMessage', true);
        } else {
            $this->model->showMessage('Mealtime', 'UpdateFailedMessage', false);
        }
    }

    public function DeleteOne() {
        $result = parent::DeleteOne();

        if($result) {
            $this->model->showMessage('Mealtime', 'DeleteSuccessMessage', true);
        } else {
            $this->model->showMessage('Mealtime', 'DeleteFailedMessage', false);
        }
    }
}
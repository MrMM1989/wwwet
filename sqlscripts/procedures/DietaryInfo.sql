-- DietaryInfo
-- Create
DROP TABLE IF EXISTS `DietaryInfo`;
CREATE TABLE `DietaryInfo` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(1023) CHARACTER SET utf8 NOT NULL,
  `InsertedBy` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `InsertedOn` timestamp NULL DEFAULT NULL,
  `UpdatedBy` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `UpdatedOn` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Stored Procedures
-- Insert
DELIMITER ;;
CREATE PROCEDURE `DietaryInfoInsert`(
	OUT pId INT ,
	IN pDescription NVARCHAR (1023),
    IN pInsertedBy NVARCHAR (255)
)
BEGIN
INSERT INTO `DietaryInfo`
	(
		`DietaryInfo`.`Description`,
		`DietaryInfo`.`InsertedBy`,
		`DietaryInfo`.`InsertedOn`
	)
	VALUES
	(
		pDescription,
		pInsertedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END ;;

-- Update
DELIMITER ;;
CREATE PROCEDURE `DietaryInfoUpdate`(
	pId INT ,
	pDescription NVARCHAR (1023) ,
	pUpdatedBy NVARCHAR (255) 
)
BEGIN
UPDATE `DietaryInfo`
	SET
		`Description` = pDescription,
		`UpdatedBy` = pUpdatedBy,
		`UpdatedOn` = NOW()
	WHERE `DishCategory`.`Id` = pId;
END ;;

-- Delete
DELIMITER ;;
CREATE PROCEDURE `DietaryInfoDelete`(
	 pId INT 
)
BEGIN
DELETE FROM `DietaryInfo`
	WHERE `DietaryInfo`.`Id` = pId;
END ;;

-- Count
DELIMITER ;;
CREATE PROCEDURE `DietaryInfoCount`(
)
BEGIN
	SELECT count(*) FROM `DietaryInfo`;
END ;;

-- SelectAll
DELIMITER ;;
CREATE PROCEDURE `DietaryInfoSelectAll`(
)
BEGIN
SELECT * FROM `DietaryInfo`;
END ;;

-- SelectOne
DELIMITER ;;
CREATE PROCEDURE `DietaryInfoSelectOne`(
	 pId INT 
)
BEGIN
SELECT * FROM `DietaryInfo`
	WHERE `DietaryInfo`.`Id` = pId;
END ;;

-- SelectLikeX Description (Search)
DELIMITER ;;
CREATE PROCEDURE `DietaryInfoSelectLikeXDescription`(
	pDescription NVARCHAR (255) 
)
BEGIN
	SELECT * FROM `DietaryInfo`
	WHERE `DietaryInfo`.`Description` like CONCAT('%', pDescription, '%');
END ;;

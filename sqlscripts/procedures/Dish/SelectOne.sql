-- Dish --
use wwwet;
-- Stored Procedures --

-- Select One --
DELIMITER ;;
CREATE PROCEDURE `DishSelectOne`(
	 pId INT 
)
BEGIN
SELECT * FROM `Dish`
	WHERE `Dish`.`Id` = pId;
END ;;
<?php $partialView('Shared', 'AdminHeader'); ?>
<!--Preferences\Deleting-->
<main>
    <section id="content">
        <h2>Een voorkeur verwijderen: <?php echo $model->getName();?></h2>
        <p class="buttonbartop">
            <a class="button buttongreen" href="index.php?uc=Preferences-delete_<?php echo $model->getId(); ?>">
                <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Ja
            </a>
            <a class="button buttonspaceleft buttonred" href="index.php?uc=Preferences-readingOne_<?php echo $model->getId(); ?>">
                <i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Nee
            </a>
        </p>

        <p><strong>Naam: </strong><?php echo $model->getName();?></p>
        <p>
            <strong>Voorkeurstype: </strong>
            <?php
            if ($model->getPreferenceTypeList())
            {
                foreach ($model->getPreferenceTypeList() as $item)
                {
                    if($item['Id'] == $model->getIdPreferenceType()):?>
                        <?php echo $item['Name'];?>
                    <?php endif; ?>
                    <?php
                }
            }
            ?>
            </select>
        </p>
        <p><strong>Ja of Nee: </strong><?php echo $model->getYesNo() ? 'Ja' : 'Nee';?></p>
        <?php if(!empty($model->getIdDish)): ?>
            <p>
                <strong>Gerecht: </strong>
                <?php
                if ($model->getDishList())
                {
                    foreach ($model->getDishList() as $item)
                    {
                        if($item['Id'] == $model->getIdDish()):
                            ?>
                            <?php echo $item['Name'];?>
                        <?php endif;
                    }
                }
                ?>
                </select>
            </p>
        <?php endif; ?>
        <?php if(!empty($model->getIdDietaryInfo)): ?>
            <p>
                <strong>Dieetinformatie: </strong>
                <?php
                if ($model->getDietaryInfoList())
                {
                    foreach ($model->getDietaryInfoList() as $item)
                    {
                        if($item['Id'] == $model->getIdDietaryInfo()):?>
                            <?php echo $item['Description'];?>
                        <?php endif;
                    }
                }
                ?>
                </select>
            </p>
        <?php endif; ?>
        <?php if(!empty($model->getIdDishCategory())): ?>
            <p>
                <strong>Type Gerecht: </strong>
                <?php
                if ($model->getDishCategoryList())
                {
                    foreach ($model->getDishCategoryList() as $item)
                    {
                        if($item['Id'] == $model->getIdDishCategory()): ?>
                            <?php echo $item['Name'];?>
                        <?php endif;
                    }
                }
                ?>
                </select>
            </p>
        <?php endif; ?>
        <?php if(!empty($model->getIdDietaryInfo())): ?>
            <p>
                <strong>Dieetinformatie: </strong>
                <?php
                if ($model->getDietaryInfoList())
                {
                    foreach ($model->getDietaryInfoList() as $item)
                    {
                        if($item['Id'] == $model->getIdDietaryInfo()):?>
                            <?php echo $item['Description'];?>
                        <?php endif;
                    }
                }
                ?>
            </p>
        <?php endif; ?>
        <?php if(!empty($model->getPreperation())):?>
            <p><strong>Bereiding: </strong><?php echo $model->getPreperation(); ?></p>
        <?php endif; ?>
        <?php if(!empty($model->getPortionSize())):?>
            <p><strong>Portie: </strong><?php echo $model->getPortionSize(); ?></p>
        <?php endif; ?>
    </section>
</main>
<?php
/**
 * Created by PhpStorm.
 * User: Kurt
 * Date: 1/05/2016
 * Time: 13:05
 */
namespace ModernWays\WWWET\Dal;
class Group extends \ModernWays\AnOrmApart\Dal
{
    public function __construct(\ModernWays\WWWET\Model\Group $model, \ModernWays\AnOrmApart\Provider $provider)
    {
        $this->model = $model;
        parent::__construct($provider);
    }
	
	public function delete()
	{
		$result = false;
		if (!$this -> provider -> isConnected()) {
			$this -> provider -> open();
		}
		if ($this -> provider -> isConnected()) {
			try {

				//Prepare the statement
				$deleteSql = 'DELETE FROM `Group` WHERE Id = :id';
				$preparedStatement = $this -> provider -> getPdo() -> prepare($deleteSql);
				$preparedStatement -> bindValue(':id', $this -> model -> getId(), \PDO::PARAM_INT);
								
				//Execute statement
				$preparedStatement -> execute();
				
				$this->model->showMessage('Group', 'DeleteSuccessMessage', true);

			} catch (\PDOException $e) {
				$this -> model -> getModelState() -> crudNotice('Delete', false, $this, $e, null);
			}
		} else {
			$this -> model -> getModelState() -> disconnected($this -> provider);
		}

		return $result;	
	}
	
	public function insert()
	{
		$result = false;
		if (!$this -> provider -> isConnected()) {
			$this -> provider -> open();
		}
		if ($this -> provider -> isConnected()) {
			try {

				//Prepare the statement
				$insertSql = 'INSERT INTO `Group` (Name) VALUES (:name)';
				$preparedStatement = $this -> provider -> getPdo() -> prepare($insertSql);
				$preparedStatement -> bindValue(':name', $this -> model -> getName(), \PDO::PARAM_STR);
								
				//Execute statement
				$preparedStatement -> execute();
				
				$this->model->showMessage('Group', 'InsertSuccessMessage', true);

			} catch (\PDOException $e) {
				$this -> model -> getModelState() -> crudNotice('Insert', false, $this, $e, null);
			}
		} else {
			$this -> model -> getModelState() -> disconnected($this -> provider);
		}

		return $result;	
	}
	
	public function update(){
		
		if (!$this->provider->isConnected()) {
                $this->provider->open();
            }
            if ($this->provider->isConnected()) {
                try {
                    				
					//Prepare the statement
					$updateSql = 'UPDATE `Group` SET Name = :name WHERE Id = :id';
					$preparedStatement = $this->provider->getPdo() -> prepare($updateSql);
					$preparedStatement -> bindValue(':id', $this->model -> getId(), \PDO::PARAM_INT);
					$preparedStatement -> bindValue(':name', $this -> model -> getName(), \PDO::PARAM_STR);
					
					//Execute statement
					$result = $preparedStatement -> execute();
					
					$this->model->showMessage('Group', 'UpdateSuccessMessage', true);					
					
					
                } catch (\PDOException $e) {
                    $this->model->getModelState()->crudNotice('Update', false, $this, $e, null);
                }
            } else {
                $this->model->getModelState()->disconnected($this->provider);
            }
		
	}
	
}
<?php
namespace ModernWays\WWWET\Controller;

class AppController extends \ModernWays\WWWET\Controller\DalController {
	
	protected $authentication;
	protected $session;
		
	public function __construct($route, $noticeBoard)
	{
		// Call parent constructor
		parent::__construct($route, $noticeBoard);
		
		//Initialise $session
		$this->session = new \ModernWays\Identity\Session($this->noticeBoard);
		
		//Initialise $authentication
		$model = $this->loadForeignModel('Attendant', $this->noticeBoard);
		$dal = $this->loadForeignDal($model);
		
		$this->authentication = new \ModernWays\WWWET\Libraries\Authentication($this->noticeBoard, 
										$this->session, $dal);
	}
	
	public function isAuthenticated(){
		return $this->authentication->isLoggedIn();
	}
}
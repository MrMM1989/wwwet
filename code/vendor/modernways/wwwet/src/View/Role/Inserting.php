<?php $partialView('Shared', 'AdminHeader'); ?>
<main>
	<section id="content">
		<h2>Een nieuwe rol toevoegen</h2>
		<form action="index.php?uc=Role-insert" method="post">
			<ul>
				<li>
					<label for="role-name">Naam: </label>
					<input id="role-name" name="role-name" type="text" required />
				</li>
				<li class="buttonbarbottom">
					<button class="buttongreen" name="submit" type="submit">
						<i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Toevoegen
					</button>
					<a class="button buttonspaceleft buttonred" href="index.php?uc=Role-index">
						<i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Annuleren
					</a>
				</li>
			</ul>
		</form>		
	</section>
</main>
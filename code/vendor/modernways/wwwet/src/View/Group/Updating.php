<?php $partialView('Shared', 'AdminHeader'); ?>
<main>
	<section id="content">
		<h2>Een groep wijzigen: <?php echo $model->getName(); ?></h2>
		<form action="index.php?uc=Group-update" method="post">
			<ul>
				<li>
					<input id="group-id" name="group-id" type="hidden" value="<?php echo $model->getId(); ?>" />
					<label for="group-name">Naam: </label>
					<input id="group-name" name="group-name" type="text" required  value="<?php echo $model->getName(); ?>"/>
				</li>
				<li class="buttonbarbottom">
					<button class="buttongreen" name="submit" type="submit">
						<i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Wijzigen
					</button>
					<a class="button buttonspaceleft buttonred" href="index.php?uc=Group-readingOne_<?php echo $model->getId(); ?>">
						<i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Annuleren
					</a>
				</li>
			</ul>
		</form>		
	</section>
</main>
-- Dish --
use wwwet;
-- Stored Procedures --

-- Delete --
DELIMITER ;;
CREATE PROCEDURE `DishDelete`(
	 pId INT 
)
BEGIN
DELETE FROM `DishDiet` WHERE `DishDiet`.`IdDish` = pId;
DELETE FROM `Dish` WHERE `Dish`.`Id` = pId;
END ;;
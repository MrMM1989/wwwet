<?php
namespace ModernWays\WWWET\Controller;

class Dish extends \ModernWays\Mvc\Controller
{
    public function index()
    {
        $model = new \ModernWays\WWWET\Model\Dish($this->noticeBoard);
        $provider = new \ModernWays\WWWET\Dal\Provider('wwwet', $this->noticeBoard);
        $dal = new \ModernWays\WWWET\Dal\Dish($model, $provider);

        $dal->readingAll();
        $dal->readDishCategories();
        return $this->view('Dish', 'Index', $model);
    }

    public function readingOne()
    {
        $model = new \ModernWays\WWWET\Model\Dish($this->noticeBoard);
        $provider = new \ModernWays\WWWET\Dal\Provider('wwwet', $this->noticeBoard);
        $dal = new \ModernWays\WWWET\Dal\Dish($model, $provider);

        if(isset($_POST['Dish-Id'])) {
            $model->setId(filter_input(INPUT_POST, 'Dish-Id', FILTER_SANITIZE_NUMBER_INT));
        } else {
            $model->setId($this->route->getId());
        }
        $dal->readingOne();
        $dal->readDishCategories();
        $dal->readLinkedDietaryInfo();

        return $this->view('Dish', 'ReadingOne', $model);
    }

    public function inserting() {
        $model = new \ModernWays\WWWET\Model\Dish($this->noticeBoard);
        $provider = new \ModernWays\WWWET\Dal\Provider('wwwet', $this->noticeBoard);
        $dal = new \ModernWays\WWWET\Dal\Dish($model, $provider);

        $_SESSION['DietaryInfoList'] = array();
        $model->setDietaryInfoList($dal->reading('`DietaryInfo`', 'Description, Id', null, false));

        $dal->readDishCategories();
        return $this->view('Dish', 'Inserting', $model);
    }

    public function insert() {
        $provider = new \ModernWays\WWWET\Dal\Provider('wwwet', $this->noticeBoard);
        $model = new \ModernWays\WWWET\Model\Dish($this->noticeBoard);
        $dal = new \ModernWays\WWWET\Dal\Dish($model, $provider);

        // Nieuw Gerecht Maken
        $model->setName(filter_input(INPUT_POST, 'Dish-Name', FILTER_SANITIZE_STRING));
        $model->setIdDishCategory(filter_input(INPUT_POST, 'Dish-IdDishCategory', FILTER_SANITIZE_NUMBER_INT));
        $model->setByDefault(!empty($_POST['Dish-ByDefault']));
        $model->setMonday(!empty($_POST['Dish-Monday']));
        $model->setTuesday(!empty($_POST['Dish-Tuesday']));
        $model->setWednesday(!empty($_POST['Dish-Wednesday']));
        $model->setThursday(!empty($_POST['Dish-Thursday']));
        $model->setFriday(!empty($_POST['Dish-Friday']));
        $model->setSaturday(!empty($_POST['Dish-Saturday']));
        $model->setSunday(!empty($_POST['Dish-Sunday']));

        // Gerecht inserten in database & dietary info linken
        $dal->createOne();

        // Refresh
        $dal->readingAll();
        $dal->readDishCategories();
        return $this->view('Dish', 'Index', $model);
    }

    public function updating() {
        $model = new \ModernWays\WWWET\Model\Dish($this->noticeBoard);
        $provider = new \ModernWays\WWWET\Dal\Provider('wwwet', $this->noticeBoard);
        $dal = new \ModernWays\WWWET\Dal\Dish($model, $provider);

        // Select te updaten
        if(isset($_POST['Dish-Id'])) {
            $model->setId(filter_input(INPUT_POST, 'Dish-Id', FILTER_SANITIZE_NUMBER_INT));
        } else {
            $model->setId($this->route->getId());
        }
        $dal->readingOne();
        $dal->readDishCategories();
        $dal->readLinkedDietaryInfo();

        $model->setDietaryInfoList($dal->reading('`DietaryInfo`', 'Description, Id', null, false));
        $_SESSION['DietaryInfoList'] = array();
        foreach($model->getLinkedDietaryInfo() as $item) {
            $id = $item['Id'];
            $name = $item['Description'];
            $_SESSION['DietaryInfoList'][$id]['Id']  = $id;
            $_SESSION['DietaryInfoList'][$id]['Name'] = $name;
        }

        return $this->view('Dish', 'Updating', $model);
    }

    public function update() {
        $model = new \ModernWays\WWWET\Model\Dish($this->noticeBoard);
        $provider = new \ModernWays\WWWET\Dal\Provider('wwwet', $this->noticeBoard);
        $dal = new \ModernWays\WWWET\Dal\Dish($model, $provider);

        // Gerecht updaten
        if(isset($_POST['Dish-Id'])) {
            $model->setId(filter_input(INPUT_POST, 'Dish-Id', FILTER_SANITIZE_NUMBER_INT));
        } else {
            $model->setId($this->route->getId());
        }
        $model->setName(filter_input(INPUT_POST, 'Dish-Name', FILTER_SANITIZE_STRING));
        $model->setIdDishCategory(filter_input(INPUT_POST, 'Dish-IdDishCategory', FILTER_SANITIZE_NUMBER_INT));
        $model->setByDefault(!empty($_POST['Dish-ByDefault']));
        $model->setMonday(!empty($_POST['Dish-Monday']));
        $model->setTuesday(!empty($_POST['Dish-Tuesday']));
        $model->setWednesday(!empty($_POST['Dish-Wednesday']));
        $model->setThursday(!empty($_POST['Dish-Thursday']));
        $model->setFriday(!empty($_POST['Dish-Friday']));
        $model->setSaturday(!empty($_POST['Dish-Saturday']));
        $model->setSunday(!empty($_POST['Dish-Sunday']));

        // Gerecht updaten in database
        $dal->updateOne();

        // DishDiet updaten
        $dal->updateDietaryInfo();

        // Refresh
        $dal->readingOne();
        $dal->readDishCategories();
        $dal->readLinkedDietaryInfo();
        return $this->view('Dish', 'ReadingOne', $model);
    }

    public function deleting() {
        $model = new \ModernWays\WWWET\Model\Dish($this->noticeBoard);
        $provider = new \ModernWays\WWWET\Dal\Provider('wwwet', $this->noticeBoard);
        $dal = new \ModernWays\WWWET\Dal\Dish($model, $provider);

        if(isset($_POST['Dish-Id'])) {
            $model->setId(filter_input(INPUT_POST, 'Dish-Id', FILTER_SANITIZE_NUMBER_INT));
        } else {
            $model->setId($this->route->getId());
        }

        $dal->readingOne();
        $dal->readDishCategories();
        $dal->readLinkedDietaryInfo();

        return $this->view('Dish', 'Deleting', $model);
    }

    public function delete()
    {
        $model = new \ModernWays\WWWET\Model\Dish($this->noticeBoard);
        $provider = new \ModernWays\WWWET\Dal\Provider('wwwet', $this->noticeBoard);
        $dal = new \ModernWays\WWWET\Dal\Dish($model, $provider);

        // Delete
        if(isset($_POST['Dish-Id'])) {
            $model->setId(filter_input(INPUT_POST, 'Dish-Id', FILTER_SANITIZE_NUMBER_INT));
        } else {
            $model->setId($this->route->getId());
        }
        $dal->deleteOne();

        // Refresh
        $dal->readingAll();
        $dal->readDishCategories();
        return $this->view('Dish', 'Index', $model);
    }

    // Dietary Info
    public function dietaryInfoAdd() {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

        $_SESSION['DietaryInfoList'][$id]['Id']  = $id;
        $_SESSION['DietaryInfoList'][$id]['Name'] = $name;
    }

    public function readList(){

        echo json_encode($_SESSION['DietaryInfoList']);

        exit();
    }

    public function removeItem(){
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);

        unset($_SESSION['DietaryInfoList'][$id]);
    }
}
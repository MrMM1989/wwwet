<?php $partialView('Shared', 'AdminHeader'); ?>
<main>
    <section id="content">
        <h2>Een gerecht verwijderen: <?php echo $model->getName(); ?></h2>
        <p class="warning">Weet je zeker dat je dit gerecht wilt verwijderen?</p>
        <p class="buttonbartop">
            <a class="button buttongreen" href="index.php?uc=Dish-delete_<?php echo $model->getId(); ?>">
                <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Ja
            </a>
            <a class="button buttonspaceleft buttonred" href="index.php?uc=Dish-readingOne_<?php echo $model->getId(); ?>">
                <i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Nee
            </a>
        </p>

        <p><strong>Naam: </strong><?php echo $model->getName(); ?></p>
        <p><strong>Marge: </strong><?php echo $model->getMargin(); ?></p>
        <p>
            <strong>Categorie: </strong>
            <select name="Dish-IdDishCategory" id="Dish-IdDishCategory" disabled>
                <?php
                if ($model->getDishCategoryList())
                {
                    foreach ($model->getDishCategoryList() as $item)
                    {
                        ?>
                        <option
                            value="<?php echo $item['Id'];?>"
                            <?php echo ($item['Id'] == $model->getIdDishCategory() ? ' selected' : '')?>>
                            <?php echo $item['Name'];?>
                        </option>
                        <?php
                    }
                }
                ?>
            </select>
        </p>
        <p><strong>Standaard Alternatief: </strong><?php echo ($model->getByDefault() ? 'Ja' : 'Nee'); ?></p>
        <p><strong>Terugkerend Op: </strong></p>
        <ul>
            <?php echo ($model->getMonday() ? '<li>Maandag</li>' : '');?>
            <?php echo ($model->getTuesday() ? '<li>Dinsdag</li>' : '');?>
            <?php echo ($model->getWednesday() ? '<li>Woensdag</li>' : '');?>
            <?php echo ($model->getThursday() ? '<li>Donderdag</li>' : '');?>
            <?php echo ($model->getFriday() ? '<li>Vrijdag</li>' : '');?>
            <?php echo ($model->getSaturday() ? '<li>Zaterdag</li>' : '');?>
            <?php echo ($model->getSunday() ? '<li>Zondag</li>' : '');?>
        </ul>
        <h3>Dieet informatie: </h3>
        <?php if ($model->getLinkedDietaryInfo()): ?>
            <ul>
                <?php foreach ($model->getLinkedDietaryInfo() as $item): ?>
                    <li><?php echo $item['Description'];?></li>
                <?php endforeach; ?>
            </ul>
        <?php else: ?>
            <p>Geen dieet informatie.</p>
        <?php endif; ?>
    </section>
</main>
<?php
	
	$loginMessage = false;
	if(isset($model)){
		$title = '';
		$message = '';
		foreach($model->getModelState()->getBoard() as $boardItem){
			if($boardItem->getType() === 'LOGIN'){
					$title = $boardItem->getName();
					$message = $boardItem->getText();
					$loginMessage = true;			
			}
		}
	}
?>
<div id="pbody" class="grey">
	<section id="login">
	<section id="loginheader">
		<p>Inloggen</p>
	</section>
	<form id="loginform" action="index.php?uc=Home-login" method="post">
		<ul>
			<?php if($loginMessage): ?>
				<li class="warning">
					Inloggegevens niet correct! Probeer opnieuw!
				</li>
			<?php endif; ?>
			<li>
				<label for="uname">Gebruikersnaam:</label>
				<input id="uname" name="uname" type="text" />
			</li>
			<li>
				<label for="upassword">Wachtwoord:</label>
				<input id="upassword" name="upassword" type="password" />
			</li>
			<li>
				<input name="login"  type="submit" value="Inloggen"/>
			</li>
			<!--<li>
				<a href="">Rapporten afprinten zonder in te loggen</a>
			</li>-->
		</ul>
	</form>
</section>

</div>

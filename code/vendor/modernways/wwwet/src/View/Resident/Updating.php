<?php $partialView('Shared', 'AdminHeader'); ?>
<main>
	<section id="content">
		<h2>Een nieuwe bewoner bewerken: <?php echo $model->getName(); ?></h2>
		<form action="index.php?uc=Resident-update" method="post">
			<h3>Gegevens</h3>
			<ul>
				<li>
					<input name="resident-id" type="hidden" value="<?php echo $model->getId(); ?>" />
					<label for="resident-name">Naam: </label>
					<input id="resident-name" name="resident-name" type="text" value="<?php echo $model->getName(); ?>" required />
				</li>
				<li>
					<label for="resident-roomnumber">Kamernummer: </label>
					<input id="resident-roomnumber" name="resident-roomnumber" type="text" value="<?php echo $model->getRoomNumber(); ?>" required />
				</li>
				<li>
					<label for="resident-group">Groep: </label>
					<select id="resident-group" name="resident-group">
						<option disabled="disabled">--Selecteer een groep--</option>
						<?php if(!empty($model->getGroupList())): ?>
							<?php foreach($model->getGroupList() as $item): ?>
								<?php if ($model->getIdGroup() === $item['Id']): ?>
									<option value="<?php echo $item['Id']; ?>" selected><?php echo $item['Name']; ?></option>
								<?php else: ?>
									<option value="<?php echo $item['Id']; ?>"><?php echo $item['Name']; ?></option>
								<?php endif; ?>
							<?php endforeach; ?>
						<?php endif; ?>
					</select>
				</li>
				<li>
					<label>Diabetes: </label>
					<input id="resident-diabetesyes" name="resident-diabetes" type="radio" value="true" required <?php echo ($model->getDiabetes() == true)? 'checked' : ''; ?> />
					<label for="resident-diabetesyes" class="autowidth">Ja</label>
					<input id="resident-diabetesno" name="resident-diabetes" type="radio" value="false" required <?php echo ($model->getDiabetes() == false)? 'checked' : ''; ?>/>
					<label for="resident-diabetesno" class="autowidth">Nee</label>
				</li>
				<li>
					<label for="resident-diet">Dieet: </label>
					<textarea id="resident-diet" name="resident-diet"><?php echo $model->getDiet(); ?></textarea>
				</li>
				<li>
					<label for="resident-extra">Extra: </label>
					<textarea id="resident-extra" name="resident-extra"><?php echo $model->getExtra(); ?></textarea>
				</li>
				<li class="buttonbarbottom">
					<button class="buttongreen" name="submit" type="submit">
						<i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Opslaan
					</button>
					<a class="button buttonspaceleft buttonred" href="index.php?uc=Resident-readingOne_<?php echo $model->getId(); ?>">
						<i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Annuleren
					</a>
				</li>
			</ul>
		</form>		
	</section>
</main>
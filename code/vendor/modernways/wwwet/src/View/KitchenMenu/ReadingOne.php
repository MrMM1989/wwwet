<?php
$succesMessage = false;
$title = '';
$message = '';
foreach($model->getModelState()->getBoard() as $boardItem){
    if($boardItem->getType() === 'SUCCESS'){
        $title = $boardItem->getName();
        $message = $boardItem->getText();
        $succesMessage = true;
    }
}
$partialView('Shared', 'AdminHeader');
?>
<!--KitchenMenu/ReadingOne-->
<main>
    <section id="content">
        <h2>Details van een Keukenmenu: <?php echo $model->getDate();?></h2>
            <?php if($succesMessage): ?>
                <p class="message messagegreen">
                    <strong><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<?php echo $title; ?></strong> <?php echo $message; ?>
                </p>
            <?php endif; ?>
            <!--Buttons-->
            <p class="buttonbartop">
                <a class="button buttonred" href="?uc=Menu-KitchenMenuDeleting_<?php echo $model->getId();?>">
                    <i class="fa fa-trash" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Verwijderen
                </a>
            </p>

            <p><strong>Datum: </strong><?php echo $model->getDate(); ?></p>
            <p><strong>Tijdstip: </strong>
                <?php if ($model->getMealtimeList()): ?>
                    <?php foreach($model->getMealtimeList() as $item): ?>
                        <?php if($item['Id'] == $model->getIdMealtime()): ?>
                            <?php echo $item['Name']; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </p>
            <h3>Gerechten: </h3>
            <?php if ($model->getMenuDishList()): ?>
                <ul class="read-list">
                    <?php foreach ($model->getMenuDishList() as $item): ?>
                        <li><?php echo $item['Name'];?></li>
                    <?php endforeach; ?>
                </ul>
            <?php else: ?>
                <p>Geen gerechten.</p>
            <?php endif; ?>

            <p><a href="index.php?uc=Menu-KitchenMenuIndex">Terug naar overzicht Keukenmenu's</a></p>
    </section>
</main>
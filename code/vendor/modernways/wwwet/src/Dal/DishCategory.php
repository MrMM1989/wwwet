<?php
/**
 * Created by PhpStorm.
 * User: Kurt
 * Date: 30/04/2016
 * Time: 18:22
 */
namespace ModernWays\WWWET\Dal;
class DishCategory extends \ModernWays\AnOrmApart\Dal
{
    public function __construct(\ModernWays\WWWET\Model\DishCategory $model, \ModernWays\AnOrmApart\Provider $provider)
    {
        $this->model = $model;
        parent::__construct($provider);
    }

    public function createOne()
    {
        $result = parent::createOne();

        if ($result) {
            $this->model->showMessage('DishCategory', 'InsertSuccessMessage', true);
        } else {
            $this->model->showMessage('DishCategory', 'InsertFailedMessage', false);
        }
    }

    public function updateOne() {
        $result = parent::updateOne();

        if ($result) {
            $this->model->showMessage('DishCategory', 'UpdateSuccessMessage', true);
        } else {
            $this->model->showMessage('DishCategory', 'UpdateFailedMessage', false);
        }
    }

    public function DeleteOne() {
        $result = parent::DeleteOne();

        if($result) {
            $this->model->showMessage('DishCategory', 'DeleteSuccessMessage', true);
        } else {
            $this->model->showMessage('DishCategory', 'DeleteFailedMessage', false);
        }
    }
}
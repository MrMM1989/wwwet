<?php

namespace ModernWays\WWWET\Model;

class Attendant extends \ModernWays\WWWET\Model\MessageModel
{
    private $id;
    private $name;
    private $userName;
    private $password;
    private $idRole;
	private $nameRole;
	
	protected $attendantGroupList;
    protected $groupList;
	protected $roleList;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getUserName()
    {
        return $this->userName;
    }

    public function setUserName($userName)
    {
        $this->userName = $userName;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getIdRole()
    {
        return $this->idRole;
    }

    public function setIdRole($idRole)
    {
        $this->idRole = $idRole;
    }
	
	public function getNameRole()
	{
		return $this->nameRole;
	}
	
	public function setNameRole($nameRole)
	{
		$this->nameRole = $nameRole;
	}
	
	public function getAttendantGroupList()
	{
		return $this->attendantGroupList;
	}
	
	public function setAttendantGroupList($attendantGroupList)
	{
		$this->attendantGroupList = $attendantGroupList;
	}

    public function getGroupList()
    {
        return $this->groupList;
    }

    public function setGroupList($groupList)
    {
        $this->groupList = $groupList;
    }
	
	public function getRoleList()
	{
		return $this->roleList;
	}
	
	public function setRoleList($roleList)
	{
		$this->roleList = $roleList;
	}
}
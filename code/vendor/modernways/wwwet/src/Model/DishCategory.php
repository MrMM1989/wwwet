<?php
/**
 * Created by PhpStorm.
 * User: Kurt
 * Date: 30/04/2016
 * Time: 17:55
 */
namespace ModernWays\WWWET\Model;

class DishCategory extends \ModernWays\WWWET\Model\MessageModel
{
    private $id;
    private $name;
    private $order;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    // Insert/Update
    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param mixed $updatedBy
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;
    }

    /**
     * @return mixed
     */
    public function getInsertedBy()
    {
        return $this->insertedBy;
    }

    /**
     * @param mixed $insertedBy
     */
    public function setInsertedBy($insertedBy)
    {
        $this->insertedBy = $insertedBy;
    }
}

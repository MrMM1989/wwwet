<?php $partialView('Shared', 'AdminHeader'); ?>
    <!--Dish/ReadingOne-->
    <main>
        <section id="content">
            <h2>Gerecht updaten: <?php echo $model->getName(); ?></h2>
                <form action="index.php?uc=Dish-Update" method="post">
                    <ul>
                        <li>
                            <input id="Dish-Id" type="hidden" name="Dish-Id" value="<?php echo $model->getId();?>" />
                            <label for="Dish-Name">Naam</label>
                            <input class="text" id="Dish-Name" name="Dish-Name" type="text"
                                   value="<?php echo $model->getName();?>" />
                        </li>
                        <li>
                            <label for="Dish-Margin">Marge</label>
                            <input class="integer" id="Dish-Margin" name="Dish-Margin" type="text"
                                   value="<?php echo $model->getMargin();?>" />
                        </li>
                        <li>
                            <input id="Dish-ByDefault" name="Dish-ByDefault" type="checkbox"
                                <?php echo ($model->getByDefault() ? ' checked' : '');?> />
                            <label for="Dish-ByDefault">Standaard alternatief</label>
                        </li>
                        <li>
                            <label for="Dish-IdDishCategory">Categorie</label>
                            <select name="Dish-IdDishCategory" id="Dish-IdDishCategory">
                                <?php
                                if ($model->getDishCategoryList())
                                {
                                    $i = 1;
                                    foreach ($model->getDishCategoryList() as $item)
                                    {
                                        ?>
                                        <option
                                            value="<?php echo $item['Id'];?>"
                                            <?php echo ($item['Id'] == $model->getIdDishCategory() ? ' selected' : '')?>>
                                            <?php echo $item['Name'];?>
                                        </option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </li>
                        <li>
                            <label>Terugkerend op</label>
                            <div>
                                <input id="Dish-Monday" name="Dish-Monday" type="checkbox"
                                    <?php echo ($model->getMonday() ? ' checked' : '');?> />
                                <label for="Dish-Monday">Maandag</label>
                            </div>
                            <div>
                                <input id="Dish-Tuesday" name="Dish-Tuesday" type="checkbox"
                                    <?php echo ($model->getTuesday() ? ' checked' : '');?> />
                                <label for="Dish-Tuesday">Dinsdag</label>
                            </div>
                            <div>
                                <input id="Dish-Wednesday" name="Dish-Wednesday" type="checkbox"
                                    <?php echo ($model->getWednesday() ? ' checked' : '');?> />
                                <label for="Dish-Wednesday">Woensdag</label>
                            </div>
                            <div>
                                <input id="Dish-Thursday" name="Dish-Thursday" type="checkbox"
                                    <?php echo ($model->getThursday() ? ' checked' : '');?> />
                                <label for="Dish-Thursday">Donderdag</label>
                            </div>
                            <div>
                                <input id="Dish-Friday" name="Dish-Friday" type="checkbox"
                                    <?php echo ($model->getFriday() ? ' checked' : '');?> />
                                <label for="Dish-Friday">Vrijdag</label>
                            </div>
                            <div>
                                <input id="Dish-Saturday" name="Dish-Saturday" type="checkbox"
                                    <?php echo ($model->getSaturday() ? ' checked' : '');?> />
                                <label for="Dish-Saturday">Zaterdag</label>
                            </div>
                            <div>
                                <input id="Dish-Sunday" name="Dish-Sunday" type="checkbox"
                                    <?php echo ($model->getSunday() ? ' checked' : '');?> />
                                <label for="Dish-Sunday">Zondag</label>
                            </div>
                        </li>
                    </ul>

                    <h3>Dieet Informatie</h3>
                    <ul>
                        <li>
                            <ul id="ajax" class="edit-list">
                                <li>Geen dieet informatie</li>
                            </ul>
                        </li>
                        <li>
                            <label for="dietary-info">Voeg dieet informatie toe: </label>
                            <select id="dietary-info" class="smaller" name="dietary-info">
                                <option disabled="disabled">--Selecteer dieet informatie--</option>
                                <?php if(!empty($model->getDietaryInfoList())): ?>
                                    <?php foreach($model->getDietaryInfoList() as $item): ?>
                                        <option value="<?php echo $item['Id']; ?>"><?php echo $item['Description']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                            <button id="dietary-info-add" class="horizontal buttongreen buttonspaceleft" name="dietary-info-add" type="button">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                        </li>
                    </ul>

                    <ul>
                        <li class="buttonbarbottom">
                            <button class="buttongreen" name="submit" type="submit">
                                <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Opslaan
                            </button>
                            <a class="button buttonspaceleft buttonred" href="index.php?uc=Dish-index">
                                <i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Annuleren
                            </a>
                        </li>
                    </ul>
                </form>
        </section>
    </main>
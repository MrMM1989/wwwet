<?php $partialView('Shared', 'AdminHeader'); ?>
    <!--DishCategory/Inserting-->
    <main>
        <section id="content">
            <h2>Nieuwe Categorie</h2>
            <form action="index.php?uc=DishCategory-insert" method="post">
                <ul>
                    <li>
                        <label for="DishCategory-Name">Naam</label>
                        <input id="DishCategory-Name" name="DishCategory-Name" type="text"/>
                    </li>
                    <li>
                        <label for="DishCategory-Order">Volgorde</label>
                        <input id="DishCategory-Order" name="DishCategory-Order" type="text"/>
                    </li>

                    <li class="buttonbarbottom">
                        <button class="buttongreen" name="submit" type="submit">
                            <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Toevoegen
                        </button>
                        <a class="button buttonspaceleft buttonred" href="index.php?uc=DishCategory-index">
                            <i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Annuleren
                        </a>
                    </li>
                </ul>
            </form>
        </section>
    </main>
USE wwwet;
-- -----------------------------------------------------
-- Create View vwSelectAttendantGroupList
-- -----------------------------------------------------
DROP VIEW IF EXISTS vwSelectAttendantGroupList;
CREATE VIEW vwSelectAttendantGroupList AS
SELECT g.Id, g.Name, a.Id AS IdAttendant
FROM `Group` AS g
INNER JOIN AttendantGroup AS ag
ON g.Id = ag.IdGroup
INNER JOIN Attendant AS a
ON ag.IdAttendant = a.Id;
<?php 
	$succesMessage = false;
	$title = '';
	$message = '';
	foreach($model->getModelState()->getBoard() as $boardItem){
		if($boardItem->getType() === 'SUCCESS'){
				$title = $boardItem->getName();
				$message = $boardItem->getText();
				$succesMessage = true;			
		}
	} 
	$partialView('Shared', 'AdminHeader'); 
?>
<main>
	<section id="content">
		<h2>Details van een beheerder bekijken: <?php echo $model->getName(); ?></h2>
		<?php if($succesMessage): ?>
			<p class="message messagegreen">
				<strong><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<?php echo $title; ?></strong> <?php echo $message; ?>
			</p>
		<?php endif; ?>
		<p class="buttonbartop">
			<a class="button buttonyellow" href="index.php?uc=Attendant-updating_<?php echo $model->getId(); ?>">
				<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Beheerder wijzigen
			</a>
			<a class="button buttonspaceleft buttonred" href="index.php?uc=Attendant-deleting_<?php echo $model->getId(); ?>">
				<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Beheerder verwijderen
			</a>
		</p>
		<p>
			<strong>Naam: </strong><?php echo $model->getName(); ?>
		</p>
		<p>
			<strong>Gebruikersnaam: </strong><?php echo $model->getUserName(); ?>
		</p>
		<p>
			<strong>Wachtwoord: </strong> <a href="index.php?uc=Attendant-changingPassword_<?php echo $model->getId() ?>">Wijzig wachtwoord</a> 
		</p>
		<p>
			<strong>Rol: </strong><?php echo $model->getNameRole(); ?>
		</p>
		<?php if(!empty($model->getAttendantGroupList())): ?>
			<p><strong>Groepen in beheer door deze beheerder</strong></p>
			<ul class="read-list">
			<?php foreach($model->getAttendantGroupList() as $item): ?>
				<li><?php echo $item['Name']; ?></li>
			<?php endforeach; ?>
			</ul>
		<?php endif; ?>
		<p><a href="index.php?uc=Attendant-index">Terug naar overzicht beheerders</a></p>
	</section>
</main>
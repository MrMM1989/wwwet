<?php $partialView('Shared', 'AdminHeader'); ?>
<!--Mealtime/ReadingOne-->
<main>
    <section id="content">
        <h2>Nieuwe tijdstip toevoegen</h2>
        <form action="index.php?uc=Mealtime-Insert" method="post">
            <ul>
                <li>
                    <label for="Mealtime-Name">Naam</label>
                    <input id="Mealtime-Name" name="Mealtime-Name" type="text" />
                </li>
            </ul>

            <ul>
                <li class="buttonbarbottom">
                    <button class="buttongreen" name="submit" type="submit">
                        <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Opslaan
                    </button>
                    <a class="button buttonspaceleft buttonred" href="index.php?uc=Mealtime-index">
                        <i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Annuleren
                    </a>
                </li>
            </ul>
        </form>
    </section>
</main>

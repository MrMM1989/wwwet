function filter() {
    var kitchenMenu = document.getElementById('kitchenMenu-list');
    var dishDropdowns = document.getElementsByName('menu-dishes');

    for (var i = 0; i < dishDropdowns.length; i++) {
        var liId = dishDropdowns[i].id.replace('menu-dishes', '');
        document.getElementById('menu-dishes-li' + liId).style.display = 'none';
    }

    document.getElementById('menu-dishes-li' + kitchenMenu.value).style.display = '';
}

document.addEventListener('DOMContentLoaded', function(event) {
    filter();
    var date = document.getElementById('kitchenMenu-list');
    date.onchange = filter;
});
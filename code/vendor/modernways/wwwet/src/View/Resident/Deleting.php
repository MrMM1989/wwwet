<?php 
	$partialView('Shared', 'AdminHeader'); 
?>
<main>
	<section id="content">
		<h2>Een bewoner verwijderen: <?php echo $model->getName(); ?></h2>
		<p class="warning">Weet je zeker dat je deze bewoner wilt verwijderen?</p>
		<p class="buttonbartop">
			<a class="button buttongreen" href="index.php?uc=Resident-delete_<?php echo $model->getId(); ?>">
				<i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Ja
			</a>
			<a class="button buttonspaceleft buttonred" href="index.php?uc=Resident-readingOne_<?php echo $model->getId(); ?>">
				<i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Nee
			</a>
		</p>
		<p>
			<strong>Naam: </strong><?php echo $model->getName(); ?>
		</p>
		<p>
			<strong>Kamernummer: </strong><?php echo $model->getRoomNumber(); ?>
		</p>
		<p>
			<strong>Diabetes: </strong> <?php echo ($model->getDiabetes())? 'Ja' : 'Nee' ?> 
		</p>
		<p>
			<strong>Groep: </strong><?php echo $model->getNameGroup(); ?>
		</p>
		<?php if(!empty($model->getDiet())): ?>
			<p>
				<strong>Dieet: </strong><?php echo $model->getDiet(); ?>
			</p>
		<?php endif; ?>
		<?php if(!empty($model->getExtra())): ?>
			<p>
				<strong>Extra: </strong><?php echo $model->getExtra(); ?>
			</p>
		<?php endif; ?>
	</section>
</main>
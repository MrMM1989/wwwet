<?php $partialView('Shared', 'AdminHeader'); ?>
<main>
	<section id="content">
		<h2>Een nieuwe groep toevoegen</h2>
		<form action="index.php?uc=Group-insert" method="post">
			<ul>
				<li>
					<label for="group-name">Naam: </label>
					<input id="group-name" name="group-name" type="text" required />
				</li>
				<li class="buttonbarbottom">
					<button class="buttongreen" name="submit" type="submit">
						<i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Toevoegen
					</button>
					<a class="button buttonspaceleft buttonred" href="index.php?uc=Group-index">
						<i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Annuleren
					</a>
				</li>
			</ul>
		</form>		
	</section>
</main>